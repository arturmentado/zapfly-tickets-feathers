import app from '../../src/app';

describe('\'api-webhook\' service', () => {
  it('registered the service', () => {
    const service = app.service('api-webhook');
    expect(service).toBeTruthy();
  });
});
