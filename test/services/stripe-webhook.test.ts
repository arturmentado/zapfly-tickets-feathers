import app from '../../src/app';

describe('\'stripe-webhook\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe-webhook');
    expect(service).toBeTruthy();
  });
});
