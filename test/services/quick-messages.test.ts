import app from '../../src/app';

describe('\'QuickMessages\' service', () => {
  it('registered the service', () => {
    const service = app.service('quick-messages');
    expect(service).toBeTruthy();
  });
});
