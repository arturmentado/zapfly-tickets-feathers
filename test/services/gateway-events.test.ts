import app from '../../src/app';

describe('\'gateway-events\' service', () => {
  it('registered the service', () => {
    const service = app.service('gateway-events');
    expect(service).toBeTruthy();
  });
});
