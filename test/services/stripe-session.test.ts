import app from '../../src/app';

describe('\'stripe-session\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe-session');
    expect(service).toBeTruthy();
  });
});
