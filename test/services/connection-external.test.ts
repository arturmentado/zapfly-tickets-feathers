import app from '../../src/app';

describe('\'connection-external\' service', () => {
  it('registered the service', () => {
    const service = app.service('connection-external');
    expect(service).toBeTruthy();
  });
});
