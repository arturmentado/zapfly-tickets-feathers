import app from '../../src/app';

describe('\'whatsapp-chats\' service', () => {
  it('registered the service', () => {
    const service = app.service('whatsapp-chats');
    expect(service).toBeTruthy();
  });
});
