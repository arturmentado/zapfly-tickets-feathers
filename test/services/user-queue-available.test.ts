import app from '../../src/app';

describe('\'user-queue-available\' service', () => {
  it('registered the service', () => {
    const service = app.service('user/queue-available');
    expect(service).toBeTruthy();
  });
});
