import app from '../../src/app';

describe('\'pagseguro-callback\' service', () => {
  it('registered the service', () => {
    const service = app.service('external/pagseguro');
    expect(service).toBeTruthy();
  });
});
