import app from '../../src/app';

describe('\'chatbot\' service', () => {
  it('registered the service', () => {
    const service = app.service('chatbot');
    expect(service).toBeTruthy();
  });
});
