import app from '../../src/app';

describe('\'connection-controller\' service', () => {
  it('registered the service', () => {
    const service = app.service('connection-controller');
    expect(service).toBeTruthy();
  });
});
