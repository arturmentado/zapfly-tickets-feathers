// envs-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';


export default function (app: Application): Model<any> {
  const modelName = 'envs';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;

  const metaLinksSchema = new Schema({
    favicon: { type: Object, required: true },
  });

  const pointSchema = new Schema({
    title: { type: String },
    points: [{ type: String }]
  });

  const supportSchema = new Schema({
    active: { type: Boolean, default: true },
    points: [{ type: Object, nullable: true }],
  });

  const politicsSchema = new Schema({
    active: { type: Boolean, default: true },
    date: { type: String, required: true },
    intro: { type: String, nullable: true },
    points: [{ type: pointSchema, nullable: true }],
  });

  const logrocketSchema = new Schema({
    active: { type: Boolean, default: true },
    code: { type: String },
  });

  const schema = new Schema({
    name: { type: String, required: true },
    frontendUrl: { type: String, required: true },
    formattedName: { type: String, required: true },
    metaLinks: metaLinksSchema,
    support: { type: supportSchema, required: true },
    terms: { type: politicsSchema, required: true },
    privacy: { type: politicsSchema, required: true },
    logrocket: { type: logrocketSchema, required: true },
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}
