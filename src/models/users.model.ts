// users-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'users';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const schema = new mongooseClient.Schema({
    name: { type: String },
    lastName: { type: String },
    userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
    connsId: [{ type: mongooseClient.Schema.Types.ObjectId, ref: 'connections' }],
    departmentId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'departmentId' },
    password: { type: String, required: true },
    passwordResetToken: { type: String, nullable: true },
    playbackRate: { type: Number, default: 1 },
    appendMyName: { type: Boolean, default: false },
    useOnboarding: { type: Boolean, default: false },
    notificationExtra: { type: Boolean, default: false },
    vapidDatas: [{ type: Object }],
    appendMyNameAfter: { type: Boolean, default: false },
    notificationSound: { type: Boolean, default: false },
    available: { type: Boolean, default: false },
    languageOverride: { type: String },
    appendMyNameCustom: { type: String, default: '*<meu-nome>*' },
    email: { type: String, unique: true, lowercase: true },
    online: { type: Boolean, default: false },
    ghost: { type: Boolean },
    permissions: { type: Array, required: true, default: ['attendant'] },
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export const createValidationSchema = {
  type: 'object',
  required: ['name', 'lastName', 'email', 'password', 'permissions'],
  properties: {
    email: {
      format: 'email',
    },
    permissions: {
      type: 'array',
      minItems: 1,
      'items': {
        'type': 'string',
        enum: ['attendant', 'admin', 'super-admin', 'supervisor']
      },
    },
    appendMyName: {
      type: 'boolean'
    },
    appendMyNameAfter: {
      type: 'boolean'
    },
    notificationSound: {
      type: 'boolean'
    },
    useOnboarding: {
      type: 'boolean'
    },
    appendMyNameCustom: {
      type: 'string'
    },
    password: {
      minLength: 1
    },
    departmentId: {
      type: ['string', 'object']
    },
    name: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
  }
};