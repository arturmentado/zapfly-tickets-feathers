// messages-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';
import Crypto from 'crypto';

export default function (app: Application): Model<any> {
  const modelName = 'messages';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;

  const keySchema = new Schema({
    remoteJid: { type: String },
    fromMe: { type: Boolean, default: false },
    id: { type: String, default: '3EB0' + Crypto.randomBytes(4).toString('hex').toUpperCase() }
  });

  const schema = new Schema({
    connectionId: { type: Schema.Types.ObjectId, ref: 'connections', required: true },
    contactId: { type: Schema.Types.ObjectId, ref: 'contacts', required: true },
    ticketId: { type: Schema.Types.ObjectId, ref: 'tickets', required: true },
    userId: { type: Schema.Types.ObjectId, ref: 'users', required: false },
    chatbotId: { type: Schema.Types.ObjectId, ref: 'chatbots', required: false },
    apiMessage: { type: Boolean, default: false, },
    chatbotClosing: { type: Boolean, default: null, },
    chatbotSuccess: { type: Boolean, default: null, },
    chatbotEnd: { type: Boolean, default: null, },
    chatbotForId: { type: Schema.Types.ObjectId, ref: 'messages', required: false },
    attendantId: { type: Schema.Types.ObjectId, ref: 'users', required: false },
    key: keySchema,
    messageTimestamp: { type: Number, required: true },
    message: { type: Object },
    paymentInfo: { type: Object },
    autoimport: { type: Boolean },
    messageStubType: { type: String, default: null, nullable: true },
    status: { type: String, required: false, default: 'ERROR' },
    participant: { type: String, nullable: true },
    ephemeralOutOfSync: { type: Boolean, default: false },
    ignore: { type: Boolean, default: false },
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export const createValidationSchema = {
  type: 'object',
  required: ['connectionId', 'contactId', 'ticketId', 'messageTimestamp'],
  properties: {
    connectionId: {
      type: ['object', 'string']
    },
    contactId: {
      type: ['object', 'string']
    },
    ticketId: {
      type: ['object', 'string']
    },
    userId: {
      type: ['object', 'string']
    },
    attendantId: {
      type: ['object', 'string']
    },
    chatbotForId: {
      type: ['object', 'string']
    },
    chatbotSuccess: {
      type: 'boolean'
    },
    chatbotEnd: {
      type: 'boolean'
    },
    autoimport: {
      type: 'boolean',
    },
    chatbotId: {
      type: ['object', 'string']
    },
    key: {
      type: 'object',
      properties: {
        remoteJid: {
          type: 'string'
        },
        fromMe: {
          type: 'boolean'
        },
        id: {
          type: 'string'
        },
      }
    },
    messageStubType: {
      type: 'string'
    },
    messageTimestamp: {
      type: 'number'
    },
    participant: {
      type: 'string'
    },
    message: {
      type: 'object',
    },
    status: {
      type: 'string',
      enum: ['ERROR', 'PENDING', 'SERVER_ACK', 'DELIVERY_ACK', 'READ', 'PLAYED']
    },
    ephemeralOutOfSync: {
      type: 'boolean'
    },
    ignore: {
      type: 'boolean'
    },
  }
};