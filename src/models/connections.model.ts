// connections-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'connections';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;


  const PreapprovalPlanSchema = new Schema({
    code: { type: String, nullable: true },
    status: { type: String, default: 'inactive' },
    type: { type: String },
    stripeSessionId: { type: String },
    stripeCustomerId: { type: String },
    stripeSubscriptionId: { type: String },
    extraTrial: { type: Date }
  }, {
    timestamps: true
  });

  const schema = new Schema({
    jid: { type: String, required: true, unique: true },
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    type: { type: String, default: 'whatsapp', required: true },
    status: { type: String, default: 'not_created', required: true },
    disabled: { type: Boolean, default: false },
    qrcode: { type: String, nullable: true },
    sessionData: { type: Schema.Types.Mixed, nullable: true },
    chatsEnable: { type: Boolean, default: false },
    apiEnable: { type: Boolean, default: false },
    apiToken: { type: String, nullable: true },
    apiWebhook: { type: String, nullable: true },
    port: { type: String, nullable: true },
    latency: { type: Object, nullable: true },
    battery: { type: Object, nullable: true },
    allSetUserId: { type: Boolean, default: false },
    allUserSeeChats: { type: Boolean, default: false },
    useQueue: { type: Boolean, default: false },
    usersSeeOnlyOwn: { type: Boolean, default: false },
    disableAutoImportUnreadMessages: { type: Boolean, default: false },
    disableForceUseHere: { type: Boolean, default: false },
    disableWaitChats: { type: Boolean, default: false },
    blockDirectToPhone: { type: Boolean, default: false },
    activeChatbot: { type: Boolean, default: false },
    activeChatbotButtons: { type: Boolean, default: false },
    allowAllReconnect: { type: Boolean, default: false },
    blockActive: { type: Boolean, default: false },
    useMultidevice: { type: Boolean, default: false },
    blockMessage: { type: String, nullable: true },
    preapprovalPlan: { type: PreapprovalPlanSchema, nullable: true },
    meta: { type: Object, nullable: true }
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

export const createValidationSchema = {
  type: 'object',
  required: ['jid', 'userId', 'type'],
  properties: {
    jid: {
      type: 'string',
    },
    preaprovalPlan: {
      type: ['object']
    },
    userId: {
      type: ['object', 'string']
    },
    type: {
      type: 'string',
      enum: ['whatsapp']
    }
  }
};