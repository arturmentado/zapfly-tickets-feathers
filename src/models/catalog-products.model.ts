// catalog-products-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'catalogProducts';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users', required: true },
    categoryId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'catalogs', required: true },
    catalogId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'catalogs', required: true },
    catalogName: { type: String, required: true },
    name: { type: String },
    available: { type: Boolean, default: false },
    description: { type: String },
    image: { type: String },
    price: { type: Number },
    unitMeasurement: { type: String, default: 'Un' }
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}
