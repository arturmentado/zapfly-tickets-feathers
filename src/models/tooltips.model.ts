// tooltips-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'tooltips';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    active: { type: Boolean, default: false },
    type: { type: String, default: 'notification' },
    html: { type: String },
    forWho: { type: String, default: 'all' },
    notificationTitle: { type: String },
    notificationText: { type: String },
    title: { type: String },
    images: [{ type: String }],
    connectionsId: [{ type: Schema.Types.ObjectId, ref: 'connections' }],
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}
