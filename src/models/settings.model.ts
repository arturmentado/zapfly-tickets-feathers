// settings-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'settings';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    type: { type: String, default: 'connections' },
    userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
    name: { type: String, required: true },
    value: { type: Boolean, default: false },
    meta: { type: Object, default: {} },
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}


export const createValidationSchema = {
  type: 'object',
  required: ['name'],
  properties: {
    name: {
      type: 'string'
    },
    userId: {
      type: ['string', 'object']
    },
    type: {
      enum: ['connections', 'system']
    },
    meta: {
      type: 'object'
    },
    value: {
      type: ['boolean']
    }
  }
};