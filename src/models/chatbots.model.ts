// chatbot-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'chatbots';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
    parentId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'chatbots' },
    type: { type: String, default: 'option' }, // option introduction
    active: { type: Boolean, default: true },
    trigger: { type: String },
    optionType: { type: String },
    optionTargetId: { type: String },
    optionActions: [{ type: Object }],
    optionMessage: { type: String, required: false },
    message: { type: String, required: true }
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}
