async function doIt(app: any) {
  const connections = await app.service('connections')
    .find({
      query: {
        $limit: 99,
        $select: ['_id', 'status', 'jid', 'port'],
        status: { $in: ['off', 'opening', 'replaced', 'RECONNECTING', 'qrcode', 'invalid_session'] },
        port: { $ne: '', },
        // updatedAt: {
        //   $lt: new Date(new Date().getTime() - 60 * 60000) // only if it's as old as 60 minutes
        // },
      }
    });

  console.log('-> connection removal total: ', connections.total);
  if (!connections.total) return;

  for (let index = 0; index < connections.data.length; index++) {
    const conn = connections.data[index];

    console.log('-> connection removal: ', conn.jid, conn.port);

    await app.service('connection-controller')
      .remove(
        conn._id,
        {},
      );
  }

}

export default function (app: any, sheduler: any) {
  const connectionClosing = sheduler.scheduleJob('0 5 * * *', async function () {
    doIt(app);
  });

  const nextRun = connectionClosing.nextInvocation();
  console.log('=> Connection Schedules created', nextRun.toLocaleString('pt-BR'));
}