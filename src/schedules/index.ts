import schedule from 'node-schedule';
import systemSchedules from './system-schedules';
import connectionsSchedules from './connections-schedules';
import notificationSchedules from './notification-schedules';

export default function (app: any) {
  systemSchedules(app, schedule);
  connectionsSchedules(app, schedule);
  notificationSchedules(app, schedule);
}