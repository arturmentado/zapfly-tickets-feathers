import cluster from 'cluster';
import logger from './logger';
import app from './app';

const CLUSTER_COUNT = 4;

if (!process.env.PRODUCTION) {
  const port = app.get('port');
  const server = app.listen(port);

  server.on('listening', () =>
    logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
  );
} else {
  if (cluster.isMaster) {
    for (let i = 0; i < CLUSTER_COUNT; i++) {
      cluster.fork();
    }
  } else {
    const port = app.get('port');
    const server = app.listen(port);

    server.on('listening', () =>
      logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
    );

  }
}
process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);
