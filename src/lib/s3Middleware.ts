import aws from 'aws-sdk';
import multerS3 from 'multer-s3';

import multer from 'multer';

export function middlewareUpload(req: any, _res: any, next: any): void {
  const { method } = req;
  if (method === 'POST' || method === 'PATCH') {
    req.feathers.files = req.files;
  }
  next();
}

export function createUpload(app: any): any {
  const s3 = new aws.S3({
    secretAccessKey: app.get('s3Secret'),
    accessKeyId: app.get('s3Key'),
    region: app.get('s3Region'),
    // sslEnabled: true, // optional
    httpOptions: {
      timeout: 6000,
    },
  });

  const upload = multer({
    storage: multerS3({
      s3,
      acl: 'public-read',
      bucket: app.get('s3Bucket'),
      key: function (req: any, file: any, cb: any) {
        cb(null, `${process.env.PRODUCTION ? 'files' : 'dev'}/${Date.now().toString()}_${file.originalname}`);
      }
    }),
    limits: {
      fieldSize: 1e+8, // Max field value size in bytes, here it's 100MB
      fileSize: 1e+8 //  The max file size in bytes, here it's 10MB
      // READ MORE https://www.npmjs.com/package/multer#limits
    }
  });

  return upload;
}