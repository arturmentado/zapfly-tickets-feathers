import Ajv from 'ajv';
import { validateSchema } from 'feathers-hooks-common';
const ajv: any = new Ajv({ allErrors: true });

function validate(schema: any): any {
  return validateSchema(schema, ajv);
}

export default validate;