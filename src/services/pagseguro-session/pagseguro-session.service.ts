// Initializes the `pagseguro-session` service on path `/pagseguro-session`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PagseguroSession } from './pagseguro-session.class';
import hooks from './pagseguro-session.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'pagseguro-session': PagseguroSession & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/pagseguro-session', new PagseguroSession(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('pagseguro-session');

  service.hooks(hooks);
}
