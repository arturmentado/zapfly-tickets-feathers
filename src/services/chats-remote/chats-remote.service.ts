// Initializes the `chats-remote` service on path `/chats-remote`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ChatsRemote } from './chats-remote.class';
import hooks from './chats-remote.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'chats-remote': ChatsRemote & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/chats-remote', new ChatsRemote(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('chats-remote');

  service.hooks(hooks);
}
