import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { NotFound } from '@feathersjs/errors';
import axios from 'axios';


interface Data { }

interface ServiceOptions { }

export class ChatsRemote implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  baseUrlMd: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.baseUrl = app.get('whatsappBase');
    this.baseUrlMd = app.get('whatsappBaseMd');
  }
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    if (!params?.query?.connectionId) throw new Error('id missing');

    const target: any = await this.app.service('connections').get(params.query.connectionId, {});
    if (!target) throw new NotFound();

    const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/chats/${id}`;
    try {
      const { data } = await axios.get(url);
      return data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

}
