// Initializes the `stripe-webhook` service on path `/stripe-webhook`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { StripeWebhook } from './stripe-webhook.class';
import hooks from './stripe-webhook.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'stripe-webhook': StripeWebhook & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/stripe-webhook', new StripeWebhook(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe-webhook');

  service.hooks(hooks);
}
