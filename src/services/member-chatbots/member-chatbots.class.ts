import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberChatbots implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const user: any = params.user;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    delete params.provider;
    return this.app.service('chatbots').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const user: any = params.user;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    delete params.provider;
    return this.app.service('chatbots').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const user: any = params.user;

    data.userId = user.userId;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    delete params.provider;
    return this.app.service('chatbots').create(data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: any, data: any, params: Params): Promise<Data> {
    const user: any = params.user;

    data.userId = user.userId;
    params.query = {
      ...params.query,
      userId: user.userId
    };

    delete params.provider;
    return this.app.service('chatbots').update(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: any, params: Params): Promise<Data> {
    const user: any = params.user;

    data.userId = user.userId;
    params.query = {
      ...params.query,
      userId: user.userId
    };

    delete params.provider;
    return this.app.service('chatbots').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params: Params): Promise<Data> {
    const user: any = params.user;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    delete params.provider;
    return this.app.service('chatbots').remove(id, params);
  }
}
