// Initializes the `member-chatbots` service on path `/member-chatbots`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberChatbots } from './member-chatbots.class';
import hooks from './member-chatbots.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-chatbots': MemberChatbots & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-chatbots', new MemberChatbots(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-chatbots');

  service.hooks(hooks);
}
