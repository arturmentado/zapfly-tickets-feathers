import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import axios from 'axios';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class ApiWebhook implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    // this.baseUrl = 'https://proxy.zapfly.workers.dev/';
    this.baseUrl = 'https://proxy.samuelcolvin.workers.dev';
  }
  [key: string]: any;
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: Params): Promise<any> {
    const { connection } = data;
    if (!connection.apiEnable) return;
    if (!connection.apiWebhook) return;

    const upstream = connection.apiWebhook;
    delete data.connection;
    const url = `${this.baseUrl}?upstream=${encodeURI(upstream)}`;
    axios.post(url, data);
    return;
  }
}
