// Initializes the `api-webhook` service on path `/api-webhook`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ApiWebhook } from './api-webhook.class';
import hooks from './api-webhook.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api-webhook': ApiWebhook & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api-webhook', new ApiWebhook(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api-webhook');

  service.hooks(hooks);
}
