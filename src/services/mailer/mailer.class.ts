import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
// import mailer from 'featthers-mailer';
import axios from 'axios';
import http from 'http';
import nodemailer from 'nodemailer';

interface Data { }

interface ServiceOptions { }

export class Mailer implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  transporter: any;
  from: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;

    this.from = '"ZapFly - Não Responda" <zapfly@arcode.online>';

    this.transporter = nodemailer.createTransport({
      host: 'srv206.prodns.com.br',
      name: 'arcode.online',
      port: 465,
      secure: true, // 487 only
      requireTLS: true,
      auth: {
        user: 'zapfly@arcode.online', // generated ethereal user
        pass: 'golpegolpe432' // generated ethereal password
      }
    });
  }
  [key: string]: any;
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<any | Paginated<Data>> {
    const a = await axios.post('https://proxy.zapfly.workers.dev/?upstream=https://b4e147a28035.ngrok.io&type=teste&id=1', { test: 111 });
    return a.data;
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: Params): Promise<Data> {
    if (process.env.TESTING) return {};

    try {
      const payload = {
        from: this.from,
        ...data
        // to: 'algoz098@gmail.com',
        // to: data.to, // list of receivers
        // subject: data.subject, // Subject line
        // text: 'Hello world?', // plain text body
        // html: data.html, // html body
      };
      const info = await this.transporter.sendMail(payload);
      return info;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
