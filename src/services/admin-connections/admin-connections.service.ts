// Initializes the `admin-connections` service on path `/admin-connections`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { AdminConnections } from './admin-connections.class';
import hooks from './admin-connections.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin-connections': AdminConnections & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate'),
    whitelist: ['$populate', '$text', '$search', '$regex', '$options'],
  };

  // Initialize our service with any options it requires
  app.use('/admin-connections', new AdminConnections(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-connections');

  service.hooks(hooks);
}
