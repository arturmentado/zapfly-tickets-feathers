import { Service, MongooseServiceOptions } from 'feathers-mongoose';
import { Id, Params } from '@feathersjs/feathers';
import { Application } from '../../declarations';

export class Catalogs extends Service {
  app: Application;
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
    this.app = app;
  }


  async get(id: Id, params?: Params): Promise<any> {
    const catalog = await this._get(id, params);
    const checkEnabled: any = await this.app.service('settings').find({ query: { $limit: 1, name: 'catalogActive', userId: catalog.userId, value: true } });
    if (!checkEnabled.length) throw Error('not found');
    return catalog;
  }
}
