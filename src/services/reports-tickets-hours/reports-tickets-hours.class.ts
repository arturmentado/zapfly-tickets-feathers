import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class ReportsTicketsHours implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const departmentId = params?.query?.departmentId ?? undefined;
    const user: any = params.user;
    const connsId = user.connsId;

    const $match: any = {
      connectionId: { $in: connsId }
    };
    if (departmentId) $match.departmentId = departmentId;

    const today = new Date();
    const priorDate = new Date(new Date().setDate(today.getDate() - 30));

    const query = await this.app.service('tickets').Model.aggregate([
      { $match: { 'createdAt': { $gt: priorDate } } },
      {
        $match
      }, {
        '$addFields': {
          'day': {
            '$dayOfWeek': '$createdAt'
          },
          'hour': {
            '$hour': '$createdAt'
          }
        }
      }, {
        '$project': {
          '_id': 1,
          'day': 1,
          'hour': 1
        }
      }, {
        '$group': {
          '_id': '$day',
          'hours': {
            '$push': '$hour'
          }
        }
      }
    ]);
    // .toArray()

    const result = [];

    for (let index = 0; index < query.length; index++) {
      const item = query[index];

      const toPush: any = {};
      item.hours.forEach((x: any) => toPush[x] = (toPush[x] || 0) + 1);

      result.push({ day: item._id, hours: toPush });
    }

    return result;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
