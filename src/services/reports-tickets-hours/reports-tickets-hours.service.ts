// Initializes the `reports-tickets-hours` service on path `/reports/tickets-hours`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ReportsTicketsHours } from './reports-tickets-hours.class';
import hooks from './reports-tickets-hours.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'reports/tickets-hours': ReportsTicketsHours & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/reports/tickets-hours', new ReportsTicketsHours(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('reports/tickets-hours');

  service.hooks(hooks);
}
