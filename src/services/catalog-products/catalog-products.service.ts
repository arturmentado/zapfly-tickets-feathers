// Initializes the `catalog-products` service on path `/catalog-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { CatalogProducts } from './catalog-products.class';
import createModel from '../../models/catalog-products.model';
import hooks from './catalog-products.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'catalog-products': CatalogProducts & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    multi: ['patch'],
    Model: createModel(app),
    paginate: {
      'default': 200,
      'max': 200
    },
  };

  // Initialize our service with any options it requires
  app.use('/catalog-products', new CatalogProducts(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('catalog-products');

  service.hooks(hooks);
}
