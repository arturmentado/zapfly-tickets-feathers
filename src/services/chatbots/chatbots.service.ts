// Initializes the `chatbot` service on path `/chatbot`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Chatbots } from './chatbots.class';
import createModel from '../../models/chatbots.model';
import hooks from './chatbots.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'chatbots': Chatbots & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/chatbots', new Chatbots(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('chatbots');

  service.hooks(hooks);
}
