// Initializes the `pagseguro-callback` service on path `/external/pagseguro`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PagseguroCallback } from './pagseguro-callback.class';
import hooks from './pagseguro-callback.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'external/pagseguro': PagseguroCallback & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/external/pagseguro', new PagseguroCallback(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('external/pagseguro');

  service.hooks(hooks);
}
