import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import Stripe from 'stripe';

interface Data { }

interface ServiceOptions { }

export class StripeSession implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  stripe: any

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.stripe = new Stripe(app.get('stripeToken'), { apiVersion: '2020-08-27', });
  }
  [key: string]: any;
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  async remove(id: Id, params: Params): Promise<Data | Data[]> {
    const connection: any = await this.app.service('member-connections').get(id, params);
    this.stripe.subscriptions.del(connection.preapprovalPlan.stripeSubscriptionId);
    return this.app.service('member-connections').patch(id, { 'preapprovalPlan.status': 'REVOKED' }, params);
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const success_url = `${this.app.get('frontendDomain')}/dashboard?stripe-success=true&session_id={CHECKOUT_SESSION_ID}`;
    const cancel_url = `${this.app.get('frontendDomain')}/dashboard?stripe-fail=true&`;
    const { priceId, connectionId } = data;

    await this.app.service('member-connections').get(connectionId, params);

    // See https://stripe.com/docs/api/checkout/sessions/create
    // for additional parameters to pass.
    try {
      const session = await this.stripe.checkout.sessions.create({
        mode: 'subscription',
        payment_method_types: ['card'],
        line_items: [
          {
            price: priceId,
            quantity: 1,
          },
        ],
        metadata: {
          connectionId
        },
        // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
        // the actual Session ID is returned in the query parameter when your customer
        // is redirected to the success page.
        success_url,
        cancel_url,
      });

      await this.app.service('member-connections').patch(connectionId, {
        'preapprovalPlan.type': 'stripe',
        'preapprovalPlan.status': 'requested',
        'preapprovalPlan.stripeSessionId': session.id
      }, params);

      return session.id;
    } catch (e) {
      console.error(e);
      throw 'error in stripe request';
    }
  }
}
