// Initializes the `stripe-session` service on path `/stripe-session`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { StripeSession } from './stripe-session.class';
import hooks from './stripe-session.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'stripe-session': StripeSession & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/stripe-session', new StripeSession(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe-session');

  service.hooks(hooks);
}
