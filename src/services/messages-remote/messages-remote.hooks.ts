import { disallow } from 'feathers-hooks-common';
import validate from '../../lib/ajv';

const createValidationSchema = {
  type: 'object',
  required: ['connectionId', 'id', 'type'],
  properties: {
    connectionId: {
      type: ['string', 'object']
    },
    id: {
      type: 'string'
    },
    caption: {
      type: 'string',
    },
    message: {
      type: ['string', 'object'],
    },
    url: {
      type: ['string', 'boolean'],
    },
    type: {
      type: 'string',
    },
  }
};

export default {
  before: {
    all: [disallow('external')],
    find: [],
    get: [],
    create: [validate(createValidationSchema)],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
