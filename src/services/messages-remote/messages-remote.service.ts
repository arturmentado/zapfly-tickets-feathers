// Initializes the `messages-remote` service on path `/messages-remote`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MessagesRemote } from './messages-remote.class';
import hooks from './messages-remote.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'messages-remote': MessagesRemote & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/messages-remote', new MessagesRemote(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('messages-remote');

  service.hooks(hooks);
}
