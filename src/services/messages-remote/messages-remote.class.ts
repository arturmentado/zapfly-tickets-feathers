import { NotFound } from '@feathersjs/errors';
import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import axios from 'axios';
import FormData from 'form-data';
import fs from 'fs';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MessagesRemote implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  baseUrlMd: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.baseUrl = app.get('whatsappBase');
    this.baseUrlMd = app.get('whatsappBaseMd');
  }
  [key: string]: any;
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const target: any = !params.connection?._id ? await this.app.service('connections').get(params.query?.connectionId) : params.connection;
    if (!target) throw new NotFound();
    if (target.useMultidevice) {
      console.trace('using multidevices');
      throw new Error('Using multidevices');
    }

    const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/messages-query`;
    const options = {
      params: params.query
    };

    try {
      const res = await axios.get(url, options);
      return res.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    if (process.env.TESTING) return {};
    if (data.jid === 'status@broadcast') return {};
    const target: any = await this.app.service('connections').get(data.connectionId);
    if (!target) throw new NotFound();

    let endpoint = 'messages-send';

    const isMedia = params?.files?.length > 0 || !!data.url;
    const hasFile = params?.files?.length > 0 && (params?.files && params?.files[0] && params?.files.path);

    if (isMedia) {
      let file;
      endpoint = 'messages-send-media';

      const form = new FormData();

      if (hasFile) {
        file = params.files[0];
        const fileStream = fs.createReadStream(file.path);
        form.append('file', fileStream);
      }

      for (const key in data) {
        const item = data[key];
        if (!item || typeof item === 'object') continue;
        form.append(key, item);
      }

      const options = {
        headers: form.getHeaders()
      };

      const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/${endpoint}`;
      try {
        const res = await axios.post(url, form, options);

        if (hasFile) {
          fs.unlinkSync(file.path);
        }

        return res.data;
      } catch (error: any) {
        console.error(error.toJSON(), data);
        throw error;
      }
    }

    const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/${endpoint}`;
    try {
      const res = await axios.post(url, data);
      return res.data;
    } catch (error: any) {
      console.error(error.toJSON(), data);
      throw error;
    }
  }
}
