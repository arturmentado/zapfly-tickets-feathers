import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { NotFound } from '@feathersjs/errors';

interface Data { }

interface ServiceOptions { }

export class Members implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const user: any = params.user;
    delete params.provider;
    params.query = {
      ...params.query,
      ghost: {
        $ne: true
      },
      userId: user.userId
    };
    return this.app.service('users').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const user: any = params.user;
    delete params.provider;
    params.query = {
      ...params.query,
      ghost: {
        $ne: true
      },
      userId: user.userId
    };
    return this.app.service('users').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: Params): Promise<Data> {
    data.userId = params?.user?.userId;
    const conns: Paginated<any> = await this.app.service('connections')
      .find({
        $select: ['_id'],
        query: { userId: data.userId },
        $limit: -1
      }) as Paginated<any>;

    data.connsId = conns.data.map(e => e._id);
    return this.app.service('users').create(data);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: Id, data: Data, params: Params): Promise<Data> {
    const target = this.get(id, params);
    if (!target) throw new NotFound();
    return this.app.service('users').update(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: Id, data: any, params: Params): Promise<Data> {
    const perms = params.user?.permissions;
    const userId = params.user?.userId;

    delete data.ghost;

    params.query = {
      ...params.query,
      userId
    };

    if (!perms.includes('admin', 'supervisor')) {
      if (!params.user?._id.equals(id)) throw new Error('not allowed');
    }

    delete params.provider;
    return this.app.service('users').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
