import { disallow } from 'feathers-hooks-common';
import * as authentication from '@feathersjs/authentication';
import * as local from '@feathersjs/authentication-local';
import validate from '../../lib/ajv';
import { createValidationSchema as usersCreateValidationSchema } from '../../models/users.model';
import checkPermissions from 'feathers-permissions';
// Don't remove this comment. It's needed to format import lines nicely.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

const createValidationSchema = JSON.parse(JSON.stringify(usersCreateValidationSchema));
createValidationSchema.properties.permissions.items.enum = ['attendant', 'supervisor'];

const { authenticate } = authentication.hooks;
const { protect } = local.hooks;

export default {
  before: {
    all: [authenticate('jwt')],
    find: [
      checkPermissions({ roles: ['admin', 'attendant'] }),
      search({ escape: false }),
      search({  // regex search on given fields
        fields: [
          'name',
          'lastName',
          'email'
        ]
      }),
    ],
    get: [checkPermissions({ roles: ['admin', 'attendant'] })],
    create: [checkPermissions({ roles: ['admin', 'supervisor'] }), validate(createValidationSchema)],
    update: [checkPermissions({ roles: ['admin'] }), validate(createValidationSchema)],
    patch: [checkPermissions({ roles: ['admin', 'attendant'] }),],
    remove: [disallow()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      protect('passwordResetToken')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
