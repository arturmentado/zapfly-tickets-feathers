import validate from '../../lib/ajv';
import { createValidationSchema } from '../../models/messages.model';
import { disallow } from 'feathers-hooks-common';
//
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

export default {
  before: {
    all: [disallow('external')],
    // all: [],
    find: [
      search({ escape: false }),
      search({  // regex search on given fields
        fields: [
          'message.conversation',
          'message.extendedTextMessage.text',
        ]
      }),
    ],
    get: [],
    create: [validate(createValidationSchema)],
    // create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
