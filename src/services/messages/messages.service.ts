// Initializes the `messages` service on path `/messages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Messages } from './messages.class';
import createModel from '../../models/messages.model';
import hooks from './messages.hooks';
import onEvents from '../messages/message.on-events';
// Add this service to the service type index

declare module '../../declarations' {
  interface ServiceTypes {
    'messages': Messages & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    multi: ['patch'],
    whitelist: ['$text', '$search', '$options', '$regex', '$populate'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use(
    '/messages',
    new Messages(options, app)
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('messages');
  onEvents(app);

  service.hooks(hooks);
}
