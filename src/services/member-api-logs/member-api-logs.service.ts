// Initializes the `member-api-logs` service on path `/member-api-logs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberApiLogs } from './member-api-logs.class';
import hooks from './member-api-logs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-api-logs': MemberApiLogs & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-api-logs', new MemberApiLogs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-api-logs');

  service.hooks(hooks);
}
