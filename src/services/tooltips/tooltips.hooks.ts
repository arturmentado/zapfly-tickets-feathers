import * as authentication from '@feathersjs/authentication';
import checkPermissions from 'feathers-permissions';
const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [authenticate('jwt'), checkPermissions({ roles: ['super-admin'] })],
    update: [authenticate('jwt'), checkPermissions({ roles: ['super-admin'] })],
    patch: [authenticate('jwt'), checkPermissions({ roles: ['super-admin'] })],
    remove: [authenticate('jwt'), checkPermissions({ roles: ['super-admin'] })]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
