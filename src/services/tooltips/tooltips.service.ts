// Initializes the `tooltips` service on path `/tooltips`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Tooltips } from './tooltips.class';
import createModel from '../../models/tooltips.model';
import hooks from './tooltips.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'tooltips': Tooltips & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/tooltips', new Tooltips(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('tooltips');

  service.hooks(hooks);
}
