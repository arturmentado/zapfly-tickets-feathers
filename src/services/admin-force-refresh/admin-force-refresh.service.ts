// Initializes the `admin-force-refresh` service on path `/admin-force-refresh`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { AdminForceRefresh } from './admin-force-refresh.class';
import hooks from './admin-force-refresh.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin-force-refresh': AdminForceRefresh & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    events: ['force-refresh'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-force-refresh', new AdminForceRefresh(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-force-refresh');

  service.hooks(hooks);
}
