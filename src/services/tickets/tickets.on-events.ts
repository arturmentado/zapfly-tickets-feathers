// import { Application } from '../../declarations';
import Crypto from 'crypto';

async function onStatusClosing(data: any, app: any) {
  try {
    let chatbotClosing = await app.service('chatbots').find({ // used for failed message processing
      paginate: false, query: {
        $limit: 1,
        userId: data.userId, type: 'closing', active: true
      }
    });

    if (!chatbotClosing.length) return; // no closing options active
    chatbotClosing = chatbotClosing[0];

    const payload = { // building the message for the DB
      connectionId: data.connectionId,
      contactId: data.contactId,
      ticketId: data._id,
      userId: data.userId,
      chatbotId: chatbotClosing._id,
      chatbotForId: data._id,
      chatbotSuccess: true,
      chatbotClosing: true,
      chatbotEnd: true,
      key: {
        fromMe: true,
        id: `3EB0${Crypto.randomBytes(4).toString('hex').toUpperCase()}`,
        remoteJid: data.jid
      },
      message: {
        conversation: chatbotClosing.message
      },
      messageTimestamp: Math.floor(new Date().getTime() - 2)
    };

    await app.service('messages').create(payload);

    const type = Object.keys(payload.message)[0]; // building for whatsapp
    const messageSend: any = {
      id: data.jid,
      messageId: payload.key.id,
      connectionId: payload.connectionId,
      message: chatbotClosing.message,
      type
    };

    await app.service('messages-remote').create(messageSend);

  } catch (error) {
    console.error('ticket closing logic error', error);
  }
}

function handlePatched(data: any, app: any, event: any) {
  // console.log(event.data);
  if (data.status === 'closing' && event.data.status && event.data.status === 'closing') return onStatusClosing(data, app);
}

export default function (app: any): void {
  app.service('tickets').on('patched', (data: any, event: any) => handlePatched(data, app, event));
}
