import validate from '../../lib/ajv';
import { createValidationSchema } from '../../models/tickets.model';
import { disallow } from 'feathers-hooks-common';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

// Don't remove this comment. It's needed to format import lines nicely.


export default {
  before: {
    all: [
      disallow('external'),
    ],
    find: [
      search({ escape: false }),
      search({  // regex search on given fields
        fields: [
          'jid',
          'contact.name',
          'contact.notify',
          'contact.overrideName',
          'contact.vname',
        ]
      }),
    ],
    get: [],
    create: [
      validate(createValidationSchema),
      async (ctx: any) => {
        let settings = await ctx.app.service('settings').find({ paginate: false, query: { name: 'useQueue', userId: ctx.data.userId, value: true } });
        if (!settings.length) return ctx;

        settings = settings[0];
        if (!settings.meta.queueOrder) return ctx;
        if (!settings.meta.queueOrder[0]) return ctx;

        const attendantId = settings.meta.queueOrder[0];

        ctx.data.status = 'in_progress';
        ctx.data.attendantId = attendantId;

        const queueOrder = settings.meta.queueOrder;
        queueOrder.push(queueOrder.shift());

        await ctx.app.service('settings').patch(settings._id, { 'meta.queueOrder': queueOrder });
        return ctx;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
