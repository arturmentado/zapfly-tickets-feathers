import { NotFound } from '@feathersjs/errors';
import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberMessages implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<any> {
    const connsId: any = JSON.parse(JSON.stringify(params?.user?.connsId));

    if (!params.query) params.query = {};

    if (!params.query.connectionId) {
      params.query.connectionId = { $in: connsId };
    } else if (!connsId.includes(params.query.connectionId)) throw new NotFound();

    delete params.provider;
    return this.app.service('messages').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const connsId: any = JSON.parse(JSON.stringify(params?.user?.connsId));

    if (!params.query) params.query = {};

    if (!params.query.connectionId) {
      params.query.connectionId = { $in: connsId };
    } else if (!connsId.includes(params.query.connectionId)) throw new NotFound();

    delete params.provider;
    return this.app.service('messages').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<any> {
    const connection = await this.app.service('member-connections').get(data.connectionId, params);
    if (!connection) throw new NotFound();
    delete params.provider;

    data.messageTimestamp = Math.floor(new Date().getTime() / 1000);
    data.status = 'PENDING';

    let url = false;
    if (data.files) {
      const file = JSON.parse(data.files);
      url = file.location;
    }

    if (params.files && params?.files.length && params.files[0]?.location) {
      url = params.files[0].location;
    }

    if (url) {
      let type = Object.keys(data).find((e: any) => e.includes('message.'));
      if (type && type.includes('.caption')) type = type.replace('.caption', '');
      data[`${type}.originalUrl`] = url;
      data[`${type}.originalMimetype`] = data.mimetype;
    }

    const dataKeys = Object.keys(data);
    const dataValues = Object.values(data);
    if (dataValues.some((e: any) => e === 'null')) {
      const key = dataKeys[dataValues.findIndex((e: any) => e === 'null')];
      data[key] = {};
    }

    let quoted = data.quoted;
    let quotedMessage;
    if (data.quoted) {
      data['message.originalQuoted'] = data.quoted;
      if (connection.useMultidevice) {
        const messagesQuoted: any = await this.app.service('messages').find({
          query: {
            $limit: 1,
            'key.id': quoted,
            connectionId: connection._id
          }
        });
        quoted = null;

        if (messagesQuoted.total) {
          const message = messagesQuoted.data[0];
          quotedMessage = {
            key: message.key,
            message: message.message,
          };
          delete message.message.quotedMessage;
          data['message.quotedMessage'] = message;
        } else {
        }
      }
    }

    delete data.quoted;
    const message = await this.app.service('messages').create(data);

    const type = Object.keys(message.message)[0];
    const messageSend: any = {
      id: data.id,
      messageId: message.key.id,
      connectionId: data.connectionId,
      quoted,
      quotedMessage,
      type,
      url,
    };

    if (message.message[type].caption) {
      messageSend.caption = message.message[type].caption;
    } else {
      messageSend.message = message.message[type];
    }

    if (params.files?.length) {
      messageSend.mimetype = data.mimetype;
    }

    const ticketData: any = { count: 0, attendantId: params?.user?._id, status: 'in_progress', lastMessage: message, messageTimestamp: data.messageTimestamp };
    if (params?.user?.departmentId) {
      ticketData.departmentId = params?.user.departmentId;
    }
    await this.app.service('tickets').patch(data.ticketId, ticketData);

    const responseRemote = await this.app.service('messages-remote').create(messageSend, params)
      .catch(() => { return false; });

    if (connection.useMultidevice) {
      if (responseRemote) await this.app.service('messages').patch(message._id, { status: 'SERVER_ACK' });
      return null;
    }
    return message;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
