// Initializes the `member-messages` service on path `/member-messages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberMessages } from './member-messages.class';
import hooks from './member-messages.hooks';
import { createUpload, middlewareUpload } from '../../lib/s3Middleware';
//
declare module '../../declarations' {
  interface ServiceTypes {
    'member-messages': MemberMessages & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const upload = createUpload(app);
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use(
    '/member-messages',
    upload.array('files'),
    middlewareUpload,
    new MemberMessages(options, app)
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('member-messages');

  service.hooks(hooks);
}
