import * as authentication from '@feathersjs/authentication';
import { createValidationSchema as messageCreateValidationSchema } from '../../models/messages.model';
import validate from '../../lib/ajv';
// Don't remove this comment. It's needed to format import lines nicely.

const createValidationSchema = JSON.parse(JSON.stringify(messageCreateValidationSchema));
createValidationSchema.required = ['connectionId', 'contactId', 'ticketId', 'userId'];
createValidationSchema.properties.quoted = {
  type: 'string'
};

const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [validate(createValidationSchema)],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
