// Initializes the `member-catalog-categories` service on path `/member-catalog-categories`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberCatalogCategories } from './member-catalog-categories.class';
import hooks from './member-catalog-categories.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-catalog-categories': MemberCatalogCategories & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-catalog-categories', new MemberCatalogCategories(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-catalog-categories');

  service.hooks(hooks);
}
