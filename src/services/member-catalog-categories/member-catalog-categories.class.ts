import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberCatalogCategories implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  remove(id: NullableId, params: Params): Promise<Data | Data[]> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalog-categories').remove(id, params);
  }

  async find(params: Params): Promise<Data | Data[] | Paginated<Data>> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalog-categories').find(params);
  }

  async get(id: Id, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalog-categories').get(id, params);
  }

  async update(id: Id, data: any, params: Params): Promise<Data | Data[]> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('catalog-categories').update(id, data, params);
  }

  async patch(id: Id, data: any, params: Params): Promise<Data | Data[]> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalog-categories').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    data.userId = user.userId;

    let catalog: any = await this.app.service('member-catalogs').find(params);

    if (!catalog.total) throw new Error('catalog non existent');
    catalog = catalog.data[0];

    data.catalogId = catalog._id;
    data.catalogName = catalog.name;

    delete params.provider;
    return this.app.service('catalog-categories').create(data, params);
  }

}
