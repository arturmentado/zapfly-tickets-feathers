// Initializes the `tooltips-read` service on path `/tooltips-read`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { TooltipsRead } from './tooltips-read.class';
import createModel from '../../models/tooltips-read.model';
import hooks from './tooltips-read.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'tooltips-read': TooltipsRead & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/tooltips-read', new TooltipsRead(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('tooltips-read');

  service.hooks(hooks);
}
