import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberTags implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  [key: string]: any;
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const user: any = params?.user;
    delete params.provider;
    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('tags').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('tags').create(data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: Id, data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('tags').update(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: Id, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('tags').remove(id, params);
  }
}
