// Initializes the `member-tags` service on path `/member-tags`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberTags } from './member-tags.class';
import hooks from './member-tags.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-tags': MemberTags & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-tags', new MemberTags(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-tags');

  service.hooks(hooks);
}
