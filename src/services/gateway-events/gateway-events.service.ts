// Initializes the `gateway-events` service on path `/gateway-events`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { GatewayEvents } from './gateway-events.class';
import createModel from '../../models/gateway-events.model';
import hooks from './gateway-events.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'gateway-events': GatewayEvents & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/gateway-events', new GatewayEvents(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('gateway-events');

  service.hooks(hooks);
}
