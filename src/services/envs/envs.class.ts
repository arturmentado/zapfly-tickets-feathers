import { Service, MongooseServiceOptions } from 'feathers-mongoose';
import { Application } from '../../declarations';

export class Envs extends Service {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(name: string, params?: any): Promise<any> {
    return this.Model.findOne({ name });
  }
}
