// Initializes the `envs` service on path `/envs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Envs } from './envs.class';
import createModel from '../../models/envs.model';
import hooks from './envs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'envs': Envs & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/envs', new Envs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('envs');

  service.hooks(hooks);
}
