import checkPermissions from 'feathers-permissions';
import { disallow } from 'feathers-hooks-common';
// Don't remove this comment. It's needed to format import lines nicely.

export default {
  before: {
    all: [],
    find: [disallow('external')],
    get: [],
    create: [disallow('external')],
    update: [disallow('external')],
    patch: [checkPermissions({ roles: ['super-admin'] })],
    remove: [disallow('external')]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
