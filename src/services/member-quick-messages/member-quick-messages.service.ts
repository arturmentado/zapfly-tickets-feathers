// Initializes the `member-quick-messages` service on path `/member-quick-messages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberQuickMessages } from './member-quick-messages.class';
import hooks from './member-quick-messages.hooks';
import { createUpload, middlewareUpload } from '../../lib/s3Middleware';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-quick-messages': MemberQuickMessages & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const upload = createUpload(app);

  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use(
    '/member-quick-messages',
    upload.array('files'),
    middlewareUpload,
    new MemberQuickMessages(options, app)
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('member-quick-messages');

  service.hooks(hooks);
}
