// Initializes the `public-channel-control` service on path `/public-channel-control`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PublicChannelControl } from './public-channel-control.class';
import hooks from './public-channel-control.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'public-channel-control': PublicChannelControl & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/public-channel-control', new PublicChannelControl(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('public-channel-control');

  service.hooks(hooks);
}
