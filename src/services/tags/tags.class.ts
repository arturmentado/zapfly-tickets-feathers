import { Service, MongooseServiceOptions } from 'feathers-mongoose';
import { Id, Params, } from '@feathersjs/feathers';
import { Application } from '../../declarations';

export class Tags extends Service {
  app: Application;

  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: Id, params: Params): Promise<any> {
    const contacts: any = await this.app.service('contacts').find({ query: { tagsId: id, $select: ['_id', 'tagsId', 'tags'] }, paginate: false, });

    contacts.forEach((contact: any) => {
      this.app.service('contacts').patch(contact._id, {
        tagsId: contact.tagsId.filter((e: any) => e !== id),
        tags: contact.tags.filter((e: any) => e._id !== id)
      });
    });

    return this._remove(id, params);
  }
}
