// Initializes the `admin-impersonate` service on path `/admin-impersonate`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { AdminImpersonate } from './admin-impersonate.class';
import hooks from './admin-impersonate.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin-impersonate': AdminImpersonate & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-impersonate', new AdminImpersonate(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-impersonate');

  service.hooks(hooks);
}
