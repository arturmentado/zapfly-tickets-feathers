import * as feathersAuthentication from '@feathersjs/authentication';
import * as local from '@feathersjs/authentication-local';
import validate from '../../lib/ajv';
import { createValidationSchema } from '../../models/users.model';
import checkPermissions from 'feathers-permissions';
import { disallow } from 'feathers-hooks-common';
// Don't remove this comment. It's needed to format import lines nicely.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;

export default {
  before: {
    all: [],
    find: [
      disallow('external'),
      authenticate('jwt'),
      search({ escape: true }),
      search({  // regex search on given fields
        fields: [
          'name',
          'lastName',
          'email'
        ]
      }),
    ],
    get: [authenticate('jwt')],
    create: [disallow('external'), authenticate('jwt'), checkPermissions({ roles: ['super-admin'] }), hashPassword('password'), validate(createValidationSchema)],
    update: [disallow('external'), hashPassword('password'), authenticate('jwt')],
    patch: [disallow('external'), hashPassword('password'), authenticate('jwt')],
    remove: [disallow('external'), authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password'),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
