// Initializes the `connection-controller` service on path `/connection-controller`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ConnectionController } from './connection-controller.class';
import hooks from './connection-controller.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'connection-controller': ConnectionController & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/connection-controller', new ConnectionController(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('connection-controller');

  service.hooks(hooks);
}
