import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { NotFound } from '@feathersjs/errors';

interface Data { }

interface ServiceOptions { }

export class ConnectionController implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: Params): Promise<Data> {

    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: Id, data: any, params: Params): Promise<Data> {
    const user = params.user as any;
    let target: any;

    if (!user.permissions.includes('super-admin')) {
      target = await this.app.service('member-connections').get(id, params);
      if (!target) throw new NotFound();
      target = await this.app.service('connections').get(id, { admin: true });
    } else {
      target = await this.app.service('connections').get(id, { admin: true });
    }

    await this.app.service('connections').patch(target._id, { status: 'opening' });

    if (target.port) {
      try {
        await this.app.service('controller-remote').remove(target.jid, { useMultidevice: target.useMultidevice });
      } catch (error) {
      }
    }

    const payload: any = {
      waitChats: !target.disableWaitChats ?? true,
      sessionData: target.useMultidevice ? true : target.sessionData,
      disableAutoImportUnreadMessages: target.disableAutoImportUnreadMessages ?? true,
      disableForceUseHere: target.disableForceUseHere ?? true,
      useMultidevice: target.useMultidevice ?? false
    };

    if (target.port) {
      payload.port = target.port;
    }

    await this.app.service('controller-remote')
      .update(target.jid, payload);

    return { success: true };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: Id, params: Params): Promise<Data> {
    const user = params.user as any;
    let target: any;

    if (params.provider && !user.permissions.includes('super-admin')) {
      target = await this.app.service('member-connections').get(id, params);
      if (!target) throw new NotFound();
    } else {
      target = await this.app.service('connections').get(id, { admin: true });
    }

    await this.app.service('connections').patch(target._id, { status: 'REMOVED', qrcode: null });

    await this.app.service('controller-remote').remove(target.jid, { useMultidevice: target.useMultidevice });
    return { success: true };
  }
}
