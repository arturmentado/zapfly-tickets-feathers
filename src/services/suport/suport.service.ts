// Initializes the `suport` service on path `/suport`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Suport } from './suport.class';
import hooks from './suport.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'suport': Suport & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/suport', new Suport(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('suport');

  service.hooks(hooks);
}
