// Initializes the `upload` service on path `/upload`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Upload } from './upload.class';
import hooks from './upload.hooks';
import { createUpload, middlewareUpload } from '../../lib/s3Middleware';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'upload': Upload & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const upload = createUpload(app);

  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use(
    '/upload',
    upload.array('files'),
    middlewareUpload,
    new Upload(options, app)
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('upload');

  service.hooks(hooks);
}
