// Initializes the `pagseguro-notification` service on path `/pagseguro-notification`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PagseguroNotification } from './pagseguro-notification.class';
import hooks from './pagseguro-notification.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'pagseguro-notification': PagseguroNotification & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/pagseguro-notification', new PagseguroNotification(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('pagseguro-notification');

  service.hooks(hooks);
}
