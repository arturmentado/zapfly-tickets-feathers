import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import axios from 'axios';

interface Data { }

interface ServiceOptions { }

export class PagseguroNotification implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  email: string;
  token: string;
  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.email = this.app.get('pagseguroEmail');
    this.token = this.app.get('pagseguroToken');
    this.baseUrl = `https://ws${this.app.get('pagseguroEnv')}pagseguro.uol.com.br/pre-approvals/notifications/`;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: any, params?: Params): Promise<Data> {
    return axios.get(`${this.baseUrl}${id}?token=${this.token}&email=${this.email}`, { headers: { Accept: 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1' } });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
