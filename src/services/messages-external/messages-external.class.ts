import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MessagesExternal implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  ignitableStubTypes: [string]

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.ignitableStubTypes = app.get('ignitableStubTypes');
  }
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  [key: string]: any;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: any, params: Params): Promise<any> {
    try {
      const sessionId = params.headers?.session_id;
      const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });

      const contactsId = [];
      const promises = [];
      for (const target of data) {
        if (target.key.id === 'status@broadcast') continue;
        if (target.key.remoteJid === 'status@broadcast') continue;
        if (this.ignitableStubTypes.includes(target.messageStubType)) continue;
        if (target.message && target.message.senderKeyDistributionMessage) continue;

        this.app.service('api-webhook').create({ message: target, connection: conn });

        const jid = target.key.remoteJid;
        const messageId = target.key.id;

        const contacts: any = await this.app.service('contacts').find({ paginate: false, query: { connectionId: conn._id, jid, $limit: 1 } });
        let contact = contacts[0];
        if (!contact) {
          contact = await this.app.service('contacts').create({ jid, connectionId: conn._id });
        }

        const ticketContact = contact;
        const ticketContactId = contact._id;

        let messageCheck: any = await this.app.service('messages') // Confere se esta mensagem já ta no banco
          .find({
            query: {
              $limit: 0, contactId: contact._id, connectionId: conn._id,
              'key.id': messageId
            }
          }) as any;

        contactsId.push(ticketContactId);
        const ticketsFind: any = { paginate: false, query: { contactId: ticketContactId, status: { $in: ['pre_service', 'waiting', 'in_progress', 'closing'] }, $limit: 1 } };
        if (messageCheck.total) { // Se a mensagem já existe no banco, a query do ticket é atualizada
          messageCheck = true;
          const target = await this.app.service('messages')
            .find({
              query: {
                $limit: 1, contactId: contact._id, connectionId: conn._id,
                'key.id': messageId
              }
            }) as any;
          delete ticketsFind.query.status;
          ticketsFind.query._id = target.data[0].ticketId;
        } else {
          messageCheck = false; // usado para atualizar o ticket
        }

        const tickets: any = await this.app.service('tickets').find(ticketsFind);
        let ticket = tickets[0];
        if (!ticket) {
          if (!messageCheck && !target.message) continue;
          const status = conn.activeChatbot ? 'pre_service' : 'waiting';
          const payload = {
            jid,
            connectionId: conn._id,
            contactId: ticketContactId,
            userId: conn.userId,
            lastMessage: target,
            messageTimestamp: target.messageTimestamp,
            contact: ticketContact,
            count: 1,
            status
          };

          ticket = await this.app.service('tickets').create(payload);
        } else {
          if (!messageCheck) {
            let count = ticket.count + 1;
            let status = ticket.status;
            if (target.key.fromMe) {
              const checkIfChatBot: any = await this.app.service('messages').find({
                query: {
                  $limit: 0,
                  'key.id': messageId,
                  connectionId: conn._id,
                  chatbotId: { $ne: undefined }
                }
              });
              if (checkIfChatBot.total) count = ticket.count;
              else {
                count = 0;
                if (ticket.status === 'closing') {
                  if (ticket.attendantId) status = 'in_progress';
                  else status = 'waiting';
                }
              }
            }

            if (ticket.status === 'closing' && !target.key.fromMe) {
              if (ticket.attendantId) status = 'in_progress';
              else status = 'waiting';
            }

            const data: any = { count, messageTimestamp: target.messageTimestamp };
            if (target.message) {
              data.lastMessage = target;
            }

            if (!data.lastMessage.message) {
              delete data.lastMessage.message;
            }

            if (status !== ticket.status) data.status = status;
            this.app.service('tickets').patch(ticket._id, data);
          }
        }

        target.userId = conn.userId;
        target.contactId = contact._id;
        target.connectionId = conn._id;
        target.ticketId = ticket._id;
        const params = {
          query: {
            'key.id': messageId,
            connectionId: conn._id,
          },
          mongoose: { upsert: true }
        };

        if (target.changeKeyTo) {
          target.key = target.changeKeyTo;
          delete target.changeKeyTo;
        }

        if (conn.useMultidevice) {
          const messages: any = await this.app.service('messages') // Confere se esta mensagem já ta no banco
            .find({
              paginate: false,
              query: {
                $limit: 1, userId: conn.userId, contactId: contact._id, connectionId: conn._id,
                'key.id': messageId
              }
            });
          const message = messages[0];

          if (message) target.key.fromMe = message.key.fromMe;
          else if (!target.message && !target.messageStubType) continue;
        }
        promises.push(this.app.service('messages').patch(null, target, params));

      }

      await Promise.all(promises);

      this.app.service('member-tickets/merge').patch(null, contactsId);

      return null;
    } catch (error) {
      console.error('messages external patch error', error);
      return null;
    }
  }

  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    delete params.provider;
    return this.app.service('messages').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
