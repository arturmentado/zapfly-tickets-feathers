// Initializes the `messages-external` service on path `/messages-external`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MessagesExternal } from './messages-external.class';
import hooks from './messages-external.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'messages-external': MessagesExternal & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/messages-external', new MessagesExternal(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('messages-external');


  service.hooks(hooks);
}
