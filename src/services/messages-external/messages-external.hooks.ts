import authenticateExternal from '../../lib/authenticateExternal';
import validate from '../../lib/ajv';
import { createValidationSchema as messagesCreateValidationSchema } from '../../models/messages.model';
const createValidationSchema = JSON.parse(JSON.stringify(messagesCreateValidationSchema));
createValidationSchema.required = ['messageTimestamp'];

export default {
  before: {
    all: [authenticateExternal],
    find: [],
    get: [],
    create: [validate(createValidationSchema)],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
