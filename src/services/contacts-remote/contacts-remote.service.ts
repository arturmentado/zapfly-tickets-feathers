// Initializes the `contacts-remote` service on path `/contacts-remote`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ContactsRemote } from './contacts-remote.class';
import hooks from './contacts-remote.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'contacts-remote': ContactsRemote & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/contacts-remote', new ContactsRemote(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('contacts-remote');

  service.hooks(hooks);
}
