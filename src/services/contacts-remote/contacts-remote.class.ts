import { NotFound } from '@feathersjs/errors';
import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import axios from 'axios';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class ContactsRemote implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  baseUrlMd: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.baseUrl = app.get('whatsappBase');
    this.baseUrlMd = app.get('whatsappBaseMd');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    if (!params?.query?.id) throw new Error('id missing');
    const target: any = await this.app.service('member-connections').get(params.query.id, params);
    if (!target) throw new NotFound();
    if (target.useMultidevice) throw new Error('Using multi devices');
    const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/contacts`;
    try {
      const { data } = await axios.get(url);
      // data = Object.values(data);
      const promises = [];
      for (const key in data) {
        if (Object.prototype.hasOwnProperty.call(data, key)) {
          const item = data[key];
          item.connectionId = target._id;
          const params = {
            query: {
              jid: item.jid
            },
            mongoose: { upsert: true }
          };
          promises.push(this.app.service('contacts').patch(null, item, params));
        }
      }

      await Promise.all(promises);
      return data;
    } catch (error) {
      console.error(error);
      throw error;
    }

  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: string, params?: Params): Promise<any> {
    if (process.env.TESTING) return { exists: true, jid: `${id}@s.whatsapp.net` };
    if (!params?.query?.id) throw new Error('id missing');
    const target: any = await this.app.service('member-connections').get(params.query.id, params);
    if (!target) throw new NotFound();

    let url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/on-whatsapp/${id}`;
    if (params?.query?.contact) url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/contacts/${id}`;

    try {
      const { data } = await axios.get(url);
      return data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: Id, data: Data, params?: Params): Promise<Data> {
    if (!params?.query?.id) throw new Error('id missing');
    const target: any = await this.app.service('member-connections').get(params.query.id, params);
    if (!target) throw new NotFound();
    if (target.useMultidevice) throw new Error('Using multi devices');
    const url = `${this.baseUrl}${target.port}/contacts/${id}`;
    try {
      const { data } = await axios.get(url);
      return data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
