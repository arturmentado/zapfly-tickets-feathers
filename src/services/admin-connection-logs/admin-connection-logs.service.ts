// Initializes the `admin-connection-logs` service on path `/admin-connection-logs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { AdminConnectionLogs } from './admin-connection-logs.class';
import hooks from './admin-connection-logs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin-connection-logs': AdminConnectionLogs & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-connection-logs', new AdminConnectionLogs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-connection-logs');

  service.hooks(hooks);
}
