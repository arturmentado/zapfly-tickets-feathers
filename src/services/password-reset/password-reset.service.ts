// Initializes the `password-reset` service on path `/password-reset`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PasswordReset } from './password-reset.class';
import hooks from './password-reset.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'password-reset': PasswordReset & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/password-reset', new PasswordReset(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('password-reset');

  service.hooks(hooks);
}
