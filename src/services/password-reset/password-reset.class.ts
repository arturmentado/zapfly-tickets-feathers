import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import Crypto from 'crypto';
import { NotFound } from '@feathersjs/errors';

interface Data { }

interface iCreateData {
  email: string
}

interface ServiceOptions { }

export class PasswordReset implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: any, params?: Params): Promise<Data | Data[]> {
    const target = await this.app.service('users').find({ paginate: false, query: { passwordResetToken: data.token } }) as Array<any>;
    if (!target || !target.length) return new NotFound();
    const user = target[0];

    return this.app.service('users').patch(user._id, { passwordResetToken: null, password: data.password });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<any> {
    const target = await this.app.service('users').find({ query: { passwordResetToken: id }, paginate: false }) as Array<any>;
    if (!target || !target.length) return new NotFound();
    const user = target[0];
    return { token: user.token };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: iCreateData, params?: Params): Promise<Data> {
    const target = await this.app.service('users').find({ paginate: false, query: { email: data.email } }) as Array<any>;
    if (!target || !target.length) return {};
    const user = target[0];

    const env = await this.app.service('envs').get('zapfly');

    const token = await new Promise((resolve) => {
      Crypto.randomBytes(32, function (ex, buf) {
        resolve(buf.toString('hex'));
      });
    });

    // eslint-disable-next-line semi
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const createEmail = require('../../lib/createEmailTemplate').default;

    let sendMessage = true;
    await this.app.service('users').patch(user._id, { passwordResetToken: token });
    const connections: any = await this.app.service('connections').find({ query: { _id: { $in: user.connsId }, 'meta.validatedJid': true, $select: ['_id', 'meta', 'jid'] } });
    let connectionId, jid;
    if (connections?.data[0]?._id) {
      connectionId = connections.data[0]._id;
      jid = `${connections.data[0].jid}@s.whatsapp.net`;
    } else {
      sendMessage = false;
    }

    const url = `${env.frontendUrl}/password-reset?token=${token}`;
    const content = `Olá, ${user.name}<br><br><a href="${url}">Clique aqui link para resetar sua senha</a>`;
    const html = createEmail({ logo: 'https://atd.zapfly.com.br/email.png', content });
    const email = {
      'to': user.email,
      'subject': 'Sobre sua conta da ZapFly',
      html
    };

    const message = `Olá, o atendente ${user.name} esqueceu sua conta para a plataforma, estamos enviando um link para o email e para seu numero cadastrado. \n\nPara adicionar uma nova senha para a conta de email: *${user.email}* clique neste link: ${url}`;
    const id = `3EB0${Crypto.randomBytes(4).toString('hex').toUpperCase()}`;

    if (sendMessage) {
      const messageSend: any = {
        id: jid,
        messageId: id,
        connectionId: String(this.app.get('mainConnectionId')),
        message: message,
        type: 'conversation'
      };
      await this.app.service('messages-remote').create(messageSend, {});

    }
    this.app.service('mailer').create(email);

    return { success: true };
  }
}
