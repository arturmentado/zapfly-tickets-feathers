// Initializes the `connection-logs` service on path `/connection-logs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ConnectionLogs } from './connection-logs.class';
import createModel from '../../models/connection-logs.model';
import hooks from './connection-logs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'connection-logs': ConnectionLogs & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    multi: ['patch', 'remove'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/connection-logs', new ConnectionLogs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('connection-logs');

  service.hooks(hooks);
}
