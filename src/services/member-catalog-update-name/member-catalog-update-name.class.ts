import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberCatalogUpdateName implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  [key: string]: any;
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: Id, data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };
    const catalog = await this.app.service('catalogs').get(id, {});
    await this.app.service('catalogs').patch(null, { name: data.name }, { query: { name: id } });
    await this.app.service('catalog-categories').patch(null, { catalogName: data.name, catalogId: catalog._id }, { query: { catalogName: id } });
    await this.app.service('catalog-products').patch(null, { catalogName: data.name, catalogId: catalog._id }, { query: { catalogName: id } });
    return data;
  }
}
