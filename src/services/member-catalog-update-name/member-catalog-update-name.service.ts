// Initializes the `member-catalog-update-name` service on path `/member-catalog-update-name`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberCatalogUpdateName } from './member-catalog-update-name.class';
import hooks from './member-catalog-update-name.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-catalog-update-name': MemberCatalogUpdateName & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-catalog-update-name', new MemberCatalogUpdateName(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-catalog-update-name');

  service.hooks(hooks);
}
