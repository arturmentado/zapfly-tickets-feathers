import { disallow } from 'feathers-hooks-common';
import { createValidationSchema as userSchema } from '../../models/users.model';
import { createValidationSchema as connectionSchema } from '../../models/connections.model';
import validate from '../../lib/ajv';
const createValidationSchema = {
  user: userSchema,
  connection: connectionSchema,
};

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [validate(createValidationSchema)],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
