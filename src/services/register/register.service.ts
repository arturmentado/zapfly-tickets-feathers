// Initializes the `register` service on path `/register`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Register } from './register.class';
import hooks from './register.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'register': Register & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/register', new Register(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('register');

  service.hooks(hooks);
}
