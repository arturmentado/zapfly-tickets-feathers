import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class Register implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  [key: string]: any;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: Params): Promise<any> {
    const checkUser = await this.app.service('users').find({ query: { email: data.user.email, $limit: 0 } }) as any;
    if (checkUser.total) throw new Error('User already registered');

    const checkConnection = await this.app.service('connections').find({ query: { jid: data.connection.jid, $limit: 0 } }) as any;
    if (checkConnection.total) throw new Error('User already registered');
    try {
      data.user.permissions = ['admin'];
      data.user.useOnboarding = true;
      let user = await this.app.service('users').create(data.user);
      await this.app.service('users').patch(user._id, { userId: user._id });
      data.connection.userId = user._id.toString();
      data.connection.preapprovalPlan = {
        code: null,
        status: 'TRIAL'
      };
      let connection = await this.app.service('connections').create(data.connection);

      user = await this.app.service('users').get(user._id);
      connection = await this.app.service('connections').get(connection._id, {});

      // eslint-disable-next-line semi
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const createEmail = require('../../lib/createEmailTemplate').default;
      const content = `Olá, ${user.name}<br><br>Ficamos muito felizes de receber seu cadastro!<br>Caso tenha qualquer problema lembre-se de mandar uma mensagem no zap para a gente, ou qualquer duvida! Estamos no numero: (16) 99157-8481<br>`;
      const html = createEmail({ logo: 'https://atd.zapfly.com.br/email.png', content });
      const email = {
        'to': user.email,
        'subject': 'Recuperação de senha da sua conta da ZapFly',
        html
      };

      this.app.service('mailer').create(email);

      return { user, connection };
    } catch (error) {
      console.error(error);
      throw error;

    }
  }
}
