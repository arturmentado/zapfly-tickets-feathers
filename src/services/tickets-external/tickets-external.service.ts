// Initializes the `tickets-external` service on path `/tickets-external`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { TicketsExternal } from './tickets-external.class';
import hooks from './tickets-external.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'tickets-external': TicketsExternal & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/tickets-external', new TicketsExternal(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('tickets-external');

  service.hooks(hooks);
}
