// Initializes the `QuickMessages` service on path `/quick-messages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { QuickMessages } from './quick-messages.class';
import createModel from '../../models/quick-messages.model';
import hooks from './quick-messages.hooks';
import { createUpload, middlewareUpload } from '../../lib/s3Middleware';

// Add this service to the service type index

declare module '../../declarations' {
  interface ServiceTypes {
    'quick-messages': QuickMessages & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const upload = createUpload(app);

  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use(
    '/quick-messages',
    upload.array('files'),
    middlewareUpload,
    new QuickMessages(options, app)
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('quick-messages');

  service.hooks(hooks);
}
