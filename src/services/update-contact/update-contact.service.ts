// Initializes the `update-contact` service on path `/update-contact`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { UpdateContact } from './update-contact.class';
import hooks from './update-contact.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'update-contact': UpdateContact & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/update-contact', new UpdateContact(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('update-contact');

  service.hooks(hooks);
}
