import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { NotFound } from '@feathersjs/errors';
import axios from 'axios';

interface Data { }

interface ServiceOptions { }

export class UpdateContact implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  baseUrlMd: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.baseUrl = app.get('whatsappBase');
    this.baseUrlMd = app.get('whatsappBaseMd');
    this.app = app;
  }

  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: string, params?: Params): Promise<Data> {
    if (!params?.query?.id) throw new Error('id missing');
    const target: any = await this.app.service('member-connections').get(params.query.id, params);
    if (!target) throw new NotFound();
    const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/contacts/${id}`;

    try {
      const { data } = await axios.get(url);
      this.app.service('contacts').patch(null, data, { query: { jid: id, connectionId: target._id } });
      return data;
    } catch (error) {
      // console.error(error);
      throw error;
    }
  }
}
