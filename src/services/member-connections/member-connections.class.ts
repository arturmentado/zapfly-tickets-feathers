import { NotFound } from '@feathersjs/errors';
import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberConnections implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    let internalParams: any = params;
    if (!internalParams) internalParams = { query: {} };
    internalParams.query._id = params?.user?.connsId;

    return this.app.service('connections').find(internalParams);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<any> {
    if (!params.user) return {};
    const conn = await this.app.service('connections').get(id, {});
    const userConns = JSON.parse(JSON.stringify(params.user?.connsId));
    if (!userConns.includes(String(conn._id))) throw new NotFound();
    return conn;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: any): Promise<Data> {
    const user = params.user;
    data.userId = user._id;
    const connsId = user.connsId;

    await this.app.service('connections').create(data);
    const conn = await this.app.service('connections').get(data.jid, {});
    connsId.push(conn._id);

    await this.app.service('users').patch(null, { connsId }, { query: { userId: user._id } });

    return conn;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    let internalParams: any = params;
    if (!internalParams) internalParams = { query: {} };
    internalParams.query._id = params?.user?.connsId;

    return this.app.service('connections').patch(id, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
