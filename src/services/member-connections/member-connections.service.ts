// Initializes the `member-connections` service on path `/member-connections`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberConnections } from './member-connections.class';
import hooks from './member-connections.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-connections': MemberConnections & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-connections', new MemberConnections(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-connections');

  service.hooks(hooks);
}
