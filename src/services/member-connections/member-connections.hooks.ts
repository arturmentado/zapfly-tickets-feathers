import * as authentication from '@feathersjs/authentication';
import * as local from '@feathersjs/authentication-local';
import validate from '../../lib/ajv';
import { createValidationSchema as connectionsCreateValidationSchema } from '../../models/connections.model';
import checkPermissions from 'feathers-permissions';
// Don't remove this comment. It's needed to format import lines nicely.

const createValidationSchema = JSON.parse(JSON.stringify(connectionsCreateValidationSchema));
createValidationSchema.required = createValidationSchema.required.filter((e: any) => e !== 'userId');

const { authenticate } = authentication.hooks;
const { protect } = local.hooks;

export default {
  before: {
    all: [authenticate('jwt')],
    find: [checkPermissions({ roles: ['admin', 'attendant'] })],
    get: [checkPermissions({ roles: ['admin', 'attendant'] })],
    create: [checkPermissions({ roles: ['admin'] }), validate(createValidationSchema)],
    update: [checkPermissions({ roles: ['admin'] })],
    patch: [checkPermissions({ roles: ['admin'] })],
    remove: [checkPermissions({ roles: ['admin'] })]
  },

  after: {
    all: [protect('sessionData')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
