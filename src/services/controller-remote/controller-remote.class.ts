import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import axios from 'axios';
interface Data { }

interface ServiceOptions { }

export class ControllerRemote implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  baseUrlMd: string;
  remoteAddress: string;
  dockerEnv: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.baseUrl = app.get('whatsappConnController');
    this.baseUrlMd = app.get('whatsappConnControllerMd');
    this.dockerEnv = app.get('dockerEnv');
    this.remoteAddress = `${app.get('domain')}`;
  }
  [key: string]: any;
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: any, params?: Params): Promise<Data> {
    if (process.env.TESTING) return {};
    const url = `${data.useMultidevice ? this.baseUrlMd : this.baseUrl}/start/${id}`;

    let setting: any;
    if (data.useMultidevice) {
      setting = await this.app.service('settings')
        .find({ query: { $limit: 1, name: 'baileyVersionMd', type: 'system' } });
    } else {
      setting = await this.app.service('settings')
        .find({ query: { $limit: 1, name: 'baileyVersion', type: 'system' } });
    }

    const connVersion = setting[0] ? setting[0].meta.version : [2, 2119, 6];
    data.port = data.useMultidevice && this.dockerEnv === 'true' ? String(parseInt(data.port) - 1000) : data.port;

    const options = {
      params: {
        remoteAddress: this.remoteAddress,
        sessionData: data.sessionData ?? '',
        waitChats: !data.disableWaitChats ?? true,
        disableAutoImportUnreadMessages: data.disableAutoImportUnreadMessages ?? false,
        disableForceUseHere: data.disableForceUseHere ?? false,
        apiSubname: 'ZapFly',
        apiName: 'Chrome',
        apiVersion: '89',
        port: data.port,
        connVersion: JSON.stringify(connVersion)
      }
    };
    try {
      const response = await axios.get(url, options);
      const port = data.useMultidevice && this.dockerEnv === 'true' ? String(parseInt(data.port) - 1000) : response.data.port;
      await this.app.service('connection-external').patch(null, { sessionId: id, port }, {});
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: Id, params: Params): Promise<Data> {
    const url = `${params.useMultidevice ? this.baseUrlMd : this.baseUrl}/delete/${id}`;
    try {
      const response = await axios.get(url);
      await this.app.service('connection-external').patch(null, { sessionId: id, port: null, state: 'disconnected' }, {});
      return response.data;
    } catch (error) {
      console.error(error.toJSON());
      throw error;
    }
  }
}
