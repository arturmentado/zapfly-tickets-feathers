// Initializes the `member-tickets-merge` service on path `/member-tickets/merge`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberTicketsMerge } from './member-tickets-merge.class';
import hooks from './member-tickets-merge.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-tickets/merge': MemberTicketsMerge & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-tickets/merge', new MemberTicketsMerge(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-tickets/merge');

  service.hooks(hooks);
}
