import { MethodNotAllowed, NotAcceptable, NotFound } from '@feathersjs/errors';
import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberTickets implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<any[] | Paginated<any>> {
    const connsId = params.user?.connsId;
    const query = params.query || {};

    params.query = {
      ...query,
      connectionId: {
        $in: connsId
      }
    };

    delete params.provider;
    return this.app.service('tickets').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    const user: any = params?.user;
    const internalParams: any = params;
    if (!internalParams?.query) internalParams.query = {};

    internalParams.connectionId = { $in: user.connsId };
    delete internalParams.provider;

    return this.app.service('tickets').get(id, internalParams);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<any> {
    const connsId: any = JSON.parse(JSON.stringify(params?.user?.connsId));
    if (!connsId.includes(data.connectionId)) throw new NotFound();

    const user: any = params?.user;
    data.userId = user.userId;
    data.messageTimestamp = new Date().getTime() / 1000;

    const contact = await this.app.service('member-contacts').get(data.contactId, params);

    data.contact = contact;

    const ticketsFind: Params = { paginate: false, query: { contactId: data.contactId, status: { $in: ['waiting', 'open'] }, $limit: 1 } };
    const tickets: any = await this.app.service('tickets').find(ticketsFind);
    if (tickets.length) throw new NotAcceptable('already open');
    delete params.provider;
    return this.app.service('tickets').create(data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    const user: any = params?.user;

    if (!user.permissions.includes('admin') && !user.permissions.includes('supervisor')) {
      if (Object.keys(data).some(e => !['status', 'count'].includes(e))) {
        const settings = await this.app.service('settings').find({ query: { userId: user.userId, name: 'allSetUserId', value: true } }) as any;
        if (!settings.length) throw new MethodNotAllowed('settings not allow this');
      }
    }

    const internalParams: any = params;
    if (!internalParams?.query) internalParams.query = {};

    internalParams.query.connectionId = { $in: user.connsId };
    delete internalParams.provider;

    return this.app.service('tickets').patch(id, data, internalParams);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
