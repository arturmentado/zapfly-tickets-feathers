// Initializes the `member-tickets` service on path `/member-tickets`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberTickets } from './member-tickets.class';
import hooks from './member-tickets.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-tickets': MemberTickets & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate'),
    whitelist: ['$populate', '$text', '$search', '$regex'],
  };

  // Initialize our service with any options it requires
  app.use('/member-tickets', new MemberTickets(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-tickets');

  service.hooks(hooks);
}
