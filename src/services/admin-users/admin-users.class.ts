import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class AdminUsers implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    delete params.provider;
    return this.app.service('users').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    delete params.provider;
    return this.app.service('users').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params: Params): Promise<Data> {
    delete params.provider;
    return this.app.service('users').create(data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params: Params): Promise<Data> {
    delete params.provider;
    return this.app.service('users').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
