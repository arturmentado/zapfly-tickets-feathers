// Initializes the `admin-users` service on path `/admin-users`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { AdminUsers } from './admin-users.class';
import hooks from './admin-users.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin-users': AdminUsers & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate'),
    whitelist: ['$populate', '$text', '$search', '$regex', '$options'],
  };

  // Initialize our service with any options it requires
  app.use('/admin-users', new AdminUsers(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-users');

  service.hooks(hooks);
}
