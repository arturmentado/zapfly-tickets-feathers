// Initializes the `whatsapp-chats` service on path `/whatsapp-chats`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { WhatsappChats } from './whatsapp-chats.class';
import hooks from './whatsapp-chats.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'whatsapp-chats': WhatsappChats & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/whatsapp-chats', new WhatsappChats(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('whatsapp-chats');

  service.hooks(hooks);
}
