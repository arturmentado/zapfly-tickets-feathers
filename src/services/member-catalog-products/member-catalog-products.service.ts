// Initializes the `member-catalog-products` service on path `/member-catalog-products`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberCatalogProducts } from './member-catalog-products.class';
import hooks from './member-catalog-products.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-catalog-products': MemberCatalogProducts & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-catalog-products', new MemberCatalogProducts(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-catalog-products');

  service.hooks(hooks);
}
