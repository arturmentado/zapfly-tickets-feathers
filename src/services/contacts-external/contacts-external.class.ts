import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { isEqual } from 'lodash';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class ContactsExternal implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: any, params: Params): Promise<Data> {
    try {
      const sessionId = params.headers?.session_id;
      const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });

      const promises = [];
      for (const target of data) {

        const params = {
          query: {
            jid: target.jid,
            connectionId: conn._id,
          },

        };

        target.connectionId = conn._id;

        const contacts = await this.app.service('contacts').find({ ...params, paginate: false }) as any;

        let shouldPatch = false;
        for (const key in target) {
          if (Object.prototype.hasOwnProperty.call(target, key)) {
            const item = target[key];

            if (!contacts[0][key]) continue;
            if (isEqual(contacts[0][key], item)) continue;
            shouldPatch = true;
          }
        }
        if (!shouldPatch) continue;

        promises.push(this.app.service('contacts').patch(null, target, { ...params, mongoose: { upsert: true } }));
      }

      await Promise.all(promises);
    } catch (error) {
      console.error(error);
    }
    return {};
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
