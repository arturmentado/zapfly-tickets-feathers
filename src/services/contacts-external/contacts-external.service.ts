// Initializes the `contacts-external` service on path `/contacts-external`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ContactsExternal } from './contacts-external.class';
import hooks from './contacts-external.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'contacts-external': ContactsExternal & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/contacts-external', new ContactsExternal(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('contacts-external');

  service.hooks(hooks);
}
