import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { NotFound } from '@feathersjs/errors';
import axios from 'axios';

interface Data { }

interface ServiceOptions { }

export class ConnectionsRemoteVersion implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions; baseUrl: string;
  baseUrlMd: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.baseUrl = app.get('whatsappBase');
    this.baseUrlMd = app.get('whatsappBaseMd');
  }
  [key: string]: any;
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const target: any = await this.app.service('member-connections').get(id, params);
    if (!target) throw new NotFound();

    const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/connection-version`;
    try {
      const { data } = await axios.get(url);
      return data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}
