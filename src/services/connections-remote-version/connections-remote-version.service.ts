// Initializes the `connections-remote-version` service on path `/connections-remote-version`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ConnectionsRemoteVersion } from './connections-remote-version.class';
import hooks from './connections-remote-version.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'connections-remote-version': ConnectionsRemoteVersion & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/connections-remote-version', new ConnectionsRemoteVersion(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('connections-remote-version');

  service.hooks(hooks);
}
