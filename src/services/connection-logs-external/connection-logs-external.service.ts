// Initializes the `connection-logs-external` service on path `/connection-logs-external`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ConnectionLogsExternal } from './connection-logs-external.class';
import hooks from './connection-logs-external.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'connection-logs-external': ConnectionLogsExternal & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    events: ['connection-logs'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/connection-logs-external', new ConnectionLogsExternal(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('connection-logs-external');

  service.hooks(hooks);
}
