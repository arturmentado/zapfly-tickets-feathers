import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class ConnectionLogsExternal implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: any, params: Params): Promise<any> {
    const sessionId = params.headers?.session_id;
    const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });

    const payload = {
      connectionId: conn._id,
      jid: sessionId,
      text: data.text
    };

    // delete params.provider;

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.emit('connection-logs', payload);

    return payload;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // async patch(id: NullableId, data: any, params: Params): Promise<any> {
  //   const sessionId = params.headers?.session_id;
  //   const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });

  //   const payload = {
  //     connectionId: conn._id,
  //     jid: sessionId,
  //     text: data.text
  //   };

  //   delete params.provider;

  //   this.app.service('connection-logs').create(payload, params);
  //   this.app.service('connection-logs').remove(null, { query: { createdAt: { $lt: new Date(new Date().setHours(0, 0, 0, 0)) } } });
  //   return null;
  // }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
