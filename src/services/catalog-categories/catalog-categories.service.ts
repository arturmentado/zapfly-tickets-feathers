// Initializes the `catalog-categories` service on path `/catalog-categories`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { CatalogCategories } from './catalog-categories.class';
import createModel from '../../models/catalog-categories.model';
import hooks from './catalog-categories.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'catalog-categories': CatalogCategories & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    multi: ['patch'],
    Model: createModel(app),
    paginate: {
      'default': 200,
      'max': 200
    },
  };

  // Initialize our service with any options it requires
  app.use('/catalog-categories', new CatalogCategories(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('catalog-categories');

  service.hooks(hooks);
}
