import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class UserQueueAvailable implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const user = params.user;
    const userId = user?.userId;
    let set = await this.app.service('settings').find({ paginate: false, query: { userId, value: true, name: 'useQueue' } }) as any;

    if (!set.length) throw new Error('Not defined');
    set = set[0];

    if (!set.meta?.queueOrder) set.meta = { queueOrder: [] };

    const check = !set.meta.queueOrder.some((e: any) => e.equals(user?._id));

    if (check) {
      set.meta.queueOrder.push(user?._id);
      await this.app.service('users').patch(user?._id, { available: true });
      // .then(console.log);
    } else {
      set.meta.queueOrder = set.meta.queueOrder.filter((e: any) => !e.equals(user?._id));
      await this.app.service('users').patch(user?._id, { available: false });
      // .then(console.log);
    }

    return this.app.service('settings').patch(set._id, { meta: set.meta });
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
}
