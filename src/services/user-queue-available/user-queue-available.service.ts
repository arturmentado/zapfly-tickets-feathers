// Initializes the `user-queue-available` service on path `/user/queue-available`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { UserQueueAvailable } from './user-queue-available.class';
import hooks from './user-queue-available.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'user/queue-available': UserQueueAvailable & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/user/queue-available', new UserQueueAvailable(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('user/queue-available');

  service.hooks(hooks);
}
