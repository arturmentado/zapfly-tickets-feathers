import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { NotFound } from '@feathersjs/errors';
import axios from 'axios';

interface Data { }

interface ServiceOptions { }

export class PagseguroPreapprovals implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  email: string;
  token: string;
  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.email = this.app.get('pagseguroEmail');
    this.token = this.app.get('pagseguroToken');
    this.baseUrl = `https://ws${this.app.get('pagseguroEnv')}pagseguro.uol.com.br`;
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<any> {
    const { connection, payload } = data;
    const check = await this.app.service('member-connections').get(connection._id, params);
    if (!check) throw new NotFound();

    const planSetting = await this.app.service('settings').find({ paginate: false, query: { type: 'system', value: true, name: 'pagseguroPlan' } }) as any;
    payload.plan = planSetting[0].meta.code;
    payload.reference = connection._id;

    const url = `${this.baseUrl}/pre-approvals?email=${this.email}&token=${this.token}`;

    try {
      const res = await axios.post(url, payload, { headers: { Accept: 'application/vnd.pagseguro.com.br.v1+json;charset=ISO-8859-1' } });
      await this.app.service('connections').patch(connection._id, { 'preapprovalPlan': { 'code': res.data.code, status: 'created' } });
      return { success: true };
    } catch (error) {
      if (error.isAxiosError) {
        console.error(error.response.data);
      } else console.error(error);
      throw new Error('problem');
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: any, params: Params): Promise<any> {
    const { connection, payload } = data;
    const check = await this.app.service('member-connections').get(connection._id, params) as any;
    if (!check) throw new NotFound();

    const url = `${this.baseUrl}/pre-approvals/${check.preapprovalPlan.code}/payment-method?email=${this.email}&token=${this.token}`;

    try {
      const res = await axios.put(url, payload, { headers: { Accept: 'application/vnd.pagseguro.com.br.v1+json;charset=ISO-8859-1' } });
      await this.app.service('connections').patch(connection._id, { 'preapprovalPlan': { status: 'created' } });
      return { success: true };
    } catch (error) {
      if (error.isAxiosError) {
        console.error(error.response.data);
      } else console.error(error);
      throw new Error('problem');
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: Id, params: Params): Promise<Data> {
    const check = await this.app.service('member-connections').get(id, params) as any;
    if (!check) throw new NotFound();
    if (!check.preapprovalPlan) throw new NotFound();
    if (check.preapprovalPlan.status === 'CANCELLED') throw new NotFound();

    const url = `${this.baseUrl}/pre-approvals/${check.preapprovalPlan.code}/cancel?email=${this.email}&token=${this.token}`;

    try {
      await axios.put(url, {}, { headers: { Accept: 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1' } });
      await this.app.service('connections').patch(check._id, { 'preapprovalPlan.status': 'CANCELLED' });
      return { success: true };
    } catch (error) {
      if (error.isAxiosError) {
        console.error(error.response.data);
      } else console.error(error);
      throw new Error('problem');
    }

  }
}
