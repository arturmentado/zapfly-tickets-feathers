// Initializes the `pagseguro-preapprovals` service on path `/pagseguro-preapprovals`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PagseguroPreapprovals } from './pagseguro-preapprovals.class';
import hooks from './pagseguro-preapprovals.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'pagseguro-preapprovals': PagseguroPreapprovals & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/pagseguro-preapprovals', new PagseguroPreapprovals(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('pagseguro-preapprovals');

  service.hooks(hooks);
}
