// Initializes the `connections-validate` service on path `/connections-validate`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ConnectionsValidate } from './connections-validate.class';
import hooks from './connections-validate.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'connections-validate': ConnectionsValidate & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/connections-validate', new ConnectionsValidate(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('connections-validate');

  service.hooks(hooks);
}
