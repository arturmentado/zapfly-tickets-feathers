import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import axios from 'axios';

interface Data { }

interface ServiceOptions { }

export class ConnectionsValidate implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  baseUrl: string;
  baseUrlMd: string;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.baseUrl = app.get('whatsappBase');
    this.baseUrlMd = app.get('whatsappBaseMd');
  }
  [key: string]: any;
  create(data: Partial<Data> | Partial<Data>[], params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: string, params?: Params): Promise<Data> {
    if (process.env.TESTING) return {};
    let connection = await this.app.service('connections').get(id, { query: { notId: true } });
    const url = `${connection.useMultidevice ? this.baseUrlMd : this.baseUrl}${connection.port}/me`;

    const res = await axios.get(url)
      .catch((error) => {
        console.error(error);
        throw error;
      });

    const jid = res.data.user.jid.split('@')[0];

    const connections: Paginated<any> = await this.app.service('connections').find({ query: { $limit: 1, jid, _id: { $ne: connection._id } } }) as Paginated<any>;

    if (connections.total) {
      const random = Math.floor(Math.random() * 90000) + 10000;
      const target = connections.data[0];

      if (target.meta?.validatedJid) {
        await this.app.service('connections').patch(connection._id, { status: 'REMOVED', qrcode: null, sessionData: null });
        await this.app.service('controller-remote').remove(connection.jid, { useMultidevice: target.useMultidevice });

        const tooltipPayload = {
          active: true,
          type: 'emergency',
          forWho: 'specific',
          notificationTitle: 'Atenção!',
          notificationText: 'O celular que escaneou o QR code já está vinculado a outra conta e não pode ser conectado a  esta. Caso isto pareça um erro, entre em contato com o suporte',
          connectionsId: [connection._id],
        };

        await this.app.service('tooltips').create(tooltipPayload);
        throw { error: 'invalid phone jid' };
      }

      await this.app.service('connections').patch(target._id, { jid: `${target.jid}-${random}`, status: 'REMOVED', qrcode: null, sessionData: null });
      await this.app.service('controller-remote').remove(target.jid, { useMultidevice: target.useMultidevice }).catch(() => { return null; });
    }

    if (!connection.meta?.validatedJid) {
      await this.app.service('controller-remote').remove(connection.jid, { useMultidevice: connection.useMultidevice }).catch(() => { return null; });
      connection = await this.app.service('connections').patch(connection._id, { 'meta.validatedJid': true, jid });
      await this.app.service('controller-remote')
        .update(connection.jid, {
          waitChats: !connection.disableWaitChats ?? true,
          sessionData: connection.useMultidevice ? true : connection.sessionData,
          disableAutoImportUnreadMessages: connection.disableAutoImportUnreadMessages ?? true,
          disableForceUseHere: connection.disableForceUseHere ?? true
        });
    }

    return {};
  }

}
