// Initializes the `member-settings` service on path `/member-settings`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberSettings } from './member-settings.class';
import hooks from './member-settings.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-settings': MemberSettings & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-settings', new MemberSettings(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-settings');

  service.hooks(hooks);
}
