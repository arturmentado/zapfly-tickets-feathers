import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberSettings implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const user = params.user;
    const userId = user?.userId;

    params.query = {
      ...params.query,
      userId,
      type: 'connections'
    };

    delete params.provider;
    return this.app.service('settings').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const user = params.user;
    const userId = user?.userId;

    params.query = {
      ...params.query,
      userId,
      type: 'connections'
    };

    delete params.provider;
    return this.app.service('settings').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const user = params.user;
    const userId = user?.userId;

    data.userId = userId;
    data.type = 'connections';

    const check = await this.app.service('settings').find({ paginate: false, query: { name: data.name, type: 'connection', userId } }) as any;
    if (check.total) throw new Error('already exists');

    delete params.provider;
    return this.app.service('settings').create(data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: any, params: Params): Promise<Data> {
    const user = params.user;
    const userId = user?.userId;

    params.query = {
      ...params.query,
      userId,
      type: 'connections'
    };

    delete data.userId;
    delete data.type;
    delete data.name;

    delete params.provider;
    return this.app.service('settings').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
