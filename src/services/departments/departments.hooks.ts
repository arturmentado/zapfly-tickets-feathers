import { disallow } from 'feathers-hooks-common';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

export default {
  before: {
    all: [
      disallow('external'),
    ],
    find: [
      search({ escape: false }),
      search({  // regex search on given fields
        fields: [
          'name'
        ]
      }),
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
