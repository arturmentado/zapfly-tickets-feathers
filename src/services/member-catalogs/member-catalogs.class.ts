import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberCatalogs implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  async find(params: Params): Promise<Data | Data[] | Paginated<Data>> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalogs').find(params);
  }

  async get(id: Id, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalogs').get(id, params);
  }

  async update(id: Id, data: any, params: Params): Promise<Data | Data[]> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('catalogs').update(id, data, params);
  }

  async patch(id: Id, data: any, params: Params): Promise<Data | Data[]> {
    const user: any = params?.user;
    delete params.provider;

    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('catalogs').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('catalogs').create(data, params);
  }

}
