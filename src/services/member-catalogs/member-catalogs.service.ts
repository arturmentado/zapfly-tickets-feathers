// Initializes the `member-catalogs` service on path `/member-catalogs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberCatalogs } from './member-catalogs.class';
import hooks from './member-catalogs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-catalogs': MemberCatalogs & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-catalogs', new MemberCatalogs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-catalogs');

  service.hooks(hooks);
}
