// Initializes the `member-contacts` service on path `/member-contacts`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberContacts } from './member-contacts.class';
import hooks from './member-contacts.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-contacts': MemberContacts & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    whitelist: ['$text', '$search', '$regex'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-contacts', new MemberContacts(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-contacts');

  service.hooks(hooks);
}
