import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberContacts implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const connsId = params.user?.connsId;
    const query = params.query || {};

    params.query = {
      ...query,
      connectionId: {
        $in: connsId
      }
    };
    delete params.provider;
    return this.app.service('contacts').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const connsId = params.user?.connsId;
    const query = params.query || {};

    params.query = {
      ...query,
      connectionId: {
        $in: connsId
      }
    };
    delete params.provider;
    return this.app.service('contacts').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const connsId: any = JSON.parse(JSON.stringify(params?.user?.connsId));
    if (!connsId.includes(data.connectionId)) throw new Error('');

    delete params.provider;

    return this.app.service('contacts').create(data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params: Params): Promise<Data> {
    const connsId = params.user?.connsId;
    const query = params.query || {};

    params.query = {
      $and: [
        query,
        {
          connectionId: {
            $in: connsId
          }
        }
      ]
    };
    delete params.provider;

    return this.app.service('contacts').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
