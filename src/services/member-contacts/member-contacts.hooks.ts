import validate from '../../lib/ajv';
import * as authentication from '@feathersjs/authentication';
import { createValidationSchema } from '../../models/contacts.model';
import checkPermissions from 'feathers-permissions';
// Don't remove this comment. It's needed to format import lines nicely.
const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [authenticate('jwt')],
    find: [checkPermissions({ roles: ['admin', 'attendant'] })],
    get: [checkPermissions({ roles: ['admin', 'attendant'] })],
    create: [checkPermissions({ roles: ['admin', 'attendant'] }), validate(createValidationSchema)],
    update: [checkPermissions({ roles: ['admin', 'attendant'] })],
    patch: [checkPermissions({ roles: ['admin', 'attendant'] })],
    remove: [checkPermissions({ roles: ['admin'] })]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
