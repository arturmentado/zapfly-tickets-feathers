// Initializes the `test-tools` service on path `/admin/test-tools`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { TestTools } from './test-tools.class';
import hooks from './test-tools.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin/test-tools': TestTools & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin/test-tools', new TestTools(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin/test-tools');

  service.hooks(hooks);
}
