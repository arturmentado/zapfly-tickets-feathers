import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class TestTools implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;
  functions: any;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;

    this.functions = functions;
  }

  [key: string]: any;
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  create(data: Partial<Data> | Partial<Data>[], params: Params): Promise<Data | Data[]> {
    const action: any = params.query?.action;
    if (!this.functions[action]) throw 'no action found';
    return this.functions[action](this.app, params.query, data);
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<any> {
    const action: any = params.query?.action;
    if (!this.functions[action]) throw 'no action found';
    return this.functions[action](this.app, params.query);
  }
}

const functions = {
  clear_database: async function (app: any) {
    console.log('CLEAR DATABASE');
    console.time('CLEAR DATABASE');

    const mongooseClient = app.get('mongooseClient');
    const models = mongooseClient.modelNames();
    const promises = [];

    for (let index = 0; index < models.length; index++) {
      const model = models[index];
      console.log('-> clearing', model);
      promises.push(mongooseClient.models[model].deleteMany({}));
    }

    await Promise.all(promises);
    await functions.basic_env(app);
    console.timeEnd('CLEAR DATABASE');
    return true;
  },

  basic_env: async function (app: any) {
    const actionName = 'Creating basic env';
    console.log(actionName);
    console.time(actionName);

    const data = {
      'name': 'zapfly',
      'formattedName': 'ZapFly Atd',
      'frontendUrl': 'http://localhost:8080',
      'metaLinks': {
        'favicon': {
          'rel': 'icon',
          'sizes': '16x16 32x32',
          'href': 'favicon.ico?v=1'
        }
      },
      'terms': {
        'active': true,
        'points': [
          {
            'points': [
              'O ChatFly&nbsp;é uma plataforma criada para empresas ou pessoas que realizam atendimento de clientes, fornecedores ou outros tipos de contatos e, para tanto, oferece a funcionalidade de utilização do WhatsApp e do Instagram (especificamente o\n            direct/mensagens do perfil) por múltiplos usuários/atendentes.',
              'Quanto à funcionalidade voltada à rede social Instagram, a ChatFly fornece um serviço          de acesso ao direct, para envio de mensagens através da Plataforma.',
              'Poderão ser aplicáveis a determinados serviços disposições adicionais (regras para uma determinada funcionalidade, atividade ou promoção), e tais disposições adicionais serão comunicadas ao Usuário através da Aplicação e/ou do Website e/ou e-mail e/ou SMS. As disposições adicionais complementam e serão consideradas parte dos Termos e Condições para efeitos dos serviços aplicáveis, após terem sido lidas e aceitas pelo Usuário. As disposições adicionais prevalecerão sobre estes Termos e Condições em caso de conflito relativo aos serviços aplicáveis.'
            ],
            'title': 'SERVIÇO'
          },
          {
            'points': [
              'A APLICAÇÃO NÃO ESTÁ DISPONÍVEL PARA MENORES DE 18 ANOS. Caso você seja menor de 18 anos, por favor, não utilize o Aplicativo e Site. Ao usar, você declara que tem capacidade jurídica plena para firmar contratos válidos sob a legislação aplicável.'
            ],
            'title': 'ELEGIBILIDADE'
          },
          {
            'points': [
              'O cadastro apenas será confirmado se você, Usuário, preencher todos os campos obrigatórios, com informações exatas, precisas e verdadeiras. Você declara e assume o compromisso de atualizar os dados inseridos em seu cadastro (“Dados Pessoais”) sempre que for necessário.',
              'O registro de uma Conta exige o fornecimento de poucos Dados Pessoais à ChatFly, tais como: nome, e-mail e número de telefone.',
              '<u>\n          O Usuário garante e responde, em qualquer caso, civil e criminalmente, pela veracidade, exatidão e autenticidade, dos Dados Pessoais cadastrados.\n        </u>',
              'A ChatFly poderá solicitar dados adicionais, bem como cópias de documentos para conferir a veracidade, exatidão e autenticidade dos Dados Pessoais.',
              'A ChatFly se reserva no direito de recusar qualquer solicitação de cadastro e de suspender um cadastro previamente aceito, que esteja ou venham a ficar em desacordo com as políticas e regras dos presentes Termos e Condições de Uso.',
              'A ChatFly, em razão de violação à legislação em vigor ou aos Termos e Condições de Uso, conforme cada situação, poderá, sem          prejuízo de outras medidas, recusar qualquer solicitação de cadastro, advertir, suspender, temporária ou definitivamente, a conta          de um Usuário.',
              'O Usuário acessará sua conta através de e-mail (login) e senha e compromete-se a não informar a terceiros esses dados,          responsabilizando-se integralmente pelo uso que deles seja feito. <b><u>Você não pode autorizar terceiros a utilizarem a sua Conta.</u></b>          Você não pode ceder ou por outra forma transferir a sua Conta para qualquer outra pessoa ou entidade. <b><u>A ChatFly, nem quaisquer            de seus empregados ou prepostos solicitará, por qualquer meio, físico ou eletrônico, que seja informada sua senha. A ChatFly            não possui acesso às senhas criadas, sendo criptografadas.</u></b>',
              'O Usuário poderá criar “Perfil de Atendente”. Os Atendentes são usuários que podem atender os contatos do Usuário, mediante o login com o e-mail e senha de cadastro criado pelo Usuário. O Usuário é responsável por criar um Perfil de Atendente e por fornecer a ele acesso à Plataforma.',
              'O Usuário possui acesso ao Perfil de Atendente, podendo bloquear suas atividades, cancelar seu perfil e trocar sua senha de acesso, a qualquer momento e a seu critério.',
              'O Usuário possui a obrigação de cadastrar o nome e e-mail de “Atendente” com dados corretos e fidedignos.',
              'Somente o Usuário possui poder de decisão. Neste sentido, o “Atendente” não possuirá autorização para deletar ou alterar informações ou mensagens.',
              'É de responsabilidade do Usuário: (a) manter o sigilo de sua identificação de conta e senha; (b) atualizar e revisar sua conta frequentemente; (c) manter seus dispositivos eletrônicos seguros, por exemplo, com uso de ferramentas de antivírus e firewall; e (d) avisar prontamente a ChatFly caso ocorra qualquer utilização não autorizada de sua conta ou qualquer quebra de segurança. Você é responsável por todas as atividades que forem realizadas através de sua conta e a ChatFly não será responsável por qualquer perda ou dano resultante de sua falha em cumprir com as obrigações aqui previstas.'
            ],
            'title': 'CADASTRO, CONTA, SENHA E PERFIL DE ATENDENTE'
          },
          {
            'points': [
              'A Plataforma possui funcionalidades que podem ser revistas, alteradas ou canceladas a qualquer momento, mediante aviso aos Usuários. A Plataforma está restrita às funcionalidades possíveis e disponíveis no WhatsApp.',
              'O Usuário deverá seguir corretamente os passos demonstrados no “Tutorial”, para que tenha acesso à Plataforma. Por exemplo, caso o Usuário conecte seu número ao WhatsApp Web, seu login na Plataforma ChatFly será desconectada.',
              'No “Modo Teste”, disponível para Usuários que queiram testar a Plataforma, disponível por 3 (três) dias, algumas funcionalidades poderão ser limitadas, à critério da ChatFly. No “Modo Teste”, não há necessidade de pagamento pelo Usuário.'
            ],
            'title': 'FUNCIONALIDADES, TESTE E TUTORIAL'
          },
          {
            'points': [
              'A íntegra das Políticas de Privacidade da ChatFly&nbsp;pode ser acessada <a href="/termos-de-uso">\n          aqui\n        </a>.\n        Um resumo dos principais pontos será disponibilizado abaixo, <b>porém esse resumo não exime o usuário de realizar a leitura integral das\n          Políticas de Privacidade.</b>',
              '<b>\n          Principais pontos das Políticas de Privacidade:\n        </b>',
              '<span          class="text-bold"          style="font-size: 18px"        >          - INFORMAÇÕES COLETADAS PELA ChatFly:        </span>        A fim de realizar o cadastro dos usuários na plataforma, a ChatFly coleta dados de cadastro: nome, e-mail e telefone. Ainda, a        fim de proporcionar o funcionamento da Plataforma, a ChatFly realiza a coleta de dados de comunicação entre usuários. Dados de        comunicação ficam indisponíveis após o Usuário desconectar da Plataforma.',
              ' <span          class="text-bold"          style="font-size: 18px"        >          - COMPARTILHAMENTO DE DADOS COM TERCEIROS:        </span>        Exceto nas hipóteses especificadas nas <a href="/politica-de-privacidade">          Políticas de Privacidade        </a>, a ChatFly não realiza o compartilhamento de dados pessoais de seus usuários com terceiros. Nesses casos, sempre que a segurança das informações depender da ChatFly, a empresa se compromete a utilizar seus melhores esforços para manter seus dados em plena segurança.',
              '<span          class="text-bold"          style="font-size: 18px"        >          - PROTEÇÃO E SEGURANÇA:        </span>        A ChatFly utiliza os mais modernos sistemas de segurança da informação em todos os nossos sistemas e dispositivos, o que inclui o uso de ferramentas tecnológicas como criptografias, controles de acesso, firewalls, registros de criação e acesso de contas, dentre outras.<br>        Caso ocorra qualquer incidente relacionado aos dados pessoais dos usuários, a ChatFly enviará um comunicado aos usuários e às autoridades competentes dentro de um prazo razoável com todas as informações necessárias.',
              '<span          class="text-bold"          style="font-size: 18px"        >          - DIREITOS DOS TITULARES DOS DADOS PESSOAIS:        </span>        Nossas Políticas de Privacidade preveem todos os direitos legalmente garantidos aos usuários cujos dados são tratados pela ChatFly. Acesse o documento para obter todas as informações a esse respeito.',
              '<span          class="text-bold"          style="font-size: 18px"        >          - MANUTENÇÃO E EXCLUSÃO DOS DADOS PESSOAIS:        </span>        A ChatFly manterá as informações dos usuários enquanto as respectivas contas forem mantidas na plataforma. A qualquer tempo o usuário poderá solicitar a exclusão da sua conta e dos seus dados pessoais diretamente pelos canais de comunicação da ChatFly.        Mesmo diante de uma solicitação de exclusão dos dados, a ChatFly poderá manter algumas informações consigo, nos termos da legislação em vigor.',
              '<span          class="text-bold"          style="font-size: 18px"        >          - ATUALIZAÇÃO DAS POLÍTICAS DE PRIVACIDADE:        </span>        A ChatFly enviará um comunicado aos usuários sempre que realizar algum tipo de atualização e/ou alteração nas <a href="/politica-de-privacidade">          Políticas de Privacidade        </a> e, caso seja necessário seu consentimento, solicitaremos imediatamente. Você reconhece que o aceite destes termos de uso será registrado nos nosso banco de dados e poderá ser utilizado como prova do aceite, independentemente de outra formalidade.'
            ],
            'title': 'CAPTURA DE DADOS E PRIVACIDADE'
          },
          {
            'points': [
              'Todos os materiais exibidos ou disponibilizados na Aplicação e no Site, incluindo, sem limitação, textos, gráficos, sons e áudios, documentos, vídeos, obras de arte, software, logos e código HTML (coletivamente, o "Material") são de propriedade exclusiva da ChatFly&nbsp;ou de seus fornecedores de conteúdo, os quais reservam a si todos os seus direitos de propriedade intelectual. Os Materiais são protegidos pelas leis de direitos autorais, propriedade intelectual e outras regras, regulamentos e legislações brasileiras, americanas e internacionais de propriedade intelectual aplicáveis.',
              'A ChatFly concede a você uma licença limitada, não-sublicenciável, não-exclusiva, revogável e inalienável que permite: (i) o acesso e utilização da Aplicação e/ou do Site no dispositivo pessoal do Usuário unicamente em conexão com a utilização dos Serviços por parte do Usuário; e (ii) o acesso e utilização de quaisquer conteúdos, informações ou materiais relacionados que possam ser disponibilizados através dos Serviços, sempre unicamente para uso pessoal (enquanto Consumidor ou para fins profissionais) e não comercial do Usuário.',
              'Todas as marcas comerciais, marcas de serviços e logotipos (as "Marcas") exibidas no Aplicativo e no Site são de propriedade exclusiva da ChatFly e de seus respectivos titulares. Você não poderá usar as Marcas, de nenhuma forma, sem o consentimento prévio e por escrito da ChatFly e de seus respectivos titulares.'
            ],
            'title': 'LICENÇA E PROPRIEDADE INTELECTUAL'
          },
          {
            'points': [
              'Constituem <b>obrigações do Usuário</b>, entre outras previstas neste instrumento:',
              'I - Fornecer, no processo de cadastro, dados verdadeiros e completos sobre seu nome, e-mail e telefone, mantendo-os sempre atualizados, assim como fornecer dados verdadeiros e completos quando cadastrar “Perfil de atendente”;',
              'II - Zelar pela segurança e confidencialidade do login e senha de acesso, de caráter pessoal e intransferível, sendo que você é o único e exclusivo responsável pela sua utilização e guarda, assumindo todos os ônus e responsabilidades decorrentes de seus atos e de sua conduta, respondendo, ainda, pelos atos que terceiros praticarem em seu nome, por meio do uso de seu login e da sua senha de acesso;',
              'III – Não violar direitos de propriedade intelectual da ChatFly;',
              'IV – Não incorporar malwares (código ou programa de computador malicioso e mal-intencionado, que possa causar danos, alterações ou roubo de informações), não sendo permitida a utilização de nenhum dispositivo, software ou outro recurso que venha a interferir nas atividades e operações da ChatFly, bem como nas contas ou banco de dados. Qualquer intromissão, tentativa ou atividade que viole ou contrarie as leis de direito de propriedade intelectual e/ou as proibições estipuladas nestes Termos e Condições de Uso, tornarão o responsável passível das ações legais pertinentes, bem como das sanções aqui previstas, sendo ainda responsável pelas indenizações por eventuais danos causados;',
              'V – Não utilizar a Plataforma para prática de atividades ilícitas e envio de mensagens (texto, voz ou imagens) ilícitas, assim como ameaças, pornografia e pornografia infantil, violência, etc;',
              'VI – Não utilizar a Plataforma para a prática de spam;',
              'VII – Arcar com gastos com ligações telefônicas, pacotes de dados, mensagens, correspondências ou qualquer outro valor despendido em razão da utilização da Plataforma, por qualquer motivo que o seja;',
              'VIII – Responder por atos irregulares ou ilícitos praticados na Plataforma por Atendentes.',
              '<b>          O Usuário é exclusivamente responsável pela legalidade e legitimidade das mensagens trocadas com seus contatos/perfis.          <u>            A ChatFly não assume responsabilidade alguma pelas mensagens, especialmente aquelas que infrinjam as suas políticas,            termos, qualquer lei ou decisão judicial vigente. A ChatFly não realiza análise prévia de conteúdo, pois caracterizaria            censura.          </u>        </b>',
              '<u>          A ChatFly , sem qualquer necessidade de notificação prévia, se reserva no direito de bloquear, temporária ou permanentemente, a seu critério, Usuários/Atendentes que tenham movimentações suspeitas via mensagens. Nessa hipótese, a ChatFly poderá fornecer todas as informações pertinentes às autoridades competentes.        </u>',
              '<u>          A ChatFly poderá ainda bloquear, temporária ou permanentemente a conta do Usuário no caso de descumprimento de qualquer obrigação prevista no presente Termos e Condições de Uso.        </u>',
              'Constituem <b>obrigações da ChatFly</b>, entre outras previstas neste instrumento:',
              'I – Disponibilizar o acesso dos Usuários à Área Restrita, mediante o fornecimento de login e senha de acesso, caso os dados cadastrais e perfil do Usuário sejam aprovados;',
              'II - Disponibilizar todas as informações e ferramentas necessárias para que o Usuário possa acessar as funcionalidades do Aplicativo/Site.',
              'A <b>ChatFly não se responsabiliza por quaisquer danos ou prejuízos decorrentes da indisponibilidade</b> total ou parcial do Aplicativo/Site, em decorrência de:',
              'I - manutenção técnica e/ou operacional;',
              'II - alteração, pelo WhatsApp e Instagram, de suas políticas e diretrizes;',
              'III - interrupção ou suspensão dos serviços fornecidos por empresas que disponibilizam acesso à Internet;',
              'IV - caso fortuito ou eventos de força maior, incluindo, mas não se limitando a quedas de energia, ataques de malwares e ações de terceiros que impeçam a sua disponibilidade, a exemplo da retirada do Aplicativo e/ou Site.',
              'A ChatFly não se responsabiliza:',
              'I – por atos de má-fé de terceiros, tais como hackers, que acessem os dados cadastrais\n          e pessoais do Usuário e se utilizem ilicitamente dos mesmos para quaisquer fins, em\n          especial, em relação à rede social Instagram onde é necessário que o Usuário forneça\n          sua senha de acesso;',
              'II - pela perda de informações e dados encaminhadas pelo Usuário através da\n          Plataforma',
              'de dados da Plataforma.',
              'Ainda, como a ChatFly não presta serviços na área de troca de mensagens eletrônicas, tampouco presta serviços de atendimento (SAC) ou compras, limitando-se  apenas a fornecer uma aplicação para seu intermédio e gerenciamento, a responsabilidade por todas as obrigações decorrentes, sejam fiscais, trabalhistas, consumeristas ou de qualquer outra natureza, será exclusivamente dos Usuários.',
              'Assim, você declara e reconhece que na hipótese de a ChatFly vir a ser demandada judicialmente ou tenha contra ela uma reclamação dos órgãos de proteção ao consumidor diante de uma atitude praticada pelo Usuário, os valores relativos às condenações, acordos, despesas processuais e honorários advocatícios despendidos pela empresa poderão ser cobrados através dos meios legalmente previstos.'
            ],
            'title': 'DAS OBRIGAÇÕES E RESPONSABILIDES DAS PARTES'
          },
          {
            'points': [
              'Você é responsável pela obtenção do acesso à rede de dados necessária para utilizar os Serviços. As tarifas e taxas da rede móvel\n          do Usuário aplicáveis a dados e mensagens podem ser aplicáveis caso você utilize os Serviços a partir de um dispositivo que funcione\n          com <i>wi-fi</i>, e você será responsável por tais tarifas e taxas.',
              'Você é responsável pela aquisição e atualização de <i>hardware</i> ou dispositivos compatíveis necessários para aceder e utilizar os Serviços da          Aplicação/Site e quaisquer atualizações aos mesmos. A ChatFly apenas garante que os Serviços, ou qualquer parte deles, funcionarão num          determinado <i>hardware</i> ou dispositivos caso haja a compatibilidade necessária. Para além disso, os Serviços podem sofrer de funcionamento          deficiente ou atrasos inerentes à utilização da Internet e de comunicações eletrônicas.',
              'Para te manter informado sobre sua conta, produtos e serviços da ChatFly, além de informações sobre segurança, poderemos te mandar          mensagens SMS no número de celular cadastrado ou mensagens através do WhatsApp.'
            ],
            'title': 'ACESSO À REDE E DISPOSITIVOS'
          },
          {
            'points': [
              'A operação da plataforma funciona mediante a compra de “crédito pré-pago”. A compra de créditos para uso por 30 (trinta) dias é realizada via Mercado Pagou ou PicPay e a ChatFly&nbsp;não possui qualquer responsabilidade por tais transações.',
              'Caso o Usuário adquira créditos, por serem pré-pagos, não haverá reembolso ou possibilidade de cancelamento.',
              'A ChatFly poderá disponibilizar cupons promocionais de desconto, que serão informados aos Usuários.'
            ],
            'title': 'PAGAMENTO E TARIFAS'
          },
          {
            'points': [
              'A ChatFly&nbsp;não economizará esforços para resolver qualquer problema que você, Usuário, tiver na nossa plataforma.',
              'No entanto, caso não haja solução amigável e extrajudicial, você concorda que o Foro competente para dirimir qualquer litígio relativo a estes Termos de Uso ou da Política de Privacidade, com exclusão de qualquer outro, será o Foro Central da Comarca de São Paulo/SP.'
            ],
            'title': 'FORO E LEGISLAÇÃO APLICÁVEL'
          }
        ],
        'date': '21/01/2020',
        'intro': '\n    <p>\n      A Aplicação/Aplicativo/Site é de propriedade e titularidade exclusiva de ArCode,\n      empresa de direito privado, devidamente inscrita no CNPJ/MF sob o nº 37.536.583/0001-80, com sede\n      na R JOÃO PENTEADO, nº 1737, Bairro JARDIM AMERICA, cidade/SP, CEP: 14.020-180, doravante denominada (“ChatFly”).\n    </p>\n\n    <p>\n      O presente documento visa estabelecer os termos e condições aplicáveis ao uso do aplicativo por parte do Usuário (direitos e obrigações), mas se você ainda tiver alguma dúvida, pode entrar em contato conosco através do WhatsApp (16) 99157-8481 e vamos ter prazer em responder suas perguntas.\n    </p>\n\n    <p class="text-bold">\n      Por favor, revise os termos de uso cuidadosamente antes de utilizá-lo. Se você não aceitar estes termos e condições, não poderá utilizar os serviços.\n    </p>\n\n    <p>\n      <u>\n        Precisamos que você <b>leia tudo com atenção</b> e, para facilitar sua leitura, preparamos esse sumário que te levará direto aos assuntos do seu maior interesse.\n      </u>\n    </p>\n\n    <p>\n      A aceitação por parte do Usuário gera uma relação contratual entre o Usuário e a ChatFly. Você reconhece que é e será sempre livre de utilizar ou não os Serviços. Você entende que o uso dos Serviços é de sua inteira responsabilidade e pode gerar consequência perante o WhatsApp.\n    </p>'
      },
      'privacy': {
        'active': true,
        'points': [
          {
            'points': [
              'Em primeiro lugar, vamos te mostrar as definições de alguns termos que aparecerão ao longo desse documento.',
              '<span class="text-bold" style="font-size: 18px">\n            1.1 - DADO PESSOAL:\n          </span>\n\n          informação relacionada a uma pessoa física que a torne diretamente identificada ou identificável;',
              '<span class="text-bold" style="font-size: 18px">\n            1.2 – DADO PESSOAL SENSÍVEL:\n          </span>\n\n          dado pessoal sobre origem racial ou étnica, convicção religiosa, opinião política, filiação a sindicato ou a organização de caráter religioso, filosófico ou político, dado referente à saúde ou à vida sexual, dado genético ou biométrico, quando vinculado a uma pessoa física;',
              '<span class="text-bold" style="font-size: 18px">\n            1.3 – TITULAR:\n          </span>\n\n          pessoa física a quem se referem os dados pessoais que são objeto de tratamento;',
              '<span class="text-bold" style="font-size: 18px">\n            1.4 – TRATAMENTO:\n          </span>\n\n          toda operação realizada com dados pessoais, como as que se referem a coleta, produção, recepção, classificação, utilização, acesso, reprodução, transmissão, distribuição, processamento, arquivamento, armazenamento, eliminação, avaliação ou controle da informação, modificação, comunicação, transferência, difusão ou extração;',
              '<span class="text-bold" style="font-size: 18px">\n            1.5 – ANONIMIZAÇÃO:\n          </span>\n\n          utilização de meios técnicos razoáveis e disponíveis no momento do tratamento, por meio dos quais um dado perde a possibilidade de associação, direta ou indireta, a um indivíduo;',
              '<span class="text-bold" style="font-size: 18px">\n            1.6 – CONSENTIMENTO:\n          </span>\n\n          manifestação livre, informada e inequívoca pela qual o titular concorda com o tratamento de seus dados pessoais para uma finalidade determinada;'
            ],
            'title': 'DEFINIÇÕES'
          },
          {
            'points': [
              'Agora que você já sabe sobre o que estamos falando, vamos explicar para você quais são seus dados pessoais que coletamos, lembrando que somente trabalhamos com aquelas informações essenciais para as finalidades da nossa Plataforma, sendo certo que outras informações podem ser coletadas e tratadas diante da imposição de normas que se apliquem aos nossos serviços.',
              '<h3 class="text-bold" style="font-size: 18px">\n          2.1 - Dados Fornecidos pelo(a) Usuário(a):\n        </h3>\n\n        <p>\n          <span class="text-bold" style="font-size: 18px">\n            a) Dados de Cadastro na Plataforma:\n          </span>\n\n          nome, e-mail e telefone.<br>\n          Para comprovação dessas informações fornecidas diretamente por você, podemos solicitar o envio de fotos do(s) documento(s) contendo as informações mencionadas acima.\n        </p>',
              '<h3 class="text-bold" style="font-size: 18px">\n          2.2 - Dados Coletados pela Plataforma:\n        </h3>\n\n        <p>\n          <span class="text-bold" style="font-size: 18px">\n            a) Dados de Localização:\n          </span>\n\n          Não coletamos informações dessa natureza.\n        </p>',
              '<p>\n          <span class="text-bold" style="font-size: 18px">\n            b) Perfil de Usuário:\n          </span>\n\n          Quando a Plataforma estiver sendo utilizada por você, coletaremos informações sobre como você usa nossa Plataforma, tais como páginas mais acessadas, datas e horários de acesso, falhas no aplicativo e demais informações relacionadas ao seu uso.<br>\n          Essas informações serão coletadas com a finalidade de garantir a constante qualidade das funcionalidades existentes na Plataforma.\n        </p>',
              '<p>\n          <span class="text-bold" style="font-size: 18px">\n            c) Dados de Transações:\n          </span>\n\n          As transações de pagamento para o uso da Plataforma serão realizadas via PagSeguro e a ZapFly não possui qualquer responsabilidade quanto à transação, tampouco coleta dados dessa natureza.\n        </p>',
              '<p>\n          <span class="text-bold" style="font-size: 18px">\n            d) Dados do Dispositivo Eletrônico:\n          </span>\n\n          Ao utilizar a Plataforma ZapFly, serão coletados dados mínimos, de acordo com a legislação brasileira, tais como as ações tomadas dentro da Plataforma, endereço IP, data e horário.<br>\n          Essas informações serão coletadas com a finalidade de cumprir com a Lei n. 12.965/2014, conhecida como Marco Civil da Internet.\n        </p>',
              '<p>\n          <span class="text-bold" style="font-size: 18px">\n            e) Dados de Comunicação entre Usuário e contatos:\n          </span>\n\n          Nossa Plataforma permite que os usuários se comuniquem com outros números, através do WhatsApp, com ferramentas de voz e texto. Ainda, nossa Plataforma permite que o Usuário receba e\nenvie mensagens no direct da rede social Instagram. Nesses casos, serão coletados dados relacionados a essas comunicações, tais como data, hora e conteúdo das comunicações. Essas informações serão coletadas com a finalidade de garantir o próprio funcionamento da Plataforma, bem como suporte aos usuários, bem como para fins de proteção e segurança. Ao desconectar do Aplicativo, não temos mais acesso ao conteúdo de comunicações de WhatsApp. Quanto ao conteúdo de\ncomunicação do Instagram, é necessário que o Usuário troque sua senha do Instagram\npara desautorizar a conexão da Plataforma com o Instagram ou que contate nosso\nSuporte (WhatsApp 16 99795-7988) para solicitar a remoção da conexão da Plataforma com o Instagram.\n        </p>',
              '<p>\n          <span class="text-bold" style="font-size: 18px">\n            f)\n          </span>\n\n          Nossa Plataforma utiliza o Google Analytics para a coleta e processamento de Dados, conforme políticas do Google. São de responsabilidade única e exclusiva do Google os usos dos dados realizados por este ou seus parceiros, não tendo a ZapFly responsabilidades resultantes de tal uso.\n        </p>'
            ],
            'title': 'INFORMAÇÕES QUE COLETAMOS'
          },
          {
            'points': [
              'Como você já sabe quais tipos de informações coletamos, agora você pode ter uma pergunta: “a ChatFly&nbsp;vai compartilhar meus dados com alguma outra pessoa e/ou empresa?”. E nessa sessão vamos responder a essa pergunta.',
              'Como falamos no começo, a privacidade dos nossos usuários é uma preocupação constante. Assim, nós não vamos compartilhar seus dados com terceiros, exceto nas hipóteses abaixo. É importante lembrar que, nesses casos, sempre que a segurança das informações depender da ZapFly, nos comprometemos a utilizar nossos melhores esforços para manter seus dados em plena segurança.',
              '<span class="text-bold" style="font-size: 18px">\n          a) Compartilhamento mediante consentimento do titular:\n        </span>\n\n        seus dados serão compartilhados sempre que você expressamente nos solicitar e/ou autorizar a fazer essa operação;',
              '<span class="text-bold" style="font-size: 18px">\n          b) Compartilhamento com órgãos públicos e entidades públicas:\n        </span>\n\n        poderemos compartilhar seus dados com órgãos públicos e entidades públicas para atender eventuais obrigações legais e/ou regulatórios que sejam impostas aos serviços que prestamos, bem como para o cumprimento de ordens judiciais eventualmente impostas;',
              '<span class="text-bold" style="font-size: 18px">\n          c) Compartilhamento com autoridades policiais:\n        </span>\n\n        poderemos compartilhar seus dados pessoais e demais informações que julgarmos necessárias caso, a critério da ZapFly, houver suspeita do cometimento de qualquer ato considerado crime ou contravenção penal, nos termos da legislação. Nesse caso, estará expressamente dispensada a existência de obrigação legal e/ou regulatória, bem como de ordem judicial;',
              '<span class="text-bold" style="font-size: 18px">\n          d) Compartilhamento com prestadores de serviço e parceiros comerciais:\n        </span>\n\n        poderemos compartilhar seus dados pessoais com prestadores de serviço e parceiros comerciais, tais como empresas que funcionem como meios de pagamento.'
            ],
            'title': 'COMPARTILHAMENTO DE DADOS'
          },
          {
            'points': [
              'Como você já entendeu quais dados coletamos e em quais hipóteses vamos compartilhar essas informações, é hora de saber como vamos manter seus dados protegidos e seguros.',
              'Nós utilizamos os mais modernos sistemas de segurança da informação em todos os nossos sistemas e dispositivos, o que inclui o uso de ferramentas tecnológicas como criptografias, controles de acesso, firewalls, registros de criação e acesso de contas, dentre outras.',
              'É importante lembrar que somente aplicamos essas camadas de segurança a partir do momento que suas informações são recebidas pela ZapFly e enquanto forem mantidas conosco. Assim, não podemos garantir a segurança do dispositivo eletrônico ou das redes que você está utilizado para acessar nossa Plataforma.',
              'Mas apesar de utilizarmos nossos maiores esforços para proteger os dados dos nossos usuários, sabemos que incidentes podem acontecer. Dessa forma, caso quaisquer de seus dados pessoais sejam expostos de alguma forma, lhe enviaremos um comunicado dentro de um prazo razoável com as seguintes informações: (i) descrição da natureza dos dados pessoais afetados; (ii) indicação das medidas técnicas e de segurança utilizadas para a proteção dos dados; (iii) riscos relacionados ao incidente; (iv) motivos da demora, no caso de a comunicação não ter sido imediata; e (v) as medidas que foram ou que serão adotadas para reverter ou mitigar os efeitos do prejuízo.'
            ],
            'title': 'PROTEÇÃO E SEGURANÇA'
          },
          {
            'points': [
              'Como dono(a) de seus próprios dados pessoais, você possui diversos direitos que podem ser exercidos a qualquer tempo e mediante requisição direta direcionada para a ChatFly. Abaixo, vamos explicar cada um desses direitos, demonstrando nosso compromisso com a transparência:',
              '<span class="text-bold" style="font-size: 18px">\n          a) Confirmação da existência de tratamento:\n        </span>\n\n        você pode nos encaminhar uma solicitação para confirmar se estamos realizando qualquer tipo de tratamento relacionado aos seus dados pessoais, bem como quais dados possuímos a seu respeito;',
              '<span class="text-bold" style="font-size: 18px">\n          b) Acesso aos dados:\n        </span>\n\n        além de confirmar se estamos tratando seus dados pessoais, você também pode solicitar acesso a todos os dados que possuímos a seu respeito;',
              '<span class="text-bold" style="font-size: 18px">\n          c) Correção de dados incompletos, inexatos ou desatualizados:\n        </span>\n\n        caso você verifique que seus dados estão incompletos, errados ou desatualizados em nossa Plataforma, você poderá nos solicitar a correção das informações;',
              '<span class="text-bold" style="font-size: 18px">\n          d) Anonimização, bloqueio ou eliminação de dados desnecessários, excessivos ou tratados em desconformidade com a lei:\n        </span>\n\n        caso você entenda que estamos tratando dados de forma excessiva a seu respeito, você poderá solicitar que realizemos a anonimização, bloqueio ou eliminação dessas informações. Sobre esse direito, é importante lembrar que primeiramente realizaremos uma consulta para verificar se realmente não precisamos dos dados que foram coletados;',
              '<span class="text-bold" style="font-size: 18px">\n          e) Portabilidade dos dados a outro fornecedor de serviço ou produto:\n        </span>\n\n        caso você opte por utilizar alguma Plataforma concorrente da ZapFly, você poderá nos solicitar o envio de todas as suas informações que possuímos conosco diretamente para essa nova Plataforma. Entretanto, preservaremos os segredos comerciais e industriais que eventualmente estejam envolvidos nas operações;',
              '<span class="text-bold" style="font-size: 18px">\n          f) Eliminação dos dados pessoais tratados com o consentimento do titular:\n        </span>\n\n        caso você tenha nos fornecido seu consentimento para o tratamento de seus dados pessoais, poderá solicitar a eliminação desses mesmos dados, exceto quando tivermos que manter essas informações, nos termos da lei, oportunidade na qual lhe informaremos sobre isso;',
              '<span class="text-bold" style="font-size: 18px">\n          g) Informação das entidades públicas e privadas com as quais compartilhamos seus dados:\n        </span>\n\n        você poderá nos solicitar o detalhamento sobre quais órgãos públicos e/ou empresas compartilhamos seus dados pessoais;',
              '<span class="text-bold" style="font-size: 18px">\n          h) Informação sobre a possibilidade de não fornecer consentimento e sobre as consequências da negativa:\n        </span>\n\n        sempre que precisarmos do seu consentimento para o tratamento dos seus dados pessoais, informaremos a você sobre a possibilidade de não nos fornecer essa autorização, bem como o que isso pode acarretar;',
              '<span class="text-bold" style="font-size: 18px">\n          i) Revogação do consentimento\n        </span>\n\n        você poderá revogar o consentimento que nos fornecer para tratar seus dados pessoais. Entretanto, poderemos manter informações para o cumprimento de leis e/ou regulamentos, além de outras hipóteses expressamente previstas na lei.'
            ],
            'title': 'DIREITOS DOS TITULARES DOS DADOS PESSOAIS'
          },
          {
            'points': [
              'Manteremos suas informações conosco enquanto você mantiver sua conta em nossa Plataforma (o que esperamos que dure por muito tempo!). Caso você opte por encerrar sua conta, poderá solicitar a exclusão dos seus dados pessoais diretamente em nossos canais de comunicação, lembrando que apenas o “log off”/”sair” da Plataforma não basta para que suas informações sejam deletadas.',
              'Lembramos também que, mesmo diante da sua solicitação de exclusão dos dados, poderemos manter algumas informações conosco, nos termos da legislação em vigor. Nesse caso, lhe informaremos a esse respeito. Ainda, poderemos manter seus dados conosco de forma anonimizada e para uso exclusivo da ZapFly.'
            ],
            'title': 'MANUTENÇÃO E EXCLUSÃO DOS DADOS PESSOAIS'
          },
          {
            'points': [
              'Enviaremos um comunicado a você sempre que realizarmos algum tipo de atualização e/ou alteração nessas Políticas de Privacidade e, caso seja necessário seu consentimento, solicitaremos imediatamente.'
            ],
            'title': 'ATUALIZAÇÕES DAS POLÍTICAS DE PRIVACIDADE'
          }
        ],
        'date': '21/01/2021',
        'intro': '<p>\n    A Aplicação/Aplicativo/Site é de propriedade e titularidade exclusiva de ArCode,\n    empresa de direito privado, devidamente inscrita no CNPJ/MF sob o nº 37.536.583/0001-80, com sede\n    na R JOÃO PENTEADO, nº 1737, Bairro JARDIM AMERICA, cidade/SP, CEP: 14.020-180, doravante denominada (“ChatFly”).\n  </p>\n\n  <p>\n    Nós da ChatFly estamos constantemente preocupados em respeitar a privacidade dos\n    nossos usuários e demonstrar como seus dados pessoais são tratados dentro da nossa\n    Plataforma.\n  </p>\n\n  <p>\n    Escrevemos esse documento para te explicar como tratamos seus dados pessoais, mas se\n    você ainda tiver alguma dúvida, pode entrar em contato conosco através do WhatsApp\n    (16) 99157-8481 e vamos ter prazer em responder suas perguntas. <u>É muito importante que\n    você leia tudo com atenção e, para facilitar sua leitura, preparamos esse sumário que te\n    levará direto aos assuntos do seu maior interesse.</u>\n  </p>\n\n  <p>\n    Vamos lá?!\n  </p>'
      },
      'support': {
        'active': true,
        'points': [
          {
            'title': 'Problema com Pagamentos',
            'description': 'Certo, pode me especificar o problema?',
            'type': 'OPTION',
            'points': [
              {
                'title': 'Paguei mas a plataforma não foi liberada',
                'description': 'Isto pode ser um problema no cartão ou no gateway escolhido, pode explicar em detalhes o que aconteceu?',
                'type': 'TEXT',
                'points': []
              },
              {
                'title': 'Cartão recusado',
                'description': 'Cartões são recusados pela autentificação do gateway de pagamento, como PagSeguro por motivos de segurança, ou por dados que nâo estão corretos.<div><br></div><div>Nesse caso, tente re-inserir os dados para ver se não errou algum deles, caso não funcione, tente usar outro cartão.<br><br>Se mesmo assim nada resolve, descreva aqui os detalhes do problema e clique em pedir suporte.</div>',
                'type': 'TEXT',
                'points': []
              },
              {
                'title': 'Outro',
                'description': 'Pode nos dizer em detalhes o seu problema?',
                'type': 'TEXT',
                'points': []
              }
            ]
          },
          {
            'title': 'Problemas de conexão',
            'description': 'Problemas de Conexão geralmente acontecem pois há algum erro na comunicação do celular com a plataforma, para começarmos vamos verificar algumas coisas antes, por hora, verifique ligue o celular, e abra o whatsapp nele. Voltou na plataforma sozinho?',
            'type': 'OPTION',
            'points': [
              {
                'title': 'Deu certo!',
                'description': 'Então o problema era seu celular fechando o whatsapp para economizar bateria. Pode ser necessario configurar para que ele não faça isso.<div><br></div><div>Caso ainda precisar de ajuda, descreva em detalhes o problema.</div>',
                'type': 'TEXT',
                'points': []
              },
              {
                'title': 'Nâo conectou sozinho',
                'description': 'Certo, pode conferir se o celular esta com internet? Tente acessar algum site no celular.',
                'type': 'OPTION',
                'points': [
                  {
                    'title': 'Está sem internet no celular mesmo',
                    'description': 'A plataforma precisa que o celular esteja sempre com internet e com o whatsapp funcionando, pois é ele que faz tudo. Pode verificar a internet nele e testar novamente?<br><br>Se tiver mais algum problema descreva em detalhes:',
                    'type': 'TEXT',
                    'points': []
                  },
                  {
                    'title': 'O celular esta com internet',
                    'description': 'Neste caso, pode ser que a conexâo gerou um problema, com a conta administradora, clique no botão na barra azul na superior direita que tem o ícone de um triangulo com uma exclamação, esse botão deve reiniciar a conexão e estabilizar, faça esse passo e me diga o que houve:',
                    'type': 'OPTION',
                    'points': [
                      {
                        'title': 'Voltou a funcionar!',
                        'description': 'Que otimo, caso tenha mais algum problema é so descrever o problema aqui e pedir o suporte.',
                        'type': 'TEXT',
                        'points': []
                      },
                      {
                        'title': 'Não funcionou',
                        'description': 'Neste caso, precisarei passar para um atendente humano para diagnosticar o problema. Pode me dar detalhes sobre como esta parando de funcionar? Se é recorrente?',
                        'type': 'TEXT',
                        'points': []
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            'title': 'Duvida',
            'description': 'Está com dúvida de algo? Me fala qual sua duvida e irei chamar um atendente humano.',
            'type': 'TEXT',
            'points': []
          },
          {
            'title': 'Problemas na minha conta',
            'description': 'Me especifique o que houve:',
            'type': 'OPTION',
            'points': [
              {
                'title': 'Problema com Senha',
                'description': 'Verifique se a tecla Caps Lock está desligada e verifique o login novamente, pode ser que o e-mail também esteja com algum erro, caso nada funcione, tente recuperar sua senha na tela de login.<div><br></div><div>Caso tenha mais alguma duvida informe os detalhes dela aqui</div>',
                'type': 'TEXT',
                'points': []
              },
              {
                'title': 'Atualizar informação da conta',
                'description': 'Para atualizar a senha e o e-mail da conta ADM, basta logar nela e ir para o menu de Configurações que está a esquerda na tela, com o ícone de engrenagem, ali é possível alterar tais dados, caso queira atualizar os dados de algum atendente, basta ver na página inicial e clicar no ícone correspondente ao nome do atendente, lembrando que não é possível alterar alguns dados de atendentes, apenas os disponíveis ali.<div><br></div><div>Caso tenha alguma outra duvida me detalhe aqui</div>',
                'type': 'TEXT',
                'points': []
              },
              {
                'title': 'Outro',
                'description': 'Entendo, neste caso por favor detalhe seu problema aqui, e irei chamar um atendente humano:',
                'type': 'TEXT',
                'points': []
              }
            ]
          }
        ],
      },
      logrocket: {
        active: false,
        code: ''
      }
    };

    await app.service('envs').create(data);

    console.timeEnd(actionName);
    return true;
  },

  create_admin: async function (app: any, query: any) {
    const actionName = 'CREATING BASE ADMIN';
    console.log(actionName);
    console.time(actionName);

    const user = {
      email: 'teste@teste.com',
      password: 'teste',
      name: 'Teste Admin',
      lastName: 'TesteLastName',
    };
    const connection = {
      jid: '11111111111111',
      type: 'whatsapp',
      meta: {
        validatedJid: !!query.validatedJid
      }
    };

    const data = await app.service('register').create({ user, connection });
    await app.service('users').patch(data.user._id, { permissions: ['admin', 'super-admin'] });

    console.timeEnd(actionName);
    return true;
  },

  clear_messages: async function (app: any) {
    const actionName = 'CLEARING MESSAGES';
    console.log(actionName);
    console.time(actionName);

    app.service('messages').Model.deleteMany({});

    console.timeEnd(actionName);
    return true;
  },

  call_method: async function (app: any, query: any, data: any) {
    if (query.parameters.length < 2) return;
    const actionName = `CALL METHOD ${query.parameters[0]} ${query.parameters[1]}`;
    console.log(actionName);
    console.time(actionName);

    try {
      await app.service(query.parameters[0])[query.parameters[1]](...data);
    } catch (error) {
      console.log(error);
    }

    console.timeEnd(actionName);
    return true;
  },


};
