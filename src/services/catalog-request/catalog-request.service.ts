// Initializes the `catalog-request` service on path `/catalog-request`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { CatalogRequest } from './catalog-request.class';
import hooks from './catalog-request.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'catalog-request': CatalogRequest & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/catalog-request', new CatalogRequest(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('catalog-request');

  service.hooks(hooks);
}
