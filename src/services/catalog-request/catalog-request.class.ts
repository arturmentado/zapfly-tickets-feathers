import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class CatalogRequest implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  [key: string]: any;
  find(params?: Params): Promise<Data | Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }
  get(id: Id, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }
  update(id: NullableId, data: Data, params?: Params): Promise<Data | Data[]> {

    throw new Error('Method not implemented.;');
  }
  patch(id: NullableId, data: Partial<Data>, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }
  remove(id: NullableId, params?: Params): Promise<Data | Data[]> {
    throw new Error('Method not implemented.');
  }


  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: Params): Promise<Data> {
    try {
      const catalog = await this.app.service('catalogs').get(data.catalog, {});
      const connections: any = await this.app.service('connections').find({ query: { $limit: 1, userId: catalog.userId } });
      const connection = connections.data[0];

      const { number } = data;
      const connectionId = connection._id;
      const userId = catalog.userId;

      const contactParam = {
        query: {
          connectionId,
          jid: `${number}@s.whatsapp.net`
        }
      };

      const contacts: any = await this.app.service('contacts').find(contactParam);
      let contact;
      if (contacts.total) contact = contacts.data[0];
      else contact = await this.app.service('contacts').create({
        connectionId,
        jid: `${number}@s.whatsapp.net`,
        notify: data.name
      });

      const ticketParam = {
        query: {
          connectionId,
          contactId: contact._id,
          status: {
            $in: ['in_progress', 'waiting']
          },
          jid: `${number}@s.whatsapp.net`
        }
      };

      const tickets: any = await this.app.service('tickets').find(ticketParam);

      let ticket;
      if (tickets.total) {
        ticket = tickets.data[0];
        const unread = ticket.unread + 1;
        await this.app.service('tickets')
          .patch(ticket._id, {
            unread
          });
      } else ticket = await this.app.service('tickets')
        .create({
          connectionId,
          userId,
          contactId: contact._id,
          contact,
          messageTimestamp: new Date().getTime() / 1000,
          status: 'waiting',
          unread: 1,
          jid: `${number}@s.whatsapp.net`
        });

      const message = await this.app.service('messages')
        .create({
          connectionId,
          contactId: contact._id,
          ticketId: ticket._id,
          key: {
            fromMe: false,
            jid: `${number}@s.whatsapp.net`
          },
          messageStubType: 'ZAPFLY_CATALOG_ASK',
          messageTimestamp: new Date().getTime() / 1000,
          message: {
            conversation: `Olá, fiz um pedido no seu catalogo meu nome é *${data.name}*, meu numero é ${number}`,
            zapflyProducts: data.list
          },
        });

      await this.app.service('tickets')
        .patch(ticket._id, {
          lastMessage: message,
          messageTimestamp: message.messageTimestamp,
          unread: 1,
        });

      return { success: true };
    } catch (error) {
      console.error('support create', error);
      throw { error: true };
    }
  }
}
