import ApiAuth from '../../hooks/api-auth';
import apiValidate from '../../hooks/api-validate';
import apiCheckPayment from '../../hooks/api-check-payment';
import apiLogsBefore from '../../hooks/api-logs-before';
import apiLogsAfter from '../../hooks/api-logs-after';
import apiLogsError from '../../hooks/api-logs-error';
export default {
  before: {
    all: [apiValidate(), apiCheckPayment(['connection']), apiLogsBefore(), ApiAuth()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [apiLogsAfter()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [apiLogsError()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
