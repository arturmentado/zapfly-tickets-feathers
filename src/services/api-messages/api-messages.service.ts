// Initializes the `api-messages` service on path `/api-messages`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ApiMessages } from './api-messages.class';
import hooks from './api-messages.hooks';

import multer from 'multer';
import path from 'path';
// Add this service to the service type index

const storage = multer.diskStorage({
  destination: (_req: any, _file: any, cb: any) => cb(null, path.resolve(__dirname + '../../../../public/uploads')), // where the file are being stored
  filename: (_req: any, file: any, cb: any) => cb(null, `${Date.now()}${file.originalname}`) // getting the file name
});

const upload = multer({
  storage,
  limits: {
    fieldSize: 1e+8, // Max field value size in bytes, here it's 100MB
    fileSize: 1e+8 //  The max file size in bytes, here it's 10MB
    // READ MORE https://www.npmjs.com/package/multer#limits
  }
});

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'api/messages': ApiMessages & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use(
    '/api/messages',
    upload.array('file'),
    (req: any, _res, next) => {
      const { method } = req;
      if (method === 'POST' || method === 'PATCH') {
        req.feathers.files = req.files;
      }
      req.feathers.ip = req.ip;
      req.feathers.headers = req.headers;
      next();
    },
    new ApiMessages(options, app)
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('api/messages');

  service.hooks(hooks);
}
