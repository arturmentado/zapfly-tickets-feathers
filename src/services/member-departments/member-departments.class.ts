import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberDepartments implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const user: any = params?.user;
    delete params.provider;
    params.query = {
      ...params.query,
      userId: user.userId
    };

    return this.app.service('departments').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    params.query = {
      ...params.query,
      userId: user.userId
    };
    return this.app.service('departments').get(id, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('departments').create(data, params);

  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: any, params: Params): Promise<Data> {
    const user: any = params?.user;
    delete params.provider;
    data.userId = user.userId;
    return this.app.service('departments').patch(id, data, params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    await this.app.service('users').patch(null, { departmentId: null }, { query: { departmentId: id } });
    return this.app.service('departments').remove(id);
  }
}
