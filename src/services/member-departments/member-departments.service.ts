// Initializes the `member-departments` service on path `/member-departments`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberDepartments } from './member-departments.class';
import hooks from './member-departments.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-departments': MemberDepartments & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    whitelist: ['$text', '$search', '$regex'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-departments', new MemberDepartments(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-departments');

  service.hooks(hooks);
}
