import validate from '../../lib/ajv';
import { createValidationSchema } from '../../models/contacts.model';
import { disallow } from 'feathers-hooks-common';
// Don't remove this comment. It's needed to format import lines nicely.

// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

export default {
  before: {
    all: [disallow('external')],
    find: [
      search({ escape: true }),
      search({  // regex search on given fields
        fields: [
          'jid',
          'overrideName',
          'notify',
          'name',
          'tags.label',
          'short',
          'vname',
        ]
      }),
    ],
    get: [],
    // create: [validate(createValidationSchema)],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
