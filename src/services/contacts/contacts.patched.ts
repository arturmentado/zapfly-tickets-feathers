export default async function (item: any, ctx: any): Promise<void> {
  ctx.app.service('tickets')
    .patch(null, { contact: item }, { query: { contactId: item._id } });
}