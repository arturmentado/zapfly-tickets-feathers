// Initializes the `contacts` service on path `/contacts`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Contacts } from './contacts.class';
import createModel from '../../models/contacts.model';
import hooks from './contacts.hooks';
import contactsCreated from './contacts.created';
import contactsPatched from './contacts.patched';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'contacts': Contacts & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    whitelist: ['$text', '$search', '$regex', '$options'],
    multi: ['patch'],
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/contacts', new Contacts(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('contacts');

  service.on('created', contactsCreated);
  service.on('patched', contactsPatched);

  service.hooks(hooks);
}
