import * as authentication from '@feathersjs/authentication';
import validate from '../../lib/ajv';
import { createValidationSchema } from '../../models/connections.model';
import checkPermissions from 'feathers-permissions';
// Don't remove this comment. It's needed to format import lines nicely.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search: any = require('feathers-mongodb-fuzzy-search');

const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [authenticate('jwt')],
    find: [
      search({ escape: true }),
      search({  // regex search on given fields
        fields: [
          'jid'
        ]
      }),
    ],
    get: [],
    create: [checkPermissions({ roles: ['admin'] }), validate(createValidationSchema)],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
