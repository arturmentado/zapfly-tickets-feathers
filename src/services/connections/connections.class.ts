import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Service, MongooseServiceOptions } from 'feathers-mongoose';
import { Application } from '../../declarations';
import { Types } from 'mongoose';

interface ServiceOptions { }
interface Data { }

export class Connections extends Service {
  app: Application;

  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
    this.app = app;
  }


  async get(id: any, params?: any): Promise<any> {
    if (params && params.admin) return super.get(id, params);

    let query: any = {
      jid: id
    };

    let notId = false;
    if (params && params.query && params.query.notId) notId = params.query.notId;

    if (Types.ObjectId.isValid(id) && !notId) {
      query = {
        _id: id
      };
    }
    // console.log(query, Object.getOwnPropertyNames(id), Types.ObjectId.isValid(id));
    const env = await this.Model.findOne(query);
    return env;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: any, params?: any): Promise<any> {
    const result = await super.create(data, params);
    const users: Paginated<any> = await this.app.service('users')
      .find({
        $select: ['_id', 'connsId'],
        query: { userId: data.userId },
        $limit: -1
      }) as Paginated<any>;

    if (users.total) {
      users.data.forEach(async e => {
        const connsId = [...e.connsId, result._id.toString()].filter(e => e);

        await this.app.service('users')
          .patch(e._id, {
            connsId: [...new Set(connsId)]
          });
      });
    }

    return result;
  }
}
