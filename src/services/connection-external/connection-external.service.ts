// Initializes the `connection-external` service on path `/connection-external`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ConnectionExternal } from './connection-external.class';
import hooks from './connection-external.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'connection-external': ConnectionExternal & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/connection-external', new ConnectionExternal(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('connection-external');

  service.hooks(hooks);
}
