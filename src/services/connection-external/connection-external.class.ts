import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class ConnectionExternal implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }
  [key: string]: any;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data> {
    const sessionId = params.headers?.session_id;
    const target = await this.app.service('connections').get(sessionId, { query: { notId: true } });
    if (!target) throw new Error();
    return target;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: any, data: any, params: Params): Promise<Data> {
    const sessionId = String(data.sessionId);
    const target = await this.app.service('connections').get(sessionId, { query: { notId: true } });
    if (!target) throw new Error();

    if (data.status && data.status !== 'qrcode' && !data.qrcode) { data.qrcode = null; }
    if (data.status === 'open') {
      await this.app.service('connections-validate').get(sessionId);

      const messagesParams: any = {
        paginate: false,
        query: {
          $select: [
            '_id',
            'key',
            'status',
            'connectionId',
            'contactId',
            'message'
          ],
          $populate: ['contactId'],
          connectionId: target._id,
          status: 'PENDING'
        }
      };
      const messages: any = await this.app.service('messages').find(messagesParams);

      for (let index = 0; index < messages.length; index++) {
        const message = messages[index];

        if (!message.contactId.jid) continue;
        if (!message.key.id) continue;

        if (!target.useMultidevice) {
          const checkRequestParams = {
            connection: target,
            query: {
              action: 'loadMessage',
              parameters: [
                message.contactId.jid,
                message.key.id
              ]
            }
          };

          const checkRequest: any = await this.app.service('messages-remote').find(checkRequestParams)
            .catch(() => { return; });

          if (!checkRequest || checkRequest.key.id === message.key.id) continue;
        }

        const type = Object.keys(message.message)[0]; // building for whatsapp
        const messageSend: any = {
          id: message.contactId.jid,
          messageId: message.key.id,
          connectionId: target._id,
          message: message.message[type],
          type
        };

        if (message.message[type].originalUrl) {
          messageSend.mimetype = message.message[type].originalMimetype;
          messageSend.url = message.message[type].originalUrl;
        }

        if (message.message.originalQuoted) {
          messageSend.quoted = message.message.originalQuoted;
        }

        await this.app.service('messages-remote').create(messageSend, {})
          .catch(console.error);
      }
    }

    return this.app.service('connections').patch(target._id, data);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
