// Initializes the `update-avatar` service on path `/update-avatar`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { UpdateAvatar } from './update-avatar.class';
import hooks from './update-avatar.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'update-avatar': UpdateAvatar & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/update-avatar', new UpdateAvatar(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('update-avatar');

  service.hooks(hooks);
}
