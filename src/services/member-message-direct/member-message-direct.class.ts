import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class MemberMessageDirect implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const connsId: any = JSON.parse(JSON.stringify(params?.user?.connsId));
    if (!params.query?.connectionId) throw new Error('Connection invalid');

    const connectionId = params.query.connectionId;
    if (!connsId.includes(connectionId)) throw new Error('Connection invalid');

    const connection = await this.app.service('member-connections').get(connectionId, params);
    if (connection.useMultidevice) throw new Error('Using multidevices');

    delete params.provider;
    // http://localhost:3001/messages-query?action=loadMessages&parameters[]=5516996160622@c.us&parameters[]=25
    // http://localhost:3001/messages-query?action=searchMessages&parameters[]=%27Teste%27&parameters[]=5516996160622@c.us&parameters[]=25&parameters[]=1
    return this.app.service('messages-remote').find(params);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
}
