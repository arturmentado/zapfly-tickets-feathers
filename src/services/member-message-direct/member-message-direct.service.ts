// Initializes the `member-message-direct` service on path `/member-message-direct`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { MemberMessageDirect } from './member-message-direct.class';
import hooks from './member-message-direct.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'member-message-direct': MemberMessageDirect & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/member-message-direct', new MemberMessageDirect(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('member-message-direct');

  service.hooks(hooks);
}
