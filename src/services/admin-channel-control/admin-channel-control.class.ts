import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';

interface Data { }

interface ServiceOptions { }

export class AdminChannelControl implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async get(id: string, params: Params): Promise<Data> {
    const userId = params.user?._id.toString();

    const connection = this.app.channel('authenticated').connections
      .find(connection => connection.user._id.toString() === userId);

    if (!connection) throw new Error('Not found connection');

    this.app.channel(id).join(connection);

    return { success: true };
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async create(data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('Method not implemented.');
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async remove(id: string, params: Params): Promise<Data> {
    const userId = params.user?._id.toString();

    const connection = this.app.channel('authenticated').connections
      .find(connection => connection.user._id.toString() === userId);

    if (!connection) throw new Error('Not found connection');

    this.app.channel(id).leave(connection);

    return { success: true };
  }
}
