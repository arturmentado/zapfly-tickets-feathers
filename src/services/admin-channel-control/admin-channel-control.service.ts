// Initializes the `admin-channel-control` service on path `/admin-channel-control`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { AdminChannelControl } from './admin-channel-control.class';
import hooks from './admin-channel-control.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'admin-channel-control': AdminChannelControl & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-channel-control', new AdminChannelControl(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-channel-control');

  service.hooks(hooks);
}
