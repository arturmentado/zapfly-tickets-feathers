// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (ctx: any): Promise<HookContext> => {
    const request = {
      query: ctx.params.query,
      data: ctx.params.query
    };

    let connection: any;
    if (ctx.params.connection) {
      connection = ctx.params.connection;
    }

    let number: any;
    if (ctx.params.query?.connection) {
      number = ctx.params.query.connection;
    }

    let method = 'GET';
    if (ctx.method === 'create') method = 'POST';

    const apiLogs = await ctx.app.service('api-logs').create({
      connection: number,
      connectionId: connection._id,
      method,
      request: request,
      endpoint: `https://beta-be.zapfly.com.br/${ctx.path}`,
      requestIp: ctx.params.ip,
      type: 'received',
    });

    ctx.params.apiLogsId = apiLogs._id;
    return ctx;
  };
};
