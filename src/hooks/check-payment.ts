// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import createApplication, { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options: any = []): Hook => {
  return async (ctx: HookContext): Promise<HookContext> => {
    let target: any = ctx;

    if (!ctx.params?.provider) return ctx;
    if (ctx.params?.user?.permissions.includes('super-admin')) return ctx;

    for (let i = 0; i < options.length; i++) {
      target = target[options[i]];
    }

    if (!target) throw new Error('invalid connection id');

    const { preapprovalPlan } = await ctx.app.service('connections').get(target, { query: { $select: ['preapprovalPlan'] } });

    const error = new Error('check payment');
    if (!preapprovalPlan) throw error;

    const { status, createdAt, extraTrial } = preapprovalPlan;

    if (extraTrial) {
      const extraTrialTimestamp = new Date(extraTrial).getTime();
      if (new Date().getTime() < extraTrialTimestamp) return ctx;
    }

    const timestampFrom7Days = new Date().getTime() - (7 * 24 * 60 * 60 * 1000);
    const createdAtTimestamp = new Date(createdAt).getTime();

    if (status === 'TRIAL' && createdAtTimestamp > timestampFrom7Days) return ctx;

    const validStatus = ['created', 'ACTIVE'];
    if (!validStatus.includes(status)) throw error;

    return ctx;
  };
};
