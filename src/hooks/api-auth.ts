// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const token = context.params.query?.token;
    const connection = context.params?.connection;

    if (!token) throw new Error('invalid token');
    if (!connection) throw new Error('invalid token');
    if (connection.apiToken !== token) throw new Error('invalid token');
    return context;
  };
};
