// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const token = context.params.query?.token;
    const connection = context.params.query?.connection;

    if (!token) throw new Error('missing data');
    if (!connection) throw new Error('invalid data');
    return context;
  };
};
