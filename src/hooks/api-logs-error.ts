// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (ctx: any): Promise<HookContext> => {
    if (ctx.params.apiLogsId) {
      await ctx.app.service('api-logs').patch(ctx.params.apiLogsId, {
        response: ctx.error.message,
        success: false
      });
    }
    return ctx;
  };
};
