"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("@feathersjs/transport-commons");
function default_1(app) {
    if (typeof app.channel !== 'function') {
        // If no real-time functionality has been configured just return
        return;
    }
    app.on('connection', (connection) => {
        if (connection.headers.type === 'whatsapp_conn' && connection.headers.session_id) {
            app.channel(`whatsapp_${connection.headers.session_id}`).join(connection);
            return;
        }
        // On a new real-time connection, add it to the anonymous channel
        app.channel('anonymous').join(connection);
    });
    app.on('login', (authResult, { connection }) => {
        // connection can be undefined if there is no
        // real-time connection, e.g. when logging in via REST
        if (connection) {
            const user = authResult.user;
            app.service('users').patch(user._id, { online: true }).catch();
            app.channel('anonymous').leave(connection);
            app.channel('authenticated').join(connection);
            app.channel('env').join(connection);
            app.channel(`user_${user._id}`).join(connection);
            app.channel(`connections_${user.userId}`).join(connection);
            if (user.permissions.includes('super-admin')) {
                app.channel('super-admin').join(connection);
            }
        }
    });
    app.on('logout', (connection) => {
        const user = connection.user;
        if (!user)
            return;
        app.service('users').patch(user._id, { online: false }).catch();
    });
    app.on('disconnect', (connection) => {
        const user = connection.user;
        if (!user)
            return;
        app.service('users').patch(user._id, { online: false }).catch();
    });
    // eslint-disable-next-line no-unused-vars
    // app.publish((data: any, hook: any) => {
    //   if (!data) return;
    //   console.log(1, hook);
    //   return app.channel('super-admin');
    // });
    // eslint-disable-next-line no-unused-vars
    app.service('tooltips').publish((data, hook) => {
        return app.channel('authenticated');
    });
    // eslint-disable-next-line no-unused-vars
    app.service('connection-logs-external').publish((data, hook) => {
        if (!data)
            return;
        return app.channel(`connection-logs-${data.connectionId}`);
    });
    // eslint-disable-next-line no-unused-vars
    app.service('admin-force-refresh').publish((data, hook) => {
        return app.channel(`user_${data.userId}`);
    });
    app.service('connections').publish((data, context) => {
        app.service('member-connections').emit(context.method + 'ed', data);
    });
    app.service('tickets').publish((data, context) => {
        let method = context.method;
        if (!['d', 'e'].includes(method.slice(-1)))
            method += 'ed';
        if (method.slice(-1) === 'e')
            method += 'd';
        app.service('member-tickets').emit(method, data);
    });
    app.service('users').publish((data, context) => {
        app.service('members').emit(context.method + 'ed', data);
    });
    app.service('messages').publish((data, context) => {
        let method = context.method;
        if (!['d', 'e'].includes(method.slice(-1)))
            method += 'ed';
        if (method.slice(-1) === 'e')
            method += 'd';
        app.service('member-messages').emit(method, data);
    });
    app.service('members').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    app.service('member-tickets').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    app.service('member-tags').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    app.service('member-quick-messages').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    app.service('member-connections').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    app.service('member-messages').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    app.service('member-settings').publish((data, context) => {
        const name = `connections_${data.userId}`;
        return [
            app.channel(name)
        ];
    });
    // Catalog Logic
    app.service('catalogs').publish((data, context) => {
        const name = `${data.name}_public_update`;
        return [
            app.channel(name)
        ];
    });
    app.service('catalog-products').publish((data, context) => {
        const name = `${data.catalogName}_public_update`;
        return [
            app.channel(name)
        ];
    });
    app.service('catalog-categories').publish((data, context) => {
        const name = `${data.catalogName}_public_update`;
        return [
            app.channel(name)
        ];
    });
    app.service('public-channel-control').on('created', (msg, ctx) => {
        const { connection } = ctx.params;
        if (connection) {
            app.channel(`${msg.name}_public_update`).join(connection);
        }
    });
}
exports.default = default_1;
