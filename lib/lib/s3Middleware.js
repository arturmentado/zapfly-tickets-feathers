"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUpload = exports.middlewareUpload = void 0;
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const multer_s3_1 = __importDefault(require("multer-s3"));
const multer_1 = __importDefault(require("multer"));
function middlewareUpload(req, _res, next) {
    const { method } = req;
    if (method === 'POST' || method === 'PATCH') {
        req.feathers.files = req.files;
    }
    next();
}
exports.middlewareUpload = middlewareUpload;
function createUpload(app) {
    const s3 = new aws_sdk_1.default.S3({
        secretAccessKey: app.get('s3Secret'),
        accessKeyId: app.get('s3Key'),
        region: app.get('s3Region'),
        // sslEnabled: true, // optional
        httpOptions: {
            timeout: 6000,
        },
    });
    const upload = multer_1.default({
        storage: multer_s3_1.default({
            s3,
            acl: 'public-read',
            bucket: app.get('s3Bucket'),
            key: function (req, file, cb) {
                cb(null, `${process.env.PRODUCTION ? 'files' : 'dev'}/${Date.now().toString()}_${file.originalname}`);
            }
        }),
        limits: {
            fieldSize: 1e+8,
            fileSize: 1e+8 //  The max file size in bytes, here it's 10MB
            // READ MORE https://www.npmjs.com/package/multer#limits
        }
    });
    return upload;
}
exports.createUpload = createUpload;
