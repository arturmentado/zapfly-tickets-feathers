"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("ajv"));
const feathers_hooks_common_1 = require("feathers-hooks-common");
const ajv = new ajv_1.default({ allErrors: true });
function validate(schema) {
    return feathers_hooks_common_1.validateSchema(schema, ajv);
}
exports.default = validate;
