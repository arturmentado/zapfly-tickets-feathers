"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
async function default_1(context) {
    if (!context.params.provider)
        return;
    if (context.params.headers.type !== 'whatsapp_conn' ||
        !context.params.headers.session_id) {
        context.result = null;
        return;
    }
    const id = context.params.headers.session_id;
    const check = await context.app.service('connections').get(id, { query: { notId: true } });
    if (!check) {
        context.result = null;
        return;
    }
}
exports.default = default_1;
