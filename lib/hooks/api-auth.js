"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-unused-vars
exports.default = (options = {}) => {
    return async (context) => {
        var _a, _b;
        const token = (_a = context.params.query) === null || _a === void 0 ? void 0 : _a.token;
        const connection = (_b = context.params) === null || _b === void 0 ? void 0 : _b.connection;
        if (!token)
            throw new Error('invalid token');
        if (!connection)
            throw new Error('invalid token');
        if (connection.apiToken !== token)
            throw new Error('invalid token');
        return context;
    };
};
