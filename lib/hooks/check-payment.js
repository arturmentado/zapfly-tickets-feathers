"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-unused-vars
exports.default = (options = []) => {
    return async (ctx) => {
        var _a, _b, _c;
        let target = ctx;
        if (!((_a = ctx.params) === null || _a === void 0 ? void 0 : _a.provider))
            return ctx;
        if ((_c = (_b = ctx.params) === null || _b === void 0 ? void 0 : _b.user) === null || _c === void 0 ? void 0 : _c.permissions.includes('super-admin'))
            return ctx;
        for (let i = 0; i < options.length; i++) {
            target = target[options[i]];
        }
        if (!target)
            throw new Error('invalid connection id');
        const { preapprovalPlan } = await ctx.app.service('connections').get(target, { query: { $select: ['preapprovalPlan'] } });
        const error = new Error('check payment');
        if (!preapprovalPlan)
            throw error;
        const { status, createdAt, extraTrial } = preapprovalPlan;
        if (extraTrial) {
            const extraTrialTimestamp = new Date(extraTrial).getTime();
            if (new Date().getTime() < extraTrialTimestamp)
                return ctx;
        }
        const timestampFrom7Days = new Date().getTime() - (7 * 24 * 60 * 60 * 1000);
        const createdAtTimestamp = new Date(createdAt).getTime();
        if (status === 'TRIAL' && createdAtTimestamp > timestampFrom7Days)
            return ctx;
        const validStatus = ['created', 'ACTIVE'];
        if (!validStatus.includes(status))
            throw error;
        return ctx;
    };
};
