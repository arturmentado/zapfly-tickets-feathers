"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-unused-vars
exports.default = (options = {}) => {
    return async (ctx) => {
        if (ctx.params.apiLogsId) {
            await ctx.app.service('api-logs').patch(ctx.params.apiLogsId, {
                response: ctx.result,
                success: true
            });
        }
        return ctx;
    };
};
