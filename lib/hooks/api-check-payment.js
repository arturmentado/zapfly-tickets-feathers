"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-unused-vars
exports.default = (options = []) => {
    return async (ctx) => {
        let target = ctx.params.query;
        for (let i = 0; i < options.length; i++) {
            target = target[options[i]];
        }
        if (!target)
            throw new Error('invalid connection number');
        const connection = await ctx.app.service('connections').get(target, { query: { notId: true } });
        const preapprovalPlan = connection.preapprovalPlan;
        const error = new Error('check payment');
        if (!preapprovalPlan)
            throw error;
        const { status, createdAt, extraTrial } = preapprovalPlan;
        if (ctx.params) {
            ctx.params.connection = connection;
        }
        if (extraTrial) {
            const extraTrialTimestamp = new Date(extraTrial).getTime();
            if (new Date().getTime() < extraTrialTimestamp)
                return ctx;
        }
        const timestampFrom7Days = new Date().getTime() - (7 * 24 * 60 * 60 * 1000);
        const createdAtTimestamp = new Date(createdAt).getTime();
        if (status === 'TRIAL' && createdAtTimestamp > timestampFrom7Days)
            return ctx;
        const validStatus = ['created', 'ACTIVE'];
        if (!validStatus.includes(status))
            throw error;
        return ctx;
    };
};
