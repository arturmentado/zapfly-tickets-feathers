"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// eslint-disable-next-line @typescript-eslint/no-unused-vars
exports.default = (options = {}) => {
    return async (ctx) => {
        var _a;
        const request = {
            query: ctx.params.query,
            data: ctx.params.query
        };
        let connection;
        if (ctx.params.connection) {
            connection = ctx.params.connection;
        }
        let number;
        if ((_a = ctx.params.query) === null || _a === void 0 ? void 0 : _a.connection) {
            number = ctx.params.query.connection;
        }
        let method = 'GET';
        if (ctx.method === 'create')
            method = 'POST';
        const apiLogs = await ctx.app.service('api-logs').create({
            connection: number,
            connectionId: connection._id,
            method,
            request: request,
            endpoint: `https://beta-be.zapfly.com.br/${ctx.path}`,
            requestIp: ctx.params.ip,
            type: 'received',
        });
        ctx.params.apiLogsId = apiLogs._id;
        return ctx;
    };
};
