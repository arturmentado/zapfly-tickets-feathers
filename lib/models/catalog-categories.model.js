"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'catalogCategories';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users', required: true },
        catalogId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'catalogs', required: true },
        catalogName: { type: String, required: true },
        name: { type: String },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
