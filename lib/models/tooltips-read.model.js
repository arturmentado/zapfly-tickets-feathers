"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'tooltipsRead';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        tooltipId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'tooltips' },
        userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
        connectionId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'connections' },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
