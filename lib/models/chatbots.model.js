"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'chatbots';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
        parentId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'chatbots' },
        type: { type: String, default: 'option' },
        active: { type: Boolean, default: true },
        trigger: { type: String },
        optionType: { type: String },
        optionTargetId: { type: String },
        optionActions: [{ type: Object }],
        optionMessage: { type: String, required: false },
        message: { type: String, required: true }
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
