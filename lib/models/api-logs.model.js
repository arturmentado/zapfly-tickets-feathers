"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'apiLogs';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        connectionId: { type: mongooseClient.Schema.Types.ObjectId, refs: 'connections' },
        connection: { type: String },
        type: { type: String },
        request: { type: Object },
        success: { type: Boolean, default: false },
        endpoint: { type: String },
        requestIp: { type: String },
        method: { type: String },
        response: { type: Object },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
