"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createValidationSchema = void 0;
function default_1(app) {
    const modelName = 'contacts';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        connectionId: { type: Schema.Types.ObjectId, ref: 'connections', required: true },
        overrideName: { type: String, nullable: true },
        jid: { type: String, required: true },
        avatar: { type: String, required: false },
        notify: { type: String, nullable: true },
        name: { type: String, nullable: true },
        tagsId: [{ type: Schema.Types.ObjectId, ref: 'tags', nullable: true }],
        tags: [{ type: Object, nullable: true }],
        short: { type: String, nullable: true },
        note: { type: String, nullable: true },
        vname: { type: String, nullable: true },
        verify: { type: String, nullable: true },
        lastSeen: { type: Object, nullable: true },
        lastKnownPresence: { type: Object, nullable: true },
        enterprise: { type: String, nullable: true },
        index: { type: String, nullable: true },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
exports.createValidationSchema = {
    type: 'object',
    required: ['jid', 'connectionId'],
    properties: {
        jid: {
            type: 'string',
        },
        connectionId: {
            type: ['string', 'object'],
        },
        notify: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        avatar: {
            type: 'string',
        },
        short: {
            type: 'string',
        },
        vname: {
            type: 'string',
        },
        verify: {
            type: 'string',
        },
        note: {
            type: 'string',
        },
        index: {
            type: 'string',
        },
        tagsId: {
            type: 'array',
            items: {
                type: ['string', 'object'],
            }
        },
        tags: {
            type: 'array',
            items: {
                type: 'object'
            }
        },
        enterprise: {
            type: 'string',
        },
    }
};
