"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'envs';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const metaLinksSchema = new Schema({
        favicon: { type: Object, required: true },
    });
    const pointSchema = new Schema({
        title: { type: String },
        points: [{ type: String }]
    });
    const supportSchema = new Schema({
        active: { type: Boolean, default: true },
        points: [{ type: Object, nullable: true }],
    });
    const politicsSchema = new Schema({
        active: { type: Boolean, default: true },
        date: { type: String, required: true },
        intro: { type: String, nullable: true },
        points: [{ type: pointSchema, nullable: true }],
    });
    const logrocketSchema = new Schema({
        active: { type: Boolean, default: true },
        code: { type: String },
    });
    const schema = new Schema({
        name: { type: String, required: true },
        frontendUrl: { type: String, required: true },
        formattedName: { type: String, required: true },
        metaLinks: metaLinksSchema,
        support: { type: supportSchema, required: true },
        terms: { type: politicsSchema, required: true },
        privacy: { type: politicsSchema, required: true },
        logrocket: { type: logrocketSchema, required: true },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
