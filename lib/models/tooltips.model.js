"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'tooltips';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        active: { type: Boolean, default: false },
        type: { type: String, default: 'notification' },
        html: { type: String },
        forWho: { type: String, default: 'all' },
        notificationTitle: { type: String },
        notificationText: { type: String },
        title: { type: String },
        images: [{ type: String }],
        connectionsId: [{ type: Schema.Types.ObjectId, ref: 'connections' }],
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
