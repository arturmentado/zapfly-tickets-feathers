"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createValidationSchema = void 0;
function default_1(app) {
    const modelName = 'settings';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        type: { type: String, default: 'connections' },
        userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
        name: { type: String, required: true },
        value: { type: Boolean, default: false },
        meta: { type: Object, default: {} },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
exports.createValidationSchema = {
    type: 'object',
    required: ['name'],
    properties: {
        name: {
            type: 'string'
        },
        userId: {
            type: ['string', 'object']
        },
        type: {
            enum: ['connections', 'system']
        },
        meta: {
            type: 'object'
        },
        value: {
            type: ['boolean']
        }
    }
};
