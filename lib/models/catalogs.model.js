"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app) {
    const modelName = 'catalogs';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const schema = new Schema({
        userId: { type: mongooseClient.Schema.Types.ObjectId, ref: 'users' },
        name: { type: String, unique: true, required: true },
        nameComplete: { type: String },
        waitTime: { type: String },
        open: { type: Boolean, default: false },
        deliveryPrice: { type: String },
        imageCover: { type: String },
        imageAvatar: { type: String },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
