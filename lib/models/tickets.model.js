"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createValidationSchema = void 0;
function default_1(app) {
    const modelName = 'tickets';
    const mongooseClient = app.get('mongooseClient');
    const { Schema } = mongooseClient;
    const contactSchema = new Schema({
        connectionId: { type: Schema.Types.ObjectId, ref: 'connections', required: true },
        jid: { type: String, required: true },
        overrideName: { type: String, nullable: true },
        notify: { type: String, nullable: true },
        name: { type: String, nullable: true },
        short: { type: String, nullable: true },
        vname: { type: String, nullable: true },
        avatar: { type: String, nullable: true },
        note: { type: String, nullable: true },
        verify: { type: String, nullable: true },
        tagsId: [{ type: Schema.Types.ObjectId, ref: 'tags', nullable: true }],
        tags: [{ type: Object, nullable: true }],
        enterprise: { type: String, nullable: true },
    }, {
        timestamps: true
    });
    const schema = new Schema({
        connectionId: { type: Schema.Types.ObjectId, ref: 'connections', required: true },
        contactId: { type: Schema.Types.ObjectId, ref: 'contacts', required: true },
        userId: { type: Schema.Types.ObjectId, ref: 'users', required: true },
        attendantId: { type: Schema.Types.ObjectId, ref: 'users', nullable: true },
        departmentId: { type: Schema.Types.ObjectId, ref: 'departments', nullable: true },
        lastMessage: { type: Object, nullable: true },
        count: { type: Number, default: 0 },
        messageTimestamp: { type: Number, default: 0 },
        contact: { type: contactSchema },
        jid: { type: String, required: true },
        reason: { type: String },
        status: { type: String, required: false, default: 'waiting' },
    }, {
        timestamps: true
    });
    // This is necessary to avoid model compilation errors in watch mode
    // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
    if (mongooseClient.modelNames().includes(modelName)) {
        mongooseClient.deleteModel(modelName);
    }
    return mongooseClient.model(modelName, schema);
}
exports.default = default_1;
exports.createValidationSchema = {
    type: 'object',
    required: ['connectionId', 'contactId', 'jid'],
    properties: {
        attendantId: {
            type: ['string', 'object']
        },
        departmentId: {
            type: ['string', 'object']
        },
        connectionId: {
            type: ['string', 'object']
        },
        contactId: {
            type: ['string', 'object']
        },
        count: {
            type: ['number']
        },
        jid: {
            type: ['string', 'object']
        },
        status: {
            type: 'string',
            enum: ['pre_service', 'waiting', 'in_progress', 'closed', 'on_hold']
        },
    }
};
