"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_schedule_1 = __importDefault(require("node-schedule"));
const system_schedules_1 = __importDefault(require("./system-schedules"));
const connections_schedules_1 = __importDefault(require("./connections-schedules"));
const notification_schedules_1 = __importDefault(require("./notification-schedules"));
function default_1(app) {
    system_schedules_1.default(app, node_schedule_1.default);
    connections_schedules_1.default(app, node_schedule_1.default);
    notification_schedules_1.default(app, node_schedule_1.default);
}
exports.default = default_1;
