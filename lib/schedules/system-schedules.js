"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(app, sheduler) {
    const closingTickets = sheduler.scheduleJob('*/5 * * * * *', async function () {
        const tickets = await app.service('tickets')
            .find({
            query: {
                $select: ['_id', 'status', 'updatedAt'],
                status: 'closing',
                updatedAt: {
                    $lt: new Date(new Date().getTime() - 5 * 60000) // only if it's as old as 5 minutes
                },
            }
        });
        if (!tickets.total)
            return;
        await app.service('tickets')
            .patch(null, {
            status: 'closed'
        }, {
            query: {
                _id: {
                    $in: tickets.data.map((e) => e._id)
                }
            }
        });
    });
    console.log('=> System Schedules created');
}
exports.default = default_1;
