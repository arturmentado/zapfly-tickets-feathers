"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
function getName(ticket) {
    if (ticket.contact.overrideName)
        return ticket.contact.overrideName;
    if (ticket.contact.name)
        return ticket.contact.name;
    if (ticket.contact.notify)
        return ticket.contact.notify;
    if (ticket.contact.vname)
        return ticket.contact.vname;
    return ticket.contact.jid;
}
async function sendNot(app, url, headers, pagination = { $limit: 10, $skip: 0 }) {
    try {
        const status = app.get('tickets').notificationStatus;
        const tm = Math.floor((new Date().getTime() / 1000) + 60 * 1);
        const findPayload = {
            query: {
                ...pagination,
                status: { $in: status },
                'lastMessage.key.fromMe': false,
                'lastMessage.messageTimestamp': { $lt: tm },
                $or: [
                    { 'lastMessage.sentWebNotification': false },
                    { 'lastMessage.sentWebNotification': null },
                    { 'lastMessage.sentWebNotification': undefined },
                ]
            }
        };
        const tickets = await app.service('tickets').find(findPayload);
        for (let index = 0; index < tickets.data.length; index++) {
            const ticket = tickets.data[index];
            const title = `${getName(ticket)} ainda não foi respondido`;
            const service = `zapfly_atd${process.env.NODE_ENV !== 'production' ? '_dev' : ''}`;
            console.log(service);
            const payload = {
                'subject': 'mailto:zapfly@arcode.online',
                title,
                'notification': {
                    'body': 'Clique aqui para responder',
                    'badge': `${app.get('frontendDomain')}/logo_512.png`,
                    'icon': `${app.get('frontendDomain')}/logo_512.png`,
                    'url': `${app.get('frontendDomain')}/dashboard/a/${ticket._id}`
                },
                'selector': {
                    service,
                    'extraData.userId': ticket.userId,
                }
            };
            if (ticket.status === 'in_progress') {
                const user = await app.service('users').get(ticket.attendantId, { query: { $select: ['_id', 'notificationExtra'] } });
                await app.service('tickets').patch(ticket._id, { 'lastMessage.sentWebNotification': true });
                if (!user.notificationExtra)
                    continue;
                payload.selector.userId = ticket.attendantId;
            }
            else {
                const users = await app.service('users').find({ paginate: false, query: { $select: ['_id', 'notificationExtra'], userId: ticket.userId, notificationExtra: true } });
                await app.service('tickets').patch(ticket._id, { 'lastMessage.sentWebNotification': true });
                if (!users.length)
                    continue;
                payload.selector.userId = { $in: users.map((e) => e._id) };
            }
            console.log('Sending Web Not...', payload);
            axios_1.default.post(url, payload, { headers }).catch(e => { console.log(e); return e; });
        }
        if (tickets.total > tickets.data.length + tickets.skip) {
            sendNot(app, url, headers, pagination);
        }
    }
    catch (error) {
        console.log('notification shedules error', error);
    }
}
function default_1(app, sheduler) {
    const apiToken = app.get('toolsApi').token;
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${apiToken}`
    };
    const url = 'https://tools.arcode.online/push';
    const notificationSchedules = sheduler.scheduleJob('*/1 * * * *', function () {
        sendNot(app, url, headers);
    });
    const nextRun = notificationSchedules.nextInvocation();
    console.log('=> Notification Schedules created', nextRun.toLocaleString('pt-BR'));
}
exports.default = default_1;
