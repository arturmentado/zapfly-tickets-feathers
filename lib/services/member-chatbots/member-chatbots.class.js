"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberChatbots = void 0;
class MemberChatbots {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const user = params.user;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        delete params.provider;
        return this.app.service('chatbots').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const user = params.user;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        delete params.provider;
        return this.app.service('chatbots').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params.user;
        data.userId = user.userId;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        delete params.provider;
        return this.app.service('chatbots').create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        const user = params.user;
        data.userId = user.userId;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        delete params.provider;
        return this.app.service('chatbots').update(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        const user = params.user;
        data.userId = user.userId;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        delete params.provider;
        return this.app.service('chatbots').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        const user = params.user;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        delete params.provider;
        return this.app.service('chatbots').remove(id, params);
    }
}
exports.MemberChatbots = MemberChatbots;
