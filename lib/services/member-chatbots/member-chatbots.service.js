"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_chatbots_class_1 = require("./member-chatbots.class");
const member_chatbots_hooks_1 = __importDefault(require("./member-chatbots.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-chatbots', new member_chatbots_class_1.MemberChatbots(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-chatbots');
    service.hooks(member_chatbots_hooks_1.default);
}
exports.default = default_1;
