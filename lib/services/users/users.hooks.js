"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const feathersAuthentication = __importStar(require("@feathersjs/authentication"));
const local = __importStar(require("@feathersjs/authentication-local"));
const ajv_1 = __importDefault(require("../../lib/ajv"));
const users_model_1 = require("../../models/users.model");
const feathers_permissions_1 = __importDefault(require("feathers-permissions"));
const feathers_hooks_common_1 = require("feathers-hooks-common");
// Don't remove this comment. It's needed to format import lines nicely.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search = require('feathers-mongodb-fuzzy-search');
const { authenticate } = feathersAuthentication.hooks;
const { hashPassword, protect } = local.hooks;
exports.default = {
    before: {
        all: [],
        find: [
            feathers_hooks_common_1.disallow('external'),
            authenticate('jwt'),
            search({ escape: true }),
            search({
                fields: [
                    'name',
                    'lastName',
                    'email'
                ]
            }),
        ],
        get: [authenticate('jwt')],
        create: [feathers_hooks_common_1.disallow('external'), authenticate('jwt'), feathers_permissions_1.default({ roles: ['super-admin'] }), hashPassword('password'), ajv_1.default(users_model_1.createValidationSchema)],
        update: [feathers_hooks_common_1.disallow('external'), hashPassword('password'), authenticate('jwt')],
        patch: [feathers_hooks_common_1.disallow('external'), hashPassword('password'), authenticate('jwt')],
        remove: [feathers_hooks_common_1.disallow('external'), authenticate('jwt')]
    },
    after: {
        all: [
            // Make sure the password field is never sent to the client
            // Always must be the last hook
            protect('password'),
        ],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
