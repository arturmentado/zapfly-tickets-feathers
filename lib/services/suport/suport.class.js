"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Suport = void 0;
class Suport {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.connectionId = String(app.get('mainConnectionId'));
        // this.connectionId = '6086deef10084823f6c0c1b8';
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        try {
            const { number, form, point } = data;
            let connectionId = this.connectionId;
            if (process.env.TESTING) {
                const conn = await this.app.service('connections').find();
                connectionId = conn.data[0]._id;
            }
            const connection = await this.app.service('connections').get(connectionId, { admin: true });
            const userId = connection.userId;
            const contactParam = {
                query: {
                    connectionId,
                    jid: `${number}@s.whatsapp.net`
                }
            };
            const contacts = await this.app.service('contacts').find(contactParam);
            let contact;
            if (contacts.total)
                contact = contacts.data[0];
            else
                contact = await this.app.service('contacts').create(contactParam.query);
            const ticketParam = {
                query: {
                    connectionId,
                    contactId: contact._id,
                    status: {
                        $in: ['in_progress', 'waiting']
                    },
                    jid: `${number}@s.whatsapp.net`
                }
            };
            const tickets = await this.app.service('tickets').find(ticketParam);
            let ticket;
            if (tickets.total)
                ticket = tickets.data[0];
            else
                ticket = await this.app.service('tickets')
                    .create({
                    connectionId,
                    userId,
                    contactId: contact._id,
                    contact: contact,
                    count: 1,
                    status: 'waiting',
                    jid: `${number}@s.whatsapp.net`
                });
            let conversation = `Pedido de suporte: ${number}\n`;
            if (point && point.title) {
                conversation += '----\n';
                conversation += 'point:\n';
                conversation += `${point.title}\n`;
                conversation += `${point.description.replace(/<[^>]*>?/gm, '')}\n`;
            }
            if (form && form.input) {
                conversation += '----\n';
                conversation += `Form: ${form.input}\n`;
            }
            const message = await this.app.service('messages')
                .create({
                connectionId,
                contactId: contact._id,
                ticketId: ticket._id,
                key: {
                    fromMe: false,
                    jid: `${number}@s.whatsapp.net`
                },
                messageTimestamp: new Date().getTime() / 1000,
                message: {
                    conversation,
                },
            });
            await this.app.service('tickets')
                .patch(ticket._id, {
                lastMessage: message,
                unread: 1,
                messageTimestamp: message.messageTimestamp,
            });
            return { success: true };
        }
        catch (error) {
            console.error('support create', error);
            throw { error: true };
        }
    }
}
exports.Suport = Suport;
