"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const suport_class_1 = require("./suport.class");
const suport_hooks_1 = __importDefault(require("./suport.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/suport', new suport_class_1.Suport(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('suport');
    service.hooks(suport_hooks_1.default);
}
exports.default = default_1;
