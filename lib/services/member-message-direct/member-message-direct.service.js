"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_message_direct_class_1 = require("./member-message-direct.class");
const member_message_direct_hooks_1 = __importDefault(require("./member-message-direct.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-message-direct', new member_message_direct_class_1.MemberMessageDirect(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-message-direct');
    service.hooks(member_message_direct_hooks_1.default);
}
exports.default = default_1;
