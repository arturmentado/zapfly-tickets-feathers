"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberMessageDirect = void 0;
class MemberMessageDirect {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a, _b;
        const connsId = JSON.parse(JSON.stringify((_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!((_b = params.query) === null || _b === void 0 ? void 0 : _b.connectionId))
            throw new Error('Connection invalid');
        const connectionId = params.query.connectionId;
        if (!connsId.includes(connectionId))
            throw new Error('Connection invalid');
        const connection = await this.app.service('member-connections').get(connectionId, params);
        if (connection.useMultidevice)
            throw new Error('Using multidevices');
        delete params.provider;
        // http://localhost:3001/messages-query?action=loadMessages&parameters[]=5516996160622@c.us&parameters[]=25
        // http://localhost:3001/messages-query?action=searchMessages&parameters[]=%27Teste%27&parameters[]=5516996160622@c.us&parameters[]=25&parameters[]=1
        return this.app.service('messages-remote').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        return {
            id, text: `A new message with ID: ${id}!`
        };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        if (Array.isArray(data)) {
            return Promise.all(data.map(current => this.create(current, params)));
        }
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.MemberMessageDirect = MemberMessageDirect;
