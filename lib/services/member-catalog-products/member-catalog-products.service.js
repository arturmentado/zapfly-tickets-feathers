"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_catalog_products_class_1 = require("./member-catalog-products.class");
const member_catalog_products_hooks_1 = __importDefault(require("./member-catalog-products.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-catalog-products', new member_catalog_products_class_1.MemberCatalogProducts(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-catalog-products');
    service.hooks(member_catalog_products_hooks_1.default);
}
exports.default = default_1;
