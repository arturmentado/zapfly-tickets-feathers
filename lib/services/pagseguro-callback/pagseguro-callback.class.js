"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagseguroCallback = void 0;
class PagseguroCallback {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        if (!data)
            return data;
        if (!data.notificationCode)
            return data;
        try {
            const not = await this.app.service('pagseguro-notification').get(data.notificationCode.split('-').join(''));
            const { code, status, reference } = not.data;
            const t = await this.app.service('connections').patch(reference, { 'preapprovalPlan.status': status }, { query: { 'preapprovalPlan.code': code } });
        }
        catch (error) {
            console.error(error.response);
        }
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.PagseguroCallback = PagseguroCallback;
