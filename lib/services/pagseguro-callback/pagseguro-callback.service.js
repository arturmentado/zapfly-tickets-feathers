"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pagseguro_callback_class_1 = require("./pagseguro-callback.class");
const pagseguro_callback_hooks_1 = __importDefault(require("./pagseguro-callback.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/external/pagseguro', new pagseguro_callback_class_1.PagseguroCallback(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('external/pagseguro');
    service.hooks(pagseguro_callback_hooks_1.default);
}
exports.default = default_1;
