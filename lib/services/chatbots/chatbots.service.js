"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chatbots_class_1 = require("./chatbots.class");
const chatbots_model_1 = __importDefault(require("../../models/chatbots.model"));
const chatbots_hooks_1 = __importDefault(require("./chatbots.hooks"));
function default_1(app) {
    const options = {
        Model: chatbots_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/chatbots', new chatbots_class_1.Chatbots(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('chatbots');
    service.hooks(chatbots_hooks_1.default);
}
exports.default = default_1;
