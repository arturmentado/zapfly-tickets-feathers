"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Connections = void 0;
const feathers_mongoose_1 = require("feathers-mongoose");
const mongoose_1 = require("mongoose");
class Connections extends feathers_mongoose_1.Service {
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(options, app) {
        super(options);
        this.app = app;
    }
    async get(id, params) {
        if (params && params.admin)
            return super.get(id, params);
        let query = {
            jid: id
        };
        let notId = false;
        if (params && params.query && params.query.notId)
            notId = params.query.notId;
        if (mongoose_1.Types.ObjectId.isValid(id) && !notId) {
            query = {
                _id: id
            };
        }
        // console.log(query, Object.getOwnPropertyNames(id), Types.ObjectId.isValid(id));
        const env = await this.Model.findOne(query);
        return env;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const result = await super.create(data, params);
        const users = await this.app.service('users')
            .find({
            $select: ['_id', 'connsId'],
            query: { userId: data.userId },
            $limit: -1
        });
        if (users.total) {
            users.data.forEach(async (e) => {
                const connsId = [...e.connsId, result._id.toString()].filter(e => e);
                await this.app.service('users')
                    .patch(e._id, {
                    connsId: [...new Set(connsId)]
                });
            });
        }
        return result;
    }
}
exports.Connections = Connections;
