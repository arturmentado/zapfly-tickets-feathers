"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connections_class_1 = require("./connections.class");
const connections_model_1 = __importDefault(require("../../models/connections.model"));
const connections_hooks_1 = __importDefault(require("./connections.hooks"));
function default_1(app) {
    const options = {
        Model: connections_model_1.default(app),
        whitelist: ['$text', '$search', '$regex', '$populate', '$options'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connections', new connections_class_1.Connections(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connections');
    service.hooks(connections_hooks_1.default);
}
exports.default = default_1;
