"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_contacts_class_1 = require("./member-contacts.class");
const member_contacts_hooks_1 = __importDefault(require("./member-contacts.hooks"));
function default_1(app) {
    const options = {
        whitelist: ['$text', '$search', '$regex'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-contacts', new member_contacts_class_1.MemberContacts(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-contacts');
    service.hooks(member_contacts_hooks_1.default);
}
exports.default = default_1;
