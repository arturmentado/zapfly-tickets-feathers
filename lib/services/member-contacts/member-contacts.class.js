"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberContacts = void 0;
class MemberContacts {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        const connsId = (_a = params.user) === null || _a === void 0 ? void 0 : _a.connsId;
        const query = params.query || {};
        params.query = {
            ...query,
            connectionId: {
                $in: connsId
            }
        };
        delete params.provider;
        return this.app.service('contacts').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a;
        const connsId = (_a = params.user) === null || _a === void 0 ? void 0 : _a.connsId;
        const query = params.query || {};
        params.query = {
            ...query,
            connectionId: {
                $in: connsId
            }
        };
        delete params.provider;
        return this.app.service('contacts').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a;
        const connsId = JSON.parse(JSON.stringify((_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!connsId.includes(data.connectionId))
            throw new Error('');
        delete params.provider;
        return this.app.service('contacts').create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        var _a;
        const connsId = (_a = params.user) === null || _a === void 0 ? void 0 : _a.connsId;
        const query = params.query || {};
        params.query = {
            $and: [
                query,
                {
                    connectionId: {
                        $in: connsId
                    }
                }
            ]
        };
        delete params.provider;
        return this.app.service('contacts').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.MemberContacts = MemberContacts;
