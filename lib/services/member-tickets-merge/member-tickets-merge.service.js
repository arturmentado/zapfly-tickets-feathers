"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_tickets_merge_class_1 = require("./member-tickets-merge.class");
const member_tickets_merge_hooks_1 = __importDefault(require("./member-tickets-merge.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-tickets/merge', new member_tickets_merge_class_1.MemberTicketsMerge(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-tickets/merge');
    service.hooks(member_tickets_merge_hooks_1.default);
}
exports.default = default_1;
