"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_controller_class_1 = require("./connection-controller.class");
const connection_controller_hooks_1 = __importDefault(require("./connection-controller.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connection-controller', new connection_controller_class_1.ConnectionController(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connection-controller');
    service.hooks(connection_controller_hooks_1.default);
}
exports.default = default_1;
