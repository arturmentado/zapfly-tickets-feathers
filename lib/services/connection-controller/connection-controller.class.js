"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionController = void 0;
const errors_1 = require("@feathersjs/errors");
class ConnectionController {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        return [];
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        return {
            id, text: `A new message with ID: ${id}!`
        };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        var _a, _b, _c, _d;
        const user = params.user;
        let target;
        if (!user.permissions.includes('super-admin')) {
            target = await this.app.service('member-connections').get(id, params);
            if (!target)
                throw new errors_1.NotFound();
            target = await this.app.service('connections').get(id, { admin: true });
        }
        else {
            target = await this.app.service('connections').get(id, { admin: true });
        }
        await this.app.service('connections').patch(target._id, { status: 'opening' });
        if (target.port) {
            try {
                await this.app.service('controller-remote').remove(target.jid, { useMultidevice: target.useMultidevice });
            }
            catch (error) {
            }
        }
        const payload = {
            waitChats: (_a = !target.disableWaitChats) !== null && _a !== void 0 ? _a : true,
            sessionData: target.useMultidevice ? true : target.sessionData,
            disableAutoImportUnreadMessages: (_b = target.disableAutoImportUnreadMessages) !== null && _b !== void 0 ? _b : true,
            disableForceUseHere: (_c = target.disableForceUseHere) !== null && _c !== void 0 ? _c : true,
            useMultidevice: (_d = target.useMultidevice) !== null && _d !== void 0 ? _d : false
        };
        if (target.port) {
            payload.port = target.port;
        }
        await this.app.service('controller-remote')
            .update(target.jid, payload);
        return { success: true };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        const user = params.user;
        let target;
        if (params.provider && !user.permissions.includes('super-admin')) {
            target = await this.app.service('member-connections').get(id, params);
            if (!target)
                throw new errors_1.NotFound();
        }
        else {
            target = await this.app.service('connections').get(id, { admin: true });
        }
        await this.app.service('connections').patch(target._id, { status: 'REMOVED', qrcode: null });
        await this.app.service('controller-remote').remove(target.jid, { useMultidevice: target.useMultidevice });
        return { success: true };
    }
}
exports.ConnectionController = ConnectionController;
