"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Members = void 0;
const errors_1 = require("@feathersjs/errors");
class Members {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const user = params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            ghost: {
                $ne: true
            },
            userId: user.userId
        };
        return this.app.service('users').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const user = params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            ghost: {
                $ne: true
            },
            userId: user.userId
        };
        return this.app.service('users').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a;
        data.userId = (_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.userId;
        const conns = await this.app.service('connections')
            .find({
            $select: ['_id'],
            query: { userId: data.userId },
            $limit: -1
        });
        data.connsId = conns.data.map(e => e._id);
        return this.app.service('users').create(data);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        const target = this.get(id, params);
        if (!target)
            throw new errors_1.NotFound();
        return this.app.service('users').update(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        var _a, _b, _c;
        const perms = (_a = params.user) === null || _a === void 0 ? void 0 : _a.permissions;
        const userId = (_b = params.user) === null || _b === void 0 ? void 0 : _b.userId;
        delete data.ghost;
        params.query = {
            ...params.query,
            userId
        };
        if (!perms.includes('admin', 'supervisor')) {
            if (!((_c = params.user) === null || _c === void 0 ? void 0 : _c._id.equals(id)))
                throw new Error('not allowed');
        }
        delete params.provider;
        return this.app.service('users').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.Members = Members;
