"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const members_class_1 = require("./members.class");
const members_hooks_1 = __importDefault(require("./members.hooks"));
function default_1(app) {
    const options = {
        whitelist: ['$text', '$search', '$regex'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/members', new members_class_1.Members(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('members');
    service.hooks(members_hooks_1.default);
}
exports.default = default_1;
