"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_users_class_1 = require("./admin-users.class");
const admin_users_hooks_1 = __importDefault(require("./admin-users.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate'),
        whitelist: ['$populate', '$text', '$search', '$regex', '$options'],
    };
    // Initialize our service with any options it requires
    app.use('/admin-users', new admin_users_class_1.AdminUsers(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin-users');
    service.hooks(admin_users_hooks_1.default);
}
exports.default = default_1;
