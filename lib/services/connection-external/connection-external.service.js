"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_external_class_1 = require("./connection-external.class");
const connection_external_hooks_1 = __importDefault(require("./connection-external.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connection-external', new connection_external_class_1.ConnectionExternal(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connection-external');
    service.hooks(connection_external_hooks_1.default);
}
exports.default = default_1;
