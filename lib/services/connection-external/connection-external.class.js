"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionExternal = void 0;
class ConnectionExternal {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        const sessionId = (_a = params.headers) === null || _a === void 0 ? void 0 : _a.session_id;
        const target = await this.app.service('connections').get(sessionId, { query: { notId: true } });
        if (!target)
            throw new Error();
        return target;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        return {
            id, text: `A new message with ID: ${id}!`
        };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        if (Array.isArray(data)) {
            return Promise.all(data.map(current => this.create(current, params)));
        }
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        const sessionId = String(data.sessionId);
        const target = await this.app.service('connections').get(sessionId, { query: { notId: true } });
        if (!target)
            throw new Error();
        if (data.status && data.status !== 'qrcode' && !data.qrcode) {
            data.qrcode = null;
        }
        if (data.status === 'open') {
            await this.app.service('connections-validate').get(sessionId);
            const messagesParams = {
                paginate: false,
                query: {
                    $select: [
                        '_id',
                        'key',
                        'status',
                        'connectionId',
                        'contactId',
                        'message'
                    ],
                    $populate: ['contactId'],
                    connectionId: target._id,
                    status: 'PENDING'
                }
            };
            const messages = await this.app.service('messages').find(messagesParams);
            for (let index = 0; index < messages.length; index++) {
                const message = messages[index];
                if (!message.contactId.jid)
                    continue;
                if (!message.key.id)
                    continue;
                if (!target.useMultidevice) {
                    const checkRequestParams = {
                        connection: target,
                        query: {
                            action: 'loadMessage',
                            parameters: [
                                message.contactId.jid,
                                message.key.id
                            ]
                        }
                    };
                    const checkRequest = await this.app.service('messages-remote').find(checkRequestParams)
                        .catch(() => { return; });
                    if (!checkRequest || checkRequest.key.id === message.key.id)
                        continue;
                }
                const type = Object.keys(message.message)[0]; // building for whatsapp
                const messageSend = {
                    id: message.contactId.jid,
                    messageId: message.key.id,
                    connectionId: target._id,
                    message: message.message[type],
                    type
                };
                if (message.message[type].originalUrl) {
                    messageSend.mimetype = message.message[type].originalMimetype;
                    messageSend.url = message.message[type].originalUrl;
                }
                if (message.message.originalQuoted) {
                    messageSend.quoted = message.message.originalQuoted;
                }
                await this.app.service('messages-remote').create(messageSend, {})
                    .catch(console.error);
            }
        }
        return this.app.service('connections').patch(target._id, data);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.ConnectionExternal = ConnectionExternal;
