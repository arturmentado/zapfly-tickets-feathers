"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tooltips_read_class_1 = require("./tooltips-read.class");
const tooltips_read_model_1 = __importDefault(require("../../models/tooltips-read.model"));
const tooltips_read_hooks_1 = __importDefault(require("./tooltips-read.hooks"));
function default_1(app) {
    const options = {
        Model: tooltips_read_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/tooltips-read', new tooltips_read_class_1.TooltipsRead(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('tooltips-read');
    service.hooks(tooltips_read_hooks_1.default);
}
exports.default = default_1;
