"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicChannelControl = void 0;
class PublicChannelControl {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    async create(data, params) {
        return { name: data.name };
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.PublicChannelControl = PublicChannelControl;
