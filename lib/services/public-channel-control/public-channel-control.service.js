"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const public_channel_control_class_1 = require("./public-channel-control.class");
const public_channel_control_hooks_1 = __importDefault(require("./public-channel-control.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/public-channel-control', new public_channel_control_class_1.PublicChannelControl(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('public-channel-control');
    service.hooks(public_channel_control_hooks_1.default);
}
exports.default = default_1;
