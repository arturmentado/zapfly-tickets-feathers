"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_channel_control_class_1 = require("./admin-channel-control.class");
const admin_channel_control_hooks_1 = __importDefault(require("./admin-channel-control.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/admin-channel-control', new admin_channel_control_class_1.AdminChannelControl(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin-channel-control');
    service.hooks(admin_channel_control_hooks_1.default);
}
exports.default = default_1;
