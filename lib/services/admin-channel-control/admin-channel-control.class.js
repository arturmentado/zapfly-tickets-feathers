"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminChannelControl = void 0;
class AdminChannelControl {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a;
        const userId = (_a = params.user) === null || _a === void 0 ? void 0 : _a._id.toString();
        const connection = this.app.channel('authenticated').connections
            .find(connection => connection.user._id.toString() === userId);
        if (!connection)
            throw new Error('Not found connection');
        this.app.channel(id).join(connection);
        return { success: true };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        var _a;
        const userId = (_a = params.user) === null || _a === void 0 ? void 0 : _a._id.toString();
        const connection = this.app.channel('authenticated').connections
            .find(connection => connection.user._id.toString() === userId);
        if (!connection)
            throw new Error('Not found connection');
        this.app.channel(id).leave(connection);
        return { success: true };
    }
}
exports.AdminChannelControl = AdminChannelControl;
