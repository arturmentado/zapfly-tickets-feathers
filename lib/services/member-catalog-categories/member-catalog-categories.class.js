"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberCatalogCategories = void 0;
class MemberCatalogCategories {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    remove(id, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalog-categories').remove(id, params);
    }
    async find(params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalog-categories').find(params);
    }
    async get(id, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalog-categories').get(id, params);
    }
    async update(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        data.userId = user.userId;
        return this.app.service('catalog-categories').update(id, data, params);
    }
    async patch(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalog-categories').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        data.userId = user.userId;
        let catalog = await this.app.service('member-catalogs').find(params);
        if (!catalog.total)
            throw new Error('catalog non existent');
        catalog = catalog.data[0];
        data.catalogId = catalog._id;
        data.catalogName = catalog.name;
        delete params.provider;
        return this.app.service('catalog-categories').create(data, params);
    }
}
exports.MemberCatalogCategories = MemberCatalogCategories;
