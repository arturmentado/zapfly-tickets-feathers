"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_settings_class_1 = require("./member-settings.class");
const member_settings_hooks_1 = __importDefault(require("./member-settings.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-settings', new member_settings_class_1.MemberSettings(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-settings');
    service.hooks(member_settings_hooks_1.default);
}
exports.default = default_1;
