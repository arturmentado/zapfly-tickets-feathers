"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberSettings = void 0;
class MemberSettings {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const user = params.user;
        const userId = user === null || user === void 0 ? void 0 : user.userId;
        params.query = {
            ...params.query,
            userId,
            type: 'connections'
        };
        delete params.provider;
        return this.app.service('settings').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const user = params.user;
        const userId = user === null || user === void 0 ? void 0 : user.userId;
        params.query = {
            ...params.query,
            userId,
            type: 'connections'
        };
        delete params.provider;
        return this.app.service('settings').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params.user;
        const userId = user === null || user === void 0 ? void 0 : user.userId;
        data.userId = userId;
        data.type = 'connections';
        const check = await this.app.service('settings').find({ paginate: false, query: { name: data.name, type: 'connection', userId } });
        if (check.total)
            throw new Error('already exists');
        delete params.provider;
        return this.app.service('settings').create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        const user = params.user;
        const userId = user === null || user === void 0 ? void 0 : user.userId;
        params.query = {
            ...params.query,
            userId,
            type: 'connections'
        };
        delete data.userId;
        delete data.type;
        delete data.name;
        delete params.provider;
        return this.app.service('settings').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.MemberSettings = MemberSettings;
