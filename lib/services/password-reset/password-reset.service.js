"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const password_reset_class_1 = require("./password-reset.class");
const password_reset_hooks_1 = __importDefault(require("./password-reset.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/password-reset', new password_reset_class_1.PasswordReset(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('password-reset');
    service.hooks(password_reset_hooks_1.default);
}
exports.default = default_1;
