"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordReset = void 0;
const crypto_1 = __importDefault(require("crypto"));
const errors_1 = require("@feathersjs/errors");
class PasswordReset {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        const target = await this.app.service('users').find({ paginate: false, query: { passwordResetToken: data.token } });
        if (!target || !target.length)
            return new errors_1.NotFound();
        const user = target[0];
        return this.app.service('users').patch(user._id, { passwordResetToken: null, password: data.password });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const target = await this.app.service('users').find({ query: { passwordResetToken: id }, paginate: false });
        if (!target || !target.length)
            return new errors_1.NotFound();
        const user = target[0];
        return { token: user.token };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a;
        const target = await this.app.service('users').find({ paginate: false, query: { email: data.email } });
        if (!target || !target.length)
            return {};
        const user = target[0];
        const env = await this.app.service('envs').get('zapfly');
        const token = await new Promise((resolve) => {
            crypto_1.default.randomBytes(32, function (ex, buf) {
                resolve(buf.toString('hex'));
            });
        });
        // eslint-disable-next-line semi
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const createEmail = require('../../lib/createEmailTemplate').default;
        let sendMessage = true;
        await this.app.service('users').patch(user._id, { passwordResetToken: token });
        const connections = await this.app.service('connections').find({ query: { _id: { $in: user.connsId }, 'meta.validatedJid': true, $select: ['_id', 'meta', 'jid'] } });
        let connectionId, jid;
        if ((_a = connections === null || connections === void 0 ? void 0 : connections.data[0]) === null || _a === void 0 ? void 0 : _a._id) {
            connectionId = connections.data[0]._id;
            jid = `${connections.data[0].jid}@s.whatsapp.net`;
        }
        else {
            sendMessage = false;
        }
        const url = `${env.frontendUrl}/password-reset?token=${token}`;
        const content = `Olá, ${user.name}<br><br><a href="${url}">Clique aqui link para resetar sua senha</a>`;
        const html = createEmail({ logo: 'https://atd.zapfly.com.br/email.png', content });
        const email = {
            'to': user.email,
            'subject': 'Sobre sua conta da ZapFly',
            html
        };
        const message = `Olá, o atendente ${user.name} esqueceu sua conta para a plataforma, estamos enviando um link para o email e para seu numero cadastrado. \n\nPara adicionar uma nova senha para a conta de email: *${user.email}* clique neste link: ${url}`;
        const id = `3EB0${crypto_1.default.randomBytes(4).toString('hex').toUpperCase()}`;
        if (sendMessage) {
            const messageSend = {
                id: jid,
                messageId: id,
                connectionId: String(this.app.get('mainConnectionId')),
                message: message,
                type: 'conversation'
            };
            await this.app.service('messages-remote').create(messageSend, {});
        }
        this.app.service('mailer').create(email);
        return { success: true };
    }
}
exports.PasswordReset = PasswordReset;
