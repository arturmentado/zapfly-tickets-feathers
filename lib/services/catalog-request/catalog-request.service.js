"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catalog_request_class_1 = require("./catalog-request.class");
const catalog_request_hooks_1 = __importDefault(require("./catalog-request.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/catalog-request', new catalog_request_class_1.CatalogRequest(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('catalog-request');
    service.hooks(catalog_request_hooks_1.default);
}
exports.default = default_1;
