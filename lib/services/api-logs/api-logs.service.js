"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_logs_class_1 = require("./api-logs.class");
const api_logs_model_1 = __importDefault(require("../../models/api-logs.model"));
const api_logs_hooks_1 = __importDefault(require("./api-logs.hooks"));
function default_1(app) {
    const options = {
        Model: api_logs_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/api-logs', new api_logs_class_1.ApiLogs(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('api-logs');
    service.hooks(api_logs_hooks_1.default);
}
exports.default = default_1;
