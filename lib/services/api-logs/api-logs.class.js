"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiLogs = void 0;
const feathers_mongoose_1 = require("feathers-mongoose");
class ApiLogs extends feathers_mongoose_1.Service {
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(options, app) {
        super(options);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        if (!params.query)
            params.query = {};
        params.query = {
            ...params.query,
            userId: (_a = params.user) === null || _a === void 0 ? void 0 : _a._id
        };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a;
        data.userId = (_a = params.user) === null || _a === void 0 ? void 0 : _a.id;
        return this._create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.ApiLogs = ApiLogs;
