"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connections_remote_version_class_1 = require("./connections-remote-version.class");
const connections_remote_version_hooks_1 = __importDefault(require("./connections-remote-version.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connections-remote-version', new connections_remote_version_class_1.ConnectionsRemoteVersion(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connections-remote-version');
    service.hooks(connections_remote_version_hooks_1.default);
}
exports.default = default_1;
