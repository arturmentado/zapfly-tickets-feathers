"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionsRemoteVersion = void 0;
const errors_1 = require("@feathersjs/errors");
const axios_1 = __importDefault(require("axios"));
class ConnectionsRemoteVersion {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.baseUrl = app.get('whatsappBase');
        this.baseUrlMd = app.get('whatsappBaseMd');
    }
    create(data, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        return [];
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const target = await this.app.service('member-connections').get(id, params);
        if (!target)
            throw new errors_1.NotFound();
        const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/connection-version`;
        try {
            const { data } = await axios_1.default.get(url);
            return data;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
}
exports.ConnectionsRemoteVersion = ConnectionsRemoteVersion;
