"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const envs_class_1 = require("./envs.class");
const envs_model_1 = __importDefault(require("../../models/envs.model"));
const envs_hooks_1 = __importDefault(require("./envs.hooks"));
function default_1(app) {
    const options = {
        Model: envs_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/envs', new envs_class_1.Envs(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('envs');
    service.hooks(envs_hooks_1.default);
}
exports.default = default_1;
