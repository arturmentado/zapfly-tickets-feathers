"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Envs = void 0;
const feathers_mongoose_1 = require("feathers-mongoose");
class Envs extends feathers_mongoose_1.Service {
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(options, app) {
        super(options);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(name, params) {
        return this.Model.findOne({ name });
    }
}
exports.Envs = Envs;
