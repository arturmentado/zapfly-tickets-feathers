"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const feathers_permissions_1 = __importDefault(require("feathers-permissions"));
const feathers_hooks_common_1 = require("feathers-hooks-common");
// Don't remove this comment. It's needed to format import lines nicely.
exports.default = {
    before: {
        all: [],
        find: [feathers_hooks_common_1.disallow('external')],
        get: [],
        create: [feathers_hooks_common_1.disallow('external')],
        update: [feathers_hooks_common_1.disallow('external')],
        patch: [feathers_permissions_1.default({ roles: ['super-admin'] })],
        remove: [feathers_hooks_common_1.disallow('external')]
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
