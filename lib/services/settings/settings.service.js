"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const settings_class_1 = require("./settings.class");
const settings_model_1 = __importDefault(require("../../models/settings.model"));
const settings_hooks_1 = __importDefault(require("./settings.hooks"));
function default_1(app) {
    const options = {
        Model: settings_model_1.default(app),
        multi: ['patch'],
        // paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/settings', new settings_class_1.Settings(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('settings');
    service.hooks(settings_hooks_1.default);
}
exports.default = default_1;
