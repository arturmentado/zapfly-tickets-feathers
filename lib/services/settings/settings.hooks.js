"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("../../lib/ajv"));
const settings_model_1 = require("../../models/settings.model");
const feathers_hooks_common_1 = require("feathers-hooks-common");
// Don't remove this comment. It's needed to format import lines nicely.
exports.default = {
    before: {
        all: [feathers_hooks_common_1.disallow('external')],
        find: [],
        get: [],
        create: [ajv_1.default(settings_model_1.createValidationSchema)],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
