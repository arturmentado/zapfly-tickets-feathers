"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const controller_remote_class_1 = require("./controller-remote.class");
const controller_remote_hooks_1 = __importDefault(require("./controller-remote.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/controller-remote', new controller_remote_class_1.ControllerRemote(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('controller-remote');
    service.hooks(controller_remote_hooks_1.default);
}
exports.default = default_1;
