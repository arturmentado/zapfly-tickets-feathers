"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ControllerRemote = void 0;
const axios_1 = __importDefault(require("axios"));
class ControllerRemote {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.baseUrl = app.get('whatsappConnController');
        this.baseUrlMd = app.get('whatsappConnControllerMd');
        this.dockerEnv = app.get('dockerEnv');
        this.remoteAddress = `${app.get('domain')}`;
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    create(data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        var _a, _b, _c, _d;
        if (process.env.TESTING)
            return {};
        const url = `${data.useMultidevice ? this.baseUrlMd : this.baseUrl}/start/${id}`;
        let setting;
        if (data.useMultidevice) {
            setting = await this.app.service('settings')
                .find({ query: { $limit: 1, name: 'baileyVersionMd', type: 'system' } });
        }
        else {
            setting = await this.app.service('settings')
                .find({ query: { $limit: 1, name: 'baileyVersion', type: 'system' } });
        }
        const connVersion = setting[0] ? setting[0].meta.version : [2, 2119, 6];
        data.port = data.useMultidevice && this.dockerEnv === 'true' ? String(parseInt(data.port) - 1000) : data.port;
        const options = {
            params: {
                remoteAddress: this.remoteAddress,
                sessionData: (_a = data.sessionData) !== null && _a !== void 0 ? _a : '',
                waitChats: (_b = !data.disableWaitChats) !== null && _b !== void 0 ? _b : true,
                disableAutoImportUnreadMessages: (_c = data.disableAutoImportUnreadMessages) !== null && _c !== void 0 ? _c : false,
                disableForceUseHere: (_d = data.disableForceUseHere) !== null && _d !== void 0 ? _d : false,
                apiSubname: 'ZapFly',
                apiName: 'Chrome',
                apiVersion: '89',
                port: data.port,
                connVersion: JSON.stringify(connVersion)
            }
        };
        try {
            const response = await axios_1.default.get(url, options);
            const port = data.useMultidevice && this.dockerEnv === 'true' ? String(parseInt(data.port) - 1000) : response.data.port;
            await this.app.service('connection-external').patch(null, { sessionId: id, port }, {});
            return response.data;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        const url = `${params.useMultidevice ? this.baseUrlMd : this.baseUrl}/delete/${id}`;
        try {
            const response = await axios_1.default.get(url);
            await this.app.service('connection-external').patch(null, { sessionId: id, port: null, state: 'disconnected' }, {});
            return response.data;
        }
        catch (error) {
            console.error(error.toJSON());
            throw error;
        }
    }
}
exports.ControllerRemote = ControllerRemote;
