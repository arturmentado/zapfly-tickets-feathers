"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberTickets = void 0;
const errors_1 = require("@feathersjs/errors");
class MemberTickets {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        const connsId = (_a = params.user) === null || _a === void 0 ? void 0 : _a.connsId;
        const query = params.query || {};
        params.query = {
            ...query,
            connectionId: {
                $in: connsId
            }
        };
        delete params.provider;
        return this.app.service('tickets').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        const internalParams = params;
        if (!(internalParams === null || internalParams === void 0 ? void 0 : internalParams.query))
            internalParams.query = {};
        internalParams.connectionId = { $in: user.connsId };
        delete internalParams.provider;
        return this.app.service('tickets').get(id, internalParams);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a;
        const connsId = JSON.parse(JSON.stringify((_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!connsId.includes(data.connectionId))
            throw new errors_1.NotFound();
        const user = params === null || params === void 0 ? void 0 : params.user;
        data.userId = user.userId;
        data.messageTimestamp = new Date().getTime() / 1000;
        const contact = await this.app.service('member-contacts').get(data.contactId, params);
        data.contact = contact;
        const ticketsFind = { paginate: false, query: { contactId: data.contactId, status: { $in: ['waiting', 'open'] }, $limit: 1 } };
        const tickets = await this.app.service('tickets').find(ticketsFind);
        if (tickets.length)
            throw new errors_1.NotAcceptable('already open');
        delete params.provider;
        return this.app.service('tickets').create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        if (!user.permissions.includes('admin') && !user.permissions.includes('supervisor')) {
            if (Object.keys(data).some(e => !['status', 'count'].includes(e))) {
                const settings = await this.app.service('settings').find({ query: { userId: user.userId, name: 'allSetUserId', value: true } });
                if (!settings.length)
                    throw new errors_1.MethodNotAllowed('settings not allow this');
            }
        }
        const internalParams = params;
        if (!(internalParams === null || internalParams === void 0 ? void 0 : internalParams.query))
            internalParams.query = {};
        internalParams.query.connectionId = { $in: user.connsId };
        delete internalParams.provider;
        return this.app.service('tickets').patch(id, data, internalParams);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.MemberTickets = MemberTickets;
