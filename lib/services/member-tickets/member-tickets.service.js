"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_tickets_class_1 = require("./member-tickets.class");
const member_tickets_hooks_1 = __importDefault(require("./member-tickets.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate'),
        whitelist: ['$populate', '$text', '$search', '$regex'],
    };
    // Initialize our service with any options it requires
    app.use('/member-tickets', new member_tickets_class_1.MemberTickets(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-tickets');
    service.hooks(member_tickets_hooks_1.default);
}
exports.default = default_1;
