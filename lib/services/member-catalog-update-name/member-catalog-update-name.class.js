"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberCatalogUpdateName = void 0;
class MemberCatalogUpdateName {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    create(data, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        const catalog = await this.app.service('catalogs').get(id, {});
        await this.app.service('catalogs').patch(null, { name: data.name }, { query: { name: id } });
        await this.app.service('catalog-categories').patch(null, { catalogName: data.name, catalogId: catalog._id }, { query: { catalogName: id } });
        await this.app.service('catalog-products').patch(null, { catalogName: data.name, catalogId: catalog._id }, { query: { catalogName: id } });
        return data;
    }
}
exports.MemberCatalogUpdateName = MemberCatalogUpdateName;
