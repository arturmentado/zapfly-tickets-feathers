"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserQueueAvailable = void 0;
class UserQueueAvailable {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        const user = params.user;
        const userId = user === null || user === void 0 ? void 0 : user.userId;
        let set = await this.app.service('settings').find({ paginate: false, query: { userId, value: true, name: 'useQueue' } });
        if (!set.length)
            throw new Error('Not defined');
        set = set[0];
        if (!((_a = set.meta) === null || _a === void 0 ? void 0 : _a.queueOrder))
            set.meta = { queueOrder: [] };
        const check = !set.meta.queueOrder.some((e) => e.equals(user === null || user === void 0 ? void 0 : user._id));
        if (check) {
            set.meta.queueOrder.push(user === null || user === void 0 ? void 0 : user._id);
            await this.app.service('users').patch(user === null || user === void 0 ? void 0 : user._id, { available: true });
            // .then(console.log);
        }
        else {
            set.meta.queueOrder = set.meta.queueOrder.filter((e) => !e.equals(user === null || user === void 0 ? void 0 : user._id));
            await this.app.service('users').patch(user === null || user === void 0 ? void 0 : user._id, { available: false });
            // .then(console.log);
        }
        return this.app.service('settings').patch(set._id, { meta: set.meta });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.UserQueueAvailable = UserQueueAvailable;
