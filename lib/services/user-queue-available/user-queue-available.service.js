"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_queue_available_class_1 = require("./user-queue-available.class");
const user_queue_available_hooks_1 = __importDefault(require("./user-queue-available.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/user/queue-available', new user_queue_available_class_1.UserQueueAvailable(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('user/queue-available');
    service.hooks(user_queue_available_hooks_1.default);
}
exports.default = default_1;
