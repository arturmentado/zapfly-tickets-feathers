"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberConnections = void 0;
const errors_1 = require("@feathersjs/errors");
class MemberConnections {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        let internalParams = params;
        if (!internalParams)
            internalParams = { query: {} };
        internalParams.query._id = (_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId;
        return this.app.service('connections').find(internalParams);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a;
        if (!params.user)
            return {};
        const conn = await this.app.service('connections').get(id, {});
        const userConns = JSON.parse(JSON.stringify((_a = params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!userConns.includes(String(conn._id)))
            throw new errors_1.NotFound();
        return conn;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params.user;
        data.userId = user._id;
        const connsId = user.connsId;
        await this.app.service('connections').create(data);
        const conn = await this.app.service('connections').get(data.jid, {});
        connsId.push(conn._id);
        await this.app.service('users').patch(null, { connsId }, { query: { userId: user._id } });
        return conn;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        var _a;
        let internalParams = params;
        if (!internalParams)
            internalParams = { query: {} };
        internalParams.query._id = (_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId;
        return this.app.service('connections').patch(id, data);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.MemberConnections = MemberConnections;
