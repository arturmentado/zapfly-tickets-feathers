"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_connections_class_1 = require("./member-connections.class");
const member_connections_hooks_1 = __importDefault(require("./member-connections.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-connections', new member_connections_class_1.MemberConnections(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-connections');
    service.hooks(member_connections_hooks_1.default);
}
exports.default = default_1;
