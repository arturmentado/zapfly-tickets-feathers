"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const upload_class_1 = require("./upload.class");
const upload_hooks_1 = __importDefault(require("./upload.hooks"));
const s3Middleware_1 = require("../../lib/s3Middleware");
function default_1(app) {
    const upload = s3Middleware_1.createUpload(app);
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/upload', upload.array('files'), s3Middleware_1.middlewareUpload, new upload_class_1.Upload(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('upload');
    service.hooks(upload_hooks_1.default);
}
exports.default = default_1;
