"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const departments_class_1 = require("./departments.class");
const departments_model_1 = __importDefault(require("../../models/departments.model"));
const departments_hooks_1 = __importDefault(require("./departments.hooks"));
function default_1(app) {
    const options = {
        Model: departments_model_1.default(app),
        whitelist: ['$text', '$search', '$regex'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/departments', new departments_class_1.Departments(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('departments');
    service.hooks(departments_hooks_1.default);
}
exports.default = default_1;
