"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_messages_class_1 = require("./api-messages.class");
const api_messages_hooks_1 = __importDefault(require("./api-messages.hooks"));
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
// Add this service to the service type index
const storage = multer_1.default.diskStorage({
    destination: (_req, _file, cb) => cb(null, path_1.default.resolve(__dirname + '../../../../public/uploads')),
    filename: (_req, file, cb) => cb(null, `${Date.now()}${file.originalname}`) // getting the file name
});
const upload = multer_1.default({
    storage,
    limits: {
        fieldSize: 1e+8,
        fileSize: 1e+8 //  The max file size in bytes, here it's 10MB
        // READ MORE https://www.npmjs.com/package/multer#limits
    }
});
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/api/messages', upload.array('file'), (req, _res, next) => {
        const { method } = req;
        if (method === 'POST' || method === 'PATCH') {
            req.feathers.files = req.files;
        }
        req.feathers.ip = req.ip;
        req.feathers.headers = req.headers;
        next();
    }, new api_messages_class_1.ApiMessages(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('api/messages');
    service.hooks(api_messages_hooks_1.default);
}
exports.default = default_1;
