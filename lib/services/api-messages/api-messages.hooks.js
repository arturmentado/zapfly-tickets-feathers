"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_auth_1 = __importDefault(require("../../hooks/api-auth"));
const api_validate_1 = __importDefault(require("../../hooks/api-validate"));
const api_check_payment_1 = __importDefault(require("../../hooks/api-check-payment"));
const api_logs_before_1 = __importDefault(require("../../hooks/api-logs-before"));
const api_logs_after_1 = __importDefault(require("../../hooks/api-logs-after"));
const api_logs_error_1 = __importDefault(require("../../hooks/api-logs-error"));
exports.default = {
    before: {
        all: [api_validate_1.default(), api_check_payment_1.default(['connection']), api_logs_before_1.default(), api_auth_1.default()],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [api_logs_after_1.default()],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [api_logs_error_1.default()],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
