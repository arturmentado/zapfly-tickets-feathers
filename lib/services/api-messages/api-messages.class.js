"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiMessages = void 0;
const crypto_1 = __importDefault(require("crypto"));
const axios_1 = __importDefault(require("axios"));
class ApiMessages {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.baseUrl = app.get('whatsappBase');
        this.baseUrlMd = app.get('whatsappBaseMd');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        if (!params.query)
            params.query = {};
        if (!params.connection)
            throw new Error();
        delete params.query.connection;
        delete params.query.token;
        params.query.connectionId = params.connection._id;
        delete params.provider;
        return this.app.service('messages').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        if (!params.query)
            params.query = {};
        if (!params.connection)
            throw new Error();
        delete params.query.connection;
        delete params.query.token;
        params.query.connectionId = params.connection._id;
        delete params.provider;
        return this.app.service('messages').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a;
        if (!params.query)
            params.query = {};
        if (!params.connection)
            throw new Error();
        delete params.query.connection;
        delete params.query.token;
        params.query.connectionId = params.connection._id;
        delete params.provider;
        const connection = params.connection;
        const to = data.to;
        const contacts = await this.app.service('contacts')
            .find({
            paginate: false,
            query: {
                $limit: 1,
                connectionId: connection._id,
                jid: to
            }
        });
        let contact = contacts[0];
        if (!contacts.length) {
            const url = `${connection.useMultidevice ? this.baseUrlMd : this.baseUrl}${connection.port}/on-whatsapp/${data.to}`;
            try {
                const { data } = await axios_1.default.get(url);
                if (!data.exists) {
                    throw new Error('invalid to');
                }
                contact = await this.app.service('contacts').create({
                    connectionId: connection._id,
                    jid: to
                });
            }
            catch (error) {
                if (error.message === 'invalid to')
                    throw error;
                throw new Error('cant validate to');
            }
            if (!contact)
                throw new Error('no contact with this "to"');
        }
        const tickets = await this.app.service('tickets')
            .find({
            paginate: false,
            query: {
                $limit: 1,
                connectionId: connection._id,
                contactId: contact._id,
                userId: connection.userId,
                status: {
                    $in: ['waiting', 'in_progress']
                }
            }
        });
        let ticket = tickets[0];
        if (!tickets.length) {
            ticket = await this.app.service('tickets').create({
                connectionId: connection._id,
                userId: connection.userId,
                contactId: contact._id,
                messageTimestamp: new Date().getTime() / 1000,
                jid: to
            });
        }
        const message = {
            conversation: data.body
        };
        const isMedia = ((_a = params === null || params === void 0 ? void 0 : params.files) === null || _a === void 0 ? void 0 : _a.length) > 0;
        if (isMedia) {
            const file = params.files[0];
            let type = file.mimetype.split('/')[0];
            if (type === 'application')
                type = 'document';
            delete message.conversation;
            message[`${type}Message`] = {
                caption: data.body,
                mimetype: file.mimetype
            };
        }
        const payload = {
            connectionId: connection._id,
            userId: connection.userId,
            ticketId: ticket._id,
            contactId: contact._id,
            apiMessage: true,
            messageTimestamp: Math.floor(new Date().getTime() - 2),
            key: {
                fromMe: true,
                id: `3EB0${crypto_1.default.randomBytes(4).toString('hex').toUpperCase()}`,
                remoteJid: to
            },
            message
        };
        await this.app.service('messages').create(payload);
        const type = Object.keys(payload.message)[0]; // building for whatsapp
        const messageSend = {
            id: payload.key.remoteJid,
            messageId: payload.key.id,
            connectionId: payload.connectionId.toString(),
            message: data.body,
            caption: data.body,
            type
        };
        await this.app.service('messages-remote').create(messageSend, params);
        return { success: true };
    }
}
exports.ApiMessages = ApiMessages;
