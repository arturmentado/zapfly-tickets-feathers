"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_settings_class_1 = require("./admin-settings.class");
const admin_settings_hooks_1 = __importDefault(require("./admin-settings.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/admin-settings', new admin_settings_class_1.AdminSettings(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin-settings');
    service.hooks(admin_settings_hooks_1.default);
}
exports.default = default_1;
