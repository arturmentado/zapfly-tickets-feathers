"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("../../lib/ajv"));
const tickets_model_1 = require("../../models/tickets.model");
const feathers_hooks_common_1 = require("feathers-hooks-common");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search = require('feathers-mongodb-fuzzy-search');
// Don't remove this comment. It's needed to format import lines nicely.
exports.default = {
    before: {
        all: [
            feathers_hooks_common_1.disallow('external'),
        ],
        find: [
            search({ escape: false }),
            search({
                fields: [
                    'jid',
                    'contact.name',
                    'contact.notify',
                    'contact.overrideName',
                    'contact.vname',
                ]
            }),
        ],
        get: [],
        create: [
            ajv_1.default(tickets_model_1.createValidationSchema),
            async (ctx) => {
                let settings = await ctx.app.service('settings').find({ paginate: false, query: { name: 'useQueue', userId: ctx.data.userId, value: true } });
                if (!settings.length)
                    return ctx;
                settings = settings[0];
                if (!settings.meta.queueOrder)
                    return ctx;
                if (!settings.meta.queueOrder[0])
                    return ctx;
                const attendantId = settings.meta.queueOrder[0];
                ctx.data.status = 'in_progress';
                ctx.data.attendantId = attendantId;
                const queueOrder = settings.meta.queueOrder;
                queueOrder.push(queueOrder.shift());
                await ctx.app.service('settings').patch(settings._id, { 'meta.queueOrder': queueOrder });
                return ctx;
            }
        ],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
