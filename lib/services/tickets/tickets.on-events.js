"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { Application } from '../../declarations';
const crypto_1 = __importDefault(require("crypto"));
async function onStatusClosing(data, app) {
    try {
        let chatbotClosing = await app.service('chatbots').find({
            paginate: false, query: {
                $limit: 1,
                userId: data.userId, type: 'closing', active: true
            }
        });
        if (!chatbotClosing.length)
            return; // no closing options active
        chatbotClosing = chatbotClosing[0];
        const payload = {
            connectionId: data.connectionId,
            contactId: data.contactId,
            ticketId: data._id,
            userId: data.userId,
            chatbotId: chatbotClosing._id,
            chatbotForId: data._id,
            chatbotSuccess: true,
            chatbotClosing: true,
            chatbotEnd: true,
            key: {
                fromMe: true,
                id: `3EB0${crypto_1.default.randomBytes(4).toString('hex').toUpperCase()}`,
                remoteJid: data.jid
            },
            message: {
                conversation: chatbotClosing.message
            },
            messageTimestamp: Math.floor(new Date().getTime() - 2)
        };
        await app.service('messages').create(payload);
        const type = Object.keys(payload.message)[0]; // building for whatsapp
        const messageSend = {
            id: data.jid,
            messageId: payload.key.id,
            connectionId: payload.connectionId,
            message: chatbotClosing.message,
            type
        };
        await app.service('messages-remote').create(messageSend);
    }
    catch (error) {
        console.error('ticket closing logic error', error);
    }
}
function handlePatched(data, app, event) {
    // console.log(event.data);
    if (data.status === 'closing' && event.data.status && event.data.status === 'closing')
        return onStatusClosing(data, app);
}
function default_1(app) {
    app.service('tickets').on('patched', (data, event) => handlePatched(data, app, event));
}
exports.default = default_1;
