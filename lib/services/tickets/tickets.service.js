"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tickets_class_1 = require("./tickets.class");
const tickets_model_1 = __importDefault(require("../../models/tickets.model"));
const tickets_hooks_1 = __importDefault(require("./tickets.hooks"));
const tickets_on_events_1 = __importDefault(require("./tickets.on-events"));
function default_1(app) {
    const options = {
        Model: tickets_model_1.default(app),
        multi: ['patch'],
        whitelist: ['$text', '$search', '$regex'],
        paginate: app.get('paginate'),
    };
    // Initialize our service with any options it requires
    app.use('/tickets', new tickets_class_1.Tickets(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('tickets');
    tickets_on_events_1.default(app);
    service.hooks(tickets_hooks_1.default);
}
exports.default = default_1;
