"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chats_remote_class_1 = require("./chats-remote.class");
const chats_remote_hooks_1 = __importDefault(require("./chats-remote.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/chats-remote', new chats_remote_class_1.ChatsRemote(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('chats-remote');
    service.hooks(chats_remote_hooks_1.default);
}
exports.default = default_1;
