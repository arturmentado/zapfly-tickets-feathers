"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateContact = void 0;
const errors_1 = require("@feathersjs/errors");
const axios_1 = __importDefault(require("axios"));
class UpdateContact {
    constructor(options = {}, app) {
        this.options = options;
        this.baseUrl = app.get('whatsappBase');
        this.baseUrlMd = app.get('whatsappBaseMd');
        this.app = app;
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    create(data, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a;
        if (!((_a = params === null || params === void 0 ? void 0 : params.query) === null || _a === void 0 ? void 0 : _a.id))
            throw new Error('id missing');
        const target = await this.app.service('member-connections').get(params.query.id, params);
        if (!target)
            throw new errors_1.NotFound();
        const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/contacts/${id}`;
        try {
            const { data } = await axios_1.default.get(url);
            this.app.service('contacts').patch(null, data, { query: { jid: id, connectionId: target._id } });
            return data;
        }
        catch (error) {
            // console.error(error);
            throw error;
        }
    }
}
exports.UpdateContact = UpdateContact;
