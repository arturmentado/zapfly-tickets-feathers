"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const update_contact_class_1 = require("./update-contact.class");
const update_contact_hooks_1 = __importDefault(require("./update-contact.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/update-contact', new update_contact_class_1.UpdateContact(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('update-contact');
    service.hooks(update_contact_hooks_1.default);
}
exports.default = default_1;
