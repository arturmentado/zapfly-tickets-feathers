"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StripeSession = void 0;
const stripe_1 = __importDefault(require("stripe"));
class StripeSession {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.stripe = new stripe_1.default(app.get('stripeToken'), { apiVersion: '2020-08-27', });
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    async remove(id, params) {
        const connection = await this.app.service('member-connections').get(id, params);
        this.stripe.subscriptions.del(connection.preapprovalPlan.stripeSubscriptionId);
        return this.app.service('member-connections').patch(id, { 'preapprovalPlan.status': 'REVOKED' }, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const success_url = `${this.app.get('frontendDomain')}/dashboard?stripe-success=true&session_id={CHECKOUT_SESSION_ID}`;
        const cancel_url = `${this.app.get('frontendDomain')}/dashboard?stripe-fail=true&`;
        const { priceId, connectionId } = data;
        await this.app.service('member-connections').get(connectionId, params);
        // See https://stripe.com/docs/api/checkout/sessions/create
        // for additional parameters to pass.
        try {
            const session = await this.stripe.checkout.sessions.create({
                mode: 'subscription',
                payment_method_types: ['card'],
                line_items: [
                    {
                        price: priceId,
                        quantity: 1,
                    },
                ],
                metadata: {
                    connectionId
                },
                // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
                // the actual Session ID is returned in the query parameter when your customer
                // is redirected to the success page.
                success_url,
                cancel_url,
            });
            await this.app.service('member-connections').patch(connectionId, {
                'preapprovalPlan.type': 'stripe',
                'preapprovalPlan.status': 'requested',
                'preapprovalPlan.stripeSessionId': session.id
            }, params);
            return session.id;
        }
        catch (e) {
            console.error(e);
            throw 'error in stripe request';
        }
    }
}
exports.StripeSession = StripeSession;
