"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const stripe_session_class_1 = require("./stripe-session.class");
const stripe_session_hooks_1 = __importDefault(require("./stripe-session.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/stripe-session', new stripe_session_class_1.StripeSession(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('stripe-session');
    service.hooks(stripe_session_hooks_1.default);
}
exports.default = default_1;
