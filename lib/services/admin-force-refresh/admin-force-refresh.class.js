"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminForceRefresh = void 0;
class AdminForceRefresh {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.events = ['force-refresh'];
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.emit('force-refresh', { userId: id });
        return { success: true };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.AdminForceRefresh = AdminForceRefresh;
