"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_force_refresh_class_1 = require("./admin-force-refresh.class");
const admin_force_refresh_hooks_1 = __importDefault(require("./admin-force-refresh.hooks"));
function default_1(app) {
    const options = {
        events: ['force-refresh'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/admin-force-refresh', new admin_force_refresh_class_1.AdminForceRefresh(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin-force-refresh');
    service.hooks(admin_force_refresh_hooks_1.default);
}
exports.default = default_1;
