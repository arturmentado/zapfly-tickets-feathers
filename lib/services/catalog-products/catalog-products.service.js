"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catalog_products_class_1 = require("./catalog-products.class");
const catalog_products_model_1 = __importDefault(require("../../models/catalog-products.model"));
const catalog_products_hooks_1 = __importDefault(require("./catalog-products.hooks"));
function default_1(app) {
    const options = {
        multi: ['patch'],
        Model: catalog_products_model_1.default(app),
        paginate: {
            'default': 200,
            'max': 200
        },
    };
    // Initialize our service with any options it requires
    app.use('/catalog-products', new catalog_products_class_1.CatalogProducts(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('catalog-products');
    service.hooks(catalog_products_hooks_1.default);
}
exports.default = default_1;
