"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tooltips_class_1 = require("./tooltips.class");
const tooltips_model_1 = __importDefault(require("../../models/tooltips.model"));
const tooltips_hooks_1 = __importDefault(require("./tooltips.hooks"));
function default_1(app) {
    const options = {
        Model: tooltips_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/tooltips', new tooltips_class_1.Tooltips(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('tooltips');
    service.hooks(tooltips_hooks_1.default);
}
exports.default = default_1;
