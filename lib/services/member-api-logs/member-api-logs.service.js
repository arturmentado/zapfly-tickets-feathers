"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_api_logs_class_1 = require("./member-api-logs.class");
const member_api_logs_hooks_1 = __importDefault(require("./member-api-logs.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-api-logs', new member_api_logs_class_1.MemberApiLogs(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-api-logs');
    service.hooks(member_api_logs_hooks_1.default);
}
exports.default = default_1;
