"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberApiLogs = void 0;
class MemberApiLogs {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    create(data, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        const connsId = JSON.parse(JSON.stringify((_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!params.query)
            params.query = {};
        params.query.$limit = 5;
        params.query.connectionId = { $in: connsId };
        delete params.provider;
        return this.app.service('api-logs').find(params);
    }
}
exports.MemberApiLogs = MemberApiLogs;
