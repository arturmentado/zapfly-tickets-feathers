"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pagseguro_preapprovals_class_1 = require("./pagseguro-preapprovals.class");
const pagseguro_preapprovals_hooks_1 = __importDefault(require("./pagseguro-preapprovals.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/pagseguro-preapprovals', new pagseguro_preapprovals_class_1.PagseguroPreapprovals(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('pagseguro-preapprovals');
    service.hooks(pagseguro_preapprovals_hooks_1.default);
}
exports.default = default_1;
