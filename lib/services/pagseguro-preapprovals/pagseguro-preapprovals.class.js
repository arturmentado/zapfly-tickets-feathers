"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagseguroPreapprovals = void 0;
const errors_1 = require("@feathersjs/errors");
const axios_1 = __importDefault(require("axios"));
class PagseguroPreapprovals {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.email = this.app.get('pagseguroEmail');
        this.token = this.app.get('pagseguroToken');
        this.baseUrl = `https://ws${this.app.get('pagseguroEnv')}pagseguro.uol.com.br`;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const { connection, payload } = data;
        const check = await this.app.service('member-connections').get(connection._id, params);
        if (!check)
            throw new errors_1.NotFound();
        const planSetting = await this.app.service('settings').find({ paginate: false, query: { type: 'system', value: true, name: 'pagseguroPlan' } });
        payload.plan = planSetting[0].meta.code;
        payload.reference = connection._id;
        const url = `${this.baseUrl}/pre-approvals?email=${this.email}&token=${this.token}`;
        try {
            const res = await axios_1.default.post(url, payload, { headers: { Accept: 'application/vnd.pagseguro.com.br.v1+json;charset=ISO-8859-1' } });
            await this.app.service('connections').patch(connection._id, { 'preapprovalPlan': { 'code': res.data.code, status: 'created' } });
            return { success: true };
        }
        catch (error) {
            if (error.isAxiosError) {
                console.error(error.response.data);
            }
            else
                console.error(error);
            throw new Error('problem');
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        const { connection, payload } = data;
        const check = await this.app.service('member-connections').get(connection._id, params);
        if (!check)
            throw new errors_1.NotFound();
        const url = `${this.baseUrl}/pre-approvals/${check.preapprovalPlan.code}/payment-method?email=${this.email}&token=${this.token}`;
        try {
            const res = await axios_1.default.put(url, payload, { headers: { Accept: 'application/vnd.pagseguro.com.br.v1+json;charset=ISO-8859-1' } });
            await this.app.service('connections').patch(connection._id, { 'preapprovalPlan': { status: 'created' } });
            return { success: true };
        }
        catch (error) {
            if (error.isAxiosError) {
                console.error(error.response.data);
            }
            else
                console.error(error);
            throw new Error('problem');
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        const check = await this.app.service('member-connections').get(id, params);
        if (!check)
            throw new errors_1.NotFound();
        if (!check.preapprovalPlan)
            throw new errors_1.NotFound();
        if (check.preapprovalPlan.status === 'CANCELLED')
            throw new errors_1.NotFound();
        const url = `${this.baseUrl}/pre-approvals/${check.preapprovalPlan.code}/cancel?email=${this.email}&token=${this.token}`;
        try {
            await axios_1.default.put(url, {}, { headers: { Accept: 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1' } });
            await this.app.service('connections').patch(check._id, { 'preapprovalPlan.status': 'CANCELLED' });
            return { success: true };
        }
        catch (error) {
            if (error.isAxiosError) {
                console.error(error.response.data);
            }
            else
                console.error(error);
            throw new Error('problem');
        }
    }
}
exports.PagseguroPreapprovals = PagseguroPreapprovals;
