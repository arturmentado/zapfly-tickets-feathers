"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_logs_external_class_1 = require("./connection-logs-external.class");
const connection_logs_external_hooks_1 = __importDefault(require("./connection-logs-external.hooks"));
function default_1(app) {
    const options = {
        events: ['connection-logs'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connection-logs-external', new connection_logs_external_class_1.ConnectionLogsExternal(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connection-logs-external');
    service.hooks(connection_logs_external_hooks_1.default);
}
exports.default = default_1;
