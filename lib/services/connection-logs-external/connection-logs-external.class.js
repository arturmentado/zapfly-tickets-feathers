"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionLogsExternal = void 0;
class ConnectionLogsExternal {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        return [];
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        return {
            id, text: `A new message with ID: ${id}!`
        };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        if (Array.isArray(data)) {
            return Promise.all(data.map(current => this.create(current, params)));
        }
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        var _a;
        const sessionId = (_a = params.headers) === null || _a === void 0 ? void 0 : _a.session_id;
        const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });
        const payload = {
            connectionId: conn._id,
            jid: sessionId,
            text: data.text
        };
        // delete params.provider;
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        this.emit('connection-logs', payload);
        return payload;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    // async patch(id: NullableId, data: any, params: Params): Promise<any> {
    //   const sessionId = params.headers?.session_id;
    //   const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });
    //   const payload = {
    //     connectionId: conn._id,
    //     jid: sessionId,
    //     text: data.text
    //   };
    //   delete params.provider;
    //   this.app.service('connection-logs').create(payload, params);
    //   this.app.service('connection-logs').remove(null, { query: { createdAt: { $lt: new Date(new Date().setHours(0, 0, 0, 0)) } } });
    //   return null;
    // }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.ConnectionLogsExternal = ConnectionLogsExternal;
