"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Catalogs = void 0;
const feathers_mongoose_1 = require("feathers-mongoose");
class Catalogs extends feathers_mongoose_1.Service {
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(options, app) {
        super(options);
        this.app = app;
    }
    async get(id, params) {
        const catalog = await this._get(id, params);
        const checkEnabled = await this.app.service('settings').find({ query: { $limit: 1, name: 'catalogActive', userId: catalog.userId, value: true } });
        if (!checkEnabled.length)
            throw Error('not found');
        return catalog;
    }
}
exports.Catalogs = Catalogs;
