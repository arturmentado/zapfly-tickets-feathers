"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catalogs_class_1 = require("./catalogs.class");
const catalogs_model_1 = __importDefault(require("../../models/catalogs.model"));
const catalogs_hooks_1 = __importDefault(require("./catalogs.hooks"));
function default_1(app) {
    const options = {
        id: 'name',
        multi: ['patch'],
        Model: catalogs_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/catalogs', new catalogs_class_1.Catalogs(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('catalogs');
    service.hooks(catalogs_hooks_1.default);
}
exports.default = default_1;
