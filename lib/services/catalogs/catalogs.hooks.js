"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const feathers_hooks_common_1 = require("feathers-hooks-common");
// Don't remove this comment. It's needed to format import lines nicely.
exports.default = {
    before: {
        all: [],
        find: [],
        get: [],
        create: [feathers_hooks_common_1.disallow('external')],
        update: [feathers_hooks_common_1.disallow('external')],
        patch: [feathers_hooks_common_1.disallow('external')],
        remove: [feathers_hooks_common_1.disallow('external')]
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
