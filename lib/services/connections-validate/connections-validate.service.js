"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connections_validate_class_1 = require("./connections-validate.class");
const connections_validate_hooks_1 = __importDefault(require("./connections-validate.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connections-validate', new connections_validate_class_1.ConnectionsValidate(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connections-validate');
    service.hooks(connections_validate_hooks_1.default);
}
exports.default = default_1;
