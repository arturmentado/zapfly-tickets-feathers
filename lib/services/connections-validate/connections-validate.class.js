"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionsValidate = void 0;
const axios_1 = __importDefault(require("axios"));
class ConnectionsValidate {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.baseUrl = app.get('whatsappBase');
        this.baseUrlMd = app.get('whatsappBaseMd');
    }
    create(data, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        return [];
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a, _b, _c, _d, _e;
        if (process.env.TESTING)
            return {};
        let connection = await this.app.service('connections').get(id, { query: { notId: true } });
        const url = `${connection.useMultidevice ? this.baseUrlMd : this.baseUrl}${connection.port}/me`;
        const res = await axios_1.default.get(url)
            .catch((error) => {
            console.error(error);
            throw error;
        });
        const jid = res.data.user.jid.split('@')[0];
        const connections = await this.app.service('connections').find({ query: { $limit: 1, jid, _id: { $ne: connection._id } } });
        if (connections.total) {
            const random = Math.floor(Math.random() * 90000) + 10000;
            const target = connections.data[0];
            if ((_a = target.meta) === null || _a === void 0 ? void 0 : _a.validatedJid) {
                await this.app.service('connections').patch(connection._id, { status: 'REMOVED', qrcode: null, sessionData: null });
                await this.app.service('controller-remote').remove(connection.jid, { useMultidevice: target.useMultidevice });
                const tooltipPayload = {
                    active: true,
                    type: 'emergency',
                    forWho: 'specific',
                    notificationTitle: 'Atenção!',
                    notificationText: 'O celular que escaneou o QR code já está vinculado a outra conta e não pode ser conectado a  esta. Caso isto pareça um erro, entre em contato com o suporte',
                    connectionsId: [connection._id],
                };
                await this.app.service('tooltips').create(tooltipPayload);
                throw { error: 'invalid phone jid' };
            }
            await this.app.service('connections').patch(target._id, { jid: `${target.jid}-${random}`, status: 'REMOVED', qrcode: null, sessionData: null });
            await this.app.service('controller-remote').remove(target.jid, { useMultidevice: target.useMultidevice }).catch(() => { return null; });
        }
        if (!((_b = connection.meta) === null || _b === void 0 ? void 0 : _b.validatedJid)) {
            await this.app.service('controller-remote').remove(connection.jid, { useMultidevice: connection.useMultidevice }).catch(() => { return null; });
            connection = await this.app.service('connections').patch(connection._id, { 'meta.validatedJid': true, jid });
            await this.app.service('controller-remote')
                .update(connection.jid, {
                waitChats: (_c = !connection.disableWaitChats) !== null && _c !== void 0 ? _c : true,
                sessionData: connection.useMultidevice ? true : connection.sessionData,
                disableAutoImportUnreadMessages: (_d = connection.disableAutoImportUnreadMessages) !== null && _d !== void 0 ? _d : true,
                disableForceUseHere: (_e = connection.disableForceUseHere) !== null && _e !== void 0 ? _e : true
            });
        }
        return {};
    }
}
exports.ConnectionsValidate = ConnectionsValidate;
