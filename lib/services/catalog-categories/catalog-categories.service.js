"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const catalog_categories_class_1 = require("./catalog-categories.class");
const catalog_categories_model_1 = __importDefault(require("../../models/catalog-categories.model"));
const catalog_categories_hooks_1 = __importDefault(require("./catalog-categories.hooks"));
function default_1(app) {
    const options = {
        multi: ['patch'],
        Model: catalog_categories_model_1.default(app),
        paginate: {
            'default': 200,
            'max': 200
        },
    };
    // Initialize our service with any options it requires
    app.use('/catalog-categories', new catalog_categories_class_1.CatalogCategories(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('catalog-categories');
    service.hooks(catalog_categories_hooks_1.default);
}
exports.default = default_1;
