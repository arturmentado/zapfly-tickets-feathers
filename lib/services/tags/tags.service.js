"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tags_class_1 = require("./tags.class");
const tags_model_1 = __importDefault(require("../../models/tags.model"));
const tags_hooks_1 = __importDefault(require("./tags.hooks"));
function default_1(app) {
    const options = {
        Model: tags_model_1.default(app),
        whitelist: ['$text', '$search', '$regex'],
        paginate: {
            'default': 300,
            'max': 500
        }
    };
    // Initialize our service with any options it requires
    app.use('/tags', new tags_class_1.Tags(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('tags');
    service.hooks(tags_hooks_1.default);
}
exports.default = default_1;
