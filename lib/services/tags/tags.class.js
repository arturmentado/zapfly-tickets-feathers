"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tags = void 0;
const feathers_mongoose_1 = require("feathers-mongoose");
class Tags extends feathers_mongoose_1.Service {
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(options, app) {
        super(options);
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        const contacts = await this.app.service('contacts').find({ query: { tagsId: id, $select: ['_id', 'tagsId', 'tags'] }, paginate: false, });
        contacts.forEach((contact) => {
            this.app.service('contacts').patch(contact._id, {
                tagsId: contact.tagsId.filter((e) => e !== id),
                tags: contact.tags.filter((e) => e._id !== id)
            });
        });
        return this._remove(id, params);
    }
}
exports.Tags = Tags;
