"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
async function default_1(item, ctx) {
    ctx.app.service('tickets')
        .patch(null, { contact: item }, { query: { contactId: item._id } });
}
exports.default = default_1;
