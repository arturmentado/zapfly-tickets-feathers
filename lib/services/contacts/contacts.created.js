"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
async function default_1(item, ctx) {
    if (process.env.TESTING)
        return;
    const conn = await ctx.app.service('connections').get(item.connectionId);
    if (conn.useMultidevice)
        return;
    try {
        const contact = await ctx.app.service('contacts-remote').update(item.jid, {}, { query: { id: item.connectionId } });
        const patchObj = {};
        if (contact.notify)
            patchObj.notify = contact.notify;
        if (contact.name)
            patchObj.name = contact.name;
        if (contact.short)
            patchObj.short = contact.short;
        if (contact.vname)
            patchObj.vname = contact.vname;
        if (contact.verify)
            patchObj.verify = contact.verify;
        if (contact.enterprise)
            patchObj.enterprise = contact.enterprise;
        if (Object.keys(patchObj).length === 0)
            return;
        await ctx.app.service('contacts').patch(item._id, patchObj);
    }
    catch (error) {
        console.log('contacts created error', error);
    }
}
exports.default = default_1;
