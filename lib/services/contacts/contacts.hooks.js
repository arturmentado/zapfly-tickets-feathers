"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const feathers_hooks_common_1 = require("feathers-hooks-common");
// Don't remove this comment. It's needed to format import lines nicely.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search = require('feathers-mongodb-fuzzy-search');
exports.default = {
    before: {
        all: [feathers_hooks_common_1.disallow('external')],
        find: [
            search({ escape: true }),
            search({
                fields: [
                    'jid',
                    'overrideName',
                    'notify',
                    'name',
                    'tags.label',
                    'short',
                    'vname',
                ]
            }),
        ],
        get: [],
        // create: [validate(createValidationSchema)],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
