"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const contacts_remote_class_1 = require("./contacts-remote.class");
const contacts_remote_hooks_1 = __importDefault(require("./contacts-remote.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/contacts-remote', new contacts_remote_class_1.ContactsRemote(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('contacts-remote');
    service.hooks(contacts_remote_hooks_1.default);
}
exports.default = default_1;
