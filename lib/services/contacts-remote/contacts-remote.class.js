"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactsRemote = void 0;
const errors_1 = require("@feathersjs/errors");
const axios_1 = __importDefault(require("axios"));
class ContactsRemote {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.baseUrl = app.get('whatsappBase');
        this.baseUrlMd = app.get('whatsappBaseMd');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        if (!((_a = params === null || params === void 0 ? void 0 : params.query) === null || _a === void 0 ? void 0 : _a.id))
            throw new Error('id missing');
        const target = await this.app.service('member-connections').get(params.query.id, params);
        if (!target)
            throw new errors_1.NotFound();
        if (target.useMultidevice)
            throw new Error('Using multi devices');
        const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/contacts`;
        try {
            const { data } = await axios_1.default.get(url);
            // data = Object.values(data);
            const promises = [];
            for (const key in data) {
                if (Object.prototype.hasOwnProperty.call(data, key)) {
                    const item = data[key];
                    item.connectionId = target._id;
                    const params = {
                        query: {
                            jid: item.jid
                        },
                        mongoose: { upsert: true }
                    };
                    promises.push(this.app.service('contacts').patch(null, item, params));
                }
            }
            await Promise.all(promises);
            return data;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a, _b;
        if (process.env.TESTING)
            return { exists: true, jid: `${id}@s.whatsapp.net` };
        if (!((_a = params === null || params === void 0 ? void 0 : params.query) === null || _a === void 0 ? void 0 : _a.id))
            throw new Error('id missing');
        const target = await this.app.service('member-connections').get(params.query.id, params);
        if (!target)
            throw new errors_1.NotFound();
        let url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/on-whatsapp/${id}`;
        if ((_b = params === null || params === void 0 ? void 0 : params.query) === null || _b === void 0 ? void 0 : _b.contact)
            url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/contacts/${id}`;
        try {
            const { data } = await axios_1.default.get(url);
            return data;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        if (Array.isArray(data)) {
            return Promise.all(data.map(current => this.create(current, params)));
        }
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        var _a;
        if (!((_a = params === null || params === void 0 ? void 0 : params.query) === null || _a === void 0 ? void 0 : _a.id))
            throw new Error('id missing');
        const target = await this.app.service('member-connections').get(params.query.id, params);
        if (!target)
            throw new errors_1.NotFound();
        if (target.useMultidevice)
            throw new Error('Using multi devices');
        const url = `${this.baseUrl}${target.port}/contacts/${id}`;
        try {
            const { data } = await axios_1.default.get(url);
            return data;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.ContactsRemote = ContactsRemote;
