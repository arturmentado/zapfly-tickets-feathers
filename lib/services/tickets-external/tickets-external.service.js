"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tickets_external_class_1 = require("./tickets-external.class");
const tickets_external_hooks_1 = __importDefault(require("./tickets-external.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/tickets-external', new tickets_external_class_1.TicketsExternal(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('tickets-external');
    service.hooks(tickets_external_hooks_1.default);
}
exports.default = default_1;
