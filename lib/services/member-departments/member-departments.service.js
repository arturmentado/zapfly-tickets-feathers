"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_departments_class_1 = require("./member-departments.class");
const member_departments_hooks_1 = __importDefault(require("./member-departments.hooks"));
function default_1(app) {
    const options = {
        whitelist: ['$text', '$search', '$regex'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-departments', new member_departments_class_1.MemberDepartments(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-departments');
    service.hooks(member_departments_hooks_1.default);
}
exports.default = default_1;
