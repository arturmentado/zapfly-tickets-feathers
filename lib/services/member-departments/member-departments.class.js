"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberDepartments = void 0;
class MemberDepartments {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('departments').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('departments').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        data.userId = user.userId;
        return this.app.service('departments').create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        data.userId = user.userId;
        return this.app.service('departments').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        await this.app.service('users').patch(null, { departmentId: null }, { query: { departmentId: id } });
        return this.app.service('departments').remove(id);
    }
}
exports.MemberDepartments = MemberDepartments;
