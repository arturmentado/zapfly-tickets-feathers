"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const version_class_1 = require("./version.class");
const version_hooks_1 = __importDefault(require("./version.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/version', new version_class_1.Version(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('version');
    service.hooks(version_hooks_1.default);
}
exports.default = default_1;
