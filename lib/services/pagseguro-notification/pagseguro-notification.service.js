"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pagseguro_notification_class_1 = require("./pagseguro-notification.class");
const pagseguro_notification_hooks_1 = __importDefault(require("./pagseguro-notification.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/pagseguro-notification', new pagseguro_notification_class_1.PagseguroNotification(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('pagseguro-notification');
    service.hooks(pagseguro_notification_hooks_1.default);
}
exports.default = default_1;
