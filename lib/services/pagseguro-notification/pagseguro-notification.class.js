"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagseguroNotification = void 0;
const axios_1 = __importDefault(require("axios"));
class PagseguroNotification {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.email = this.app.get('pagseguroEmail');
        this.token = this.app.get('pagseguroToken');
        this.baseUrl = `https://ws${this.app.get('pagseguroEnv')}pagseguro.uol.com.br/pre-approvals/notifications/`;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        return axios_1.default.get(`${this.baseUrl}${id}?token=${this.token}&email=${this.email}`, { headers: { Accept: 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1' } });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.PagseguroNotification = PagseguroNotification;
