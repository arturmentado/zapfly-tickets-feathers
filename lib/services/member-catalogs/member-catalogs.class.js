"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberCatalogs = void 0;
class MemberCatalogs {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    async find(params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalogs').find(params);
    }
    async get(id, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalogs').get(id, params);
    }
    async update(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        data.userId = user.userId;
        return this.app.service('catalogs').update(id, data, params);
    }
    async patch(id, data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('catalogs').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        data.userId = user.userId;
        return this.app.service('catalogs').create(data, params);
    }
}
exports.MemberCatalogs = MemberCatalogs;
