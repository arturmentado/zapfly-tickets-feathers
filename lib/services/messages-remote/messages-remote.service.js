"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const messages_remote_class_1 = require("./messages-remote.class");
const messages_remote_hooks_1 = __importDefault(require("./messages-remote.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/messages-remote', new messages_remote_class_1.MessagesRemote(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('messages-remote');
    service.hooks(messages_remote_hooks_1.default);
}
exports.default = default_1;
