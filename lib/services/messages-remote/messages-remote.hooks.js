"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const feathers_hooks_common_1 = require("feathers-hooks-common");
const ajv_1 = __importDefault(require("../../lib/ajv"));
const createValidationSchema = {
    type: 'object',
    required: ['connectionId', 'id', 'type'],
    properties: {
        connectionId: {
            type: ['string', 'object']
        },
        id: {
            type: 'string'
        },
        caption: {
            type: 'string',
        },
        message: {
            type: ['string', 'object'],
        },
        url: {
            type: ['string', 'boolean'],
        },
        type: {
            type: 'string',
        },
    }
};
exports.default = {
    before: {
        all: [feathers_hooks_common_1.disallow('external')],
        find: [],
        get: [],
        create: [ajv_1.default(createValidationSchema)],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
