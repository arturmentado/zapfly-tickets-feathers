"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessagesRemote = void 0;
const errors_1 = require("@feathersjs/errors");
const axios_1 = __importDefault(require("axios"));
const form_data_1 = __importDefault(require("form-data"));
const fs_1 = __importDefault(require("fs"));
class MessagesRemote {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.baseUrl = app.get('whatsappBase');
        this.baseUrlMd = app.get('whatsappBaseMd');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a, _b;
        const target = !((_a = params.connection) === null || _a === void 0 ? void 0 : _a._id) ? await this.app.service('connections').get((_b = params.query) === null || _b === void 0 ? void 0 : _b.connectionId) : params.connection;
        if (!target)
            throw new errors_1.NotFound();
        if (target.useMultidevice) {
            console.trace('using multidevices');
            throw new Error('Using multidevices');
        }
        const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/messages-query`;
        const options = {
            params: params.query
        };
        try {
            const res = await axios_1.default.get(url, options);
            return res.data;
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a, _b;
        if (process.env.TESTING)
            return {};
        if (data.jid === 'status@broadcast')
            return {};
        const target = await this.app.service('connections').get(data.connectionId);
        if (!target)
            throw new errors_1.NotFound();
        let endpoint = 'messages-send';
        const isMedia = ((_a = params === null || params === void 0 ? void 0 : params.files) === null || _a === void 0 ? void 0 : _a.length) > 0 || !!data.url;
        const hasFile = ((_b = params === null || params === void 0 ? void 0 : params.files) === null || _b === void 0 ? void 0 : _b.length) > 0 && ((params === null || params === void 0 ? void 0 : params.files) && (params === null || params === void 0 ? void 0 : params.files[0]) && (params === null || params === void 0 ? void 0 : params.files.path));
        if (isMedia) {
            let file;
            endpoint = 'messages-send-media';
            const form = new form_data_1.default();
            if (hasFile) {
                file = params.files[0];
                const fileStream = fs_1.default.createReadStream(file.path);
                form.append('file', fileStream);
            }
            for (const key in data) {
                const item = data[key];
                if (!item || typeof item === 'object')
                    continue;
                form.append(key, item);
            }
            const options = {
                headers: form.getHeaders()
            };
            const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/${endpoint}`;
            try {
                const res = await axios_1.default.post(url, form, options);
                if (hasFile) {
                    fs_1.default.unlinkSync(file.path);
                }
                return res.data;
            }
            catch (error) {
                console.error(error.toJSON(), data);
                throw error;
            }
        }
        const url = `${target.useMultidevice ? this.baseUrlMd : this.baseUrl}${target.port}/${endpoint}`;
        try {
            const res = await axios_1.default.post(url, data);
            return res.data;
        }
        catch (error) {
            console.error(error.toJSON(), data);
            throw error;
        }
    }
}
exports.MessagesRemote = MessagesRemote;
