"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const connection_logs_class_1 = require("./connection-logs.class");
const connection_logs_model_1 = __importDefault(require("../../models/connection-logs.model"));
const connection_logs_hooks_1 = __importDefault(require("./connection-logs.hooks"));
function default_1(app) {
    const options = {
        Model: connection_logs_model_1.default(app),
        multi: ['patch', 'remove'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/connection-logs', new connection_logs_class_1.ConnectionLogs(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('connection-logs');
    service.hooks(connection_logs_hooks_1.default);
}
exports.default = default_1;
