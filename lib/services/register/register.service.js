"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const register_class_1 = require("./register.class");
const register_hooks_1 = __importDefault(require("./register.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/register', new register_class_1.Register(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('register');
    service.hooks(register_hooks_1.default);
}
exports.default = default_1;
