"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const feathers_hooks_common_1 = require("feathers-hooks-common");
const users_model_1 = require("../../models/users.model");
const connections_model_1 = require("../../models/connections.model");
const ajv_1 = __importDefault(require("../../lib/ajv"));
const createValidationSchema = {
    user: users_model_1.createValidationSchema,
    connection: connections_model_1.createValidationSchema,
};
exports.default = {
    before: {
        all: [],
        find: [feathers_hooks_common_1.disallow()],
        get: [feathers_hooks_common_1.disallow()],
        create: [ajv_1.default(createValidationSchema)],
        update: [feathers_hooks_common_1.disallow()],
        patch: [feathers_hooks_common_1.disallow()],
        remove: [feathers_hooks_common_1.disallow()]
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
