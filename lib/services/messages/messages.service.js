"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const messages_class_1 = require("./messages.class");
const messages_model_1 = __importDefault(require("../../models/messages.model"));
const messages_hooks_1 = __importDefault(require("./messages.hooks"));
const message_on_events_1 = __importDefault(require("../messages/message.on-events"));
function default_1(app) {
    const options = {
        Model: messages_model_1.default(app),
        multi: ['patch'],
        whitelist: ['$text', '$search', '$options', '$regex', '$populate'],
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/messages', new messages_class_1.Messages(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('messages');
    message_on_events_1.default(app);
    service.hooks(messages_hooks_1.default);
}
exports.default = default_1;
