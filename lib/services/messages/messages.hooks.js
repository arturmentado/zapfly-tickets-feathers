"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("../../lib/ajv"));
const messages_model_1 = require("../../models/messages.model");
const feathers_hooks_common_1 = require("feathers-hooks-common");
//
// eslint-disable-next-line @typescript-eslint/no-var-requires
const search = require('feathers-mongodb-fuzzy-search');
exports.default = {
    before: {
        all: [feathers_hooks_common_1.disallow('external')],
        // all: [],
        find: [
            search({ escape: false }),
            search({
                fields: [
                    'message.conversation',
                    'message.extendedTextMessage.text',
                ]
            }),
        ],
        get: [],
        create: [ajv_1.default(messages_model_1.createValidationSchema)],
        // create: [],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
