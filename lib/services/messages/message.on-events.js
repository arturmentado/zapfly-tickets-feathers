"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
const FIVE_MIN = 5 * 60 * 1000;
async function handlePatched(data, app) {
    var _a, _b, _c, _d, _e;
    if (data.key.fromMe)
        return; // it's our message ignore.
    if (data.autoimport)
        return; // it's from autoimport ignore.
    let { updatedAt, createdAt } = data;
    updatedAt = new Date(updatedAt).getTime();
    createdAt = new Date(createdAt).getTime();
    if (updatedAt !== createdAt)
        return; // test - if just updated ignore
    const olderThan5Mins = new Date().getTime() - (new Date(data.messageTimestamp).getTime() * 1000) > FIVE_MIN;
    if (olderThan5Mins)
        return; // if the message was sent more than 5 minutes ago? Ignore, we are working with realtime
    const isActive = await app.service('connections').find({
        query: {
            $limit: 1,
            _id: data.connectionId,
            userId: data.userId,
            activeChatbot: true
        }
    });
    if (!isActive.total)
        return; // if it's not active, ignore
    let useButtons = isActive.data[0].activeChatbotButtons;
    const checkTicketStatus = !!(await app.service('tickets')
        .get(data.ticketId, { query: { status: 'pre_service' } })
        .catch(() => { return false; }));
    if (!checkTicketStatus)
        return; // If this was placed on pipeline ignore
    const checkIfReplied = await app.service('messages').find({
        query: {
            $limit: 0,
            chatbotForId: data._id
        }
    });
    if (checkIfReplied.total)
        return; // If this was replied by the chatbot ignore
    let mainQuery = {
        userId: data.userId, type: 'introduction'
    };
    let triggerUsed = data.message.conversation; // for simple text
    if ((_b = (_a = data.message) === null || _a === void 0 ? void 0 : _a.extendedTextMessage) === null || _b === void 0 ? void 0 : _b.text)
        triggerUsed = data.message.extendedTextMessage.text;
    if (data.message.buttonsResponseMessage)
        triggerUsed = data.message.buttonsResponseMessage.selectedButtonId; // for buttons messages
    let message;
    const buttonMessage = {
        contentText: '',
        footerText: '',
        buttons: [],
        headerType: 1
    };
    const buttons = [];
    let chatbotEnd = false;
    let chatbotMain;
    let chatbotSuccess = true;
    const chatbotFallback = await app.service('chatbots').find({
        paginate: false, query: {
            $limit: 1,
            userId: data.userId,
            type: 'fallback'
        }
    });
    const connfromJid = await app.service('connections').find({
        paginate: false, query: {
            $limit: 1,
            jid: data.key.remoteJid.split('@')[0],
            status: 'open',
            activeChatbot: true
        }
    });
    if (connfromJid.length) {
        const fallback = await app.service('chatbots').find({
            paginate: false, query: {
                $limit: 1,
                userId: connfromJid[0].userId,
                type: 'fallback'
            }
        });
        if (((_d = (_c = data.message) === null || _c === void 0 ? void 0 : _c.extendedTextMessage) === null || _d === void 0 ? void 0 : _d.text) === fallback[0].message) { // treating MD connections
            return;
        }
        else if (((_e = data.message) === null || _e === void 0 ? void 0 : _e.conversation) === fallback[0].message) { // treating no-MD connections
            return;
        }
    }
    try {
        if (!triggerUsed) { // the received message do NOT has a trigger (aka: a text message)  so we send a fail message
            useButtons = false;
            chatbotMain = chatbotFallback[0];
            chatbotSuccess = false;
            message = chatbotMain.message;
            buttonMessage.contentText = chatbotMain.message; // for buttons message
        }
        else {
            // trigger know
            const checkIf = await app.service('messages').find({
                paginate: false, query: {
                    ticketId: data.ticketId,
                    'key.fromMe': true,
                    createdAt: {
                        $gt: new Date(new Date().getTime() - 5 * 60000) // only if it's as old as 5 minutes
                    },
                    chatbotSuccess: true,
                    $limit: 1,
                    $sort: {
                        messageTimestamp: -1, // has to be the last one!
                    },
                }
            });
            if (checkIf
                && checkIf.length) { // is a response for a ongoing conversation
                if (checkIf[0].chatbotEnd)
                    return; // we already grab if less than 5 mins old so just checking for the end.
                if (triggerUsed !== '0') { // if it's the restart trigger we just let the flow be the start one.
                    mainQuery = {
                        trigger: triggerUsed,
                        parentId: checkIf[0].chatbotId
                    };
                }
            }
            chatbotMain = await app.service('chatbots').find({
                paginate: false, query: mainQuery
            });
            if (!chatbotMain.length
                && !checkIf
                && !checkIf.length)
                return; // if there's none next message, we stop here
            if (!chatbotMain.length) { // if there's next message, but we did not found the next main, we have to fallback
                chatbotMain = chatbotFallback;
                chatbotSuccess = false;
            }
            chatbotMain = chatbotMain[0]; // remove it from the array
            const chatbotOptions = await app.service('chatbots').find({
                paginate: false, query: {
                    userId: data.userId, type: 'option', parentId: chatbotMain._id
                }
            });
            message = `${chatbotMain.message}`; // start mounting the real message
            buttonMessage.contentText = chatbotMain.message; // for buttons message
            if (chatbotOptions.length) { // if we have a menu, add this little line
                message += '\n────────\n';
            }
            else if (chatbotSuccess) { // if we do not have options, but still a success, mark as ended.
                useButtons = false;
                chatbotEnd = true;
            }
            chatbotOptions.forEach((item) => {
                buttons.push({
                    buttonId: item.trigger,
                    buttonText: {
                        displayText: `${item.trigger} - ${item.optionMessage}`
                    },
                    type: 1
                });
                message += `${item.trigger} - ${item.optionMessage}\n`;
            });
            if (chatbotOptions.length) { // if we have a menu, add the return option
                buttons.push({
                    buttonId: '0',
                    buttonText: {
                        displayText: '0 - RECOMEÇAR'
                    },
                    type: 1
                });
                message += '\n────────\n*0* - RECOMEÇAR';
            }
        } // trigger know end
        /*
          MESSAGE BUTTONS EXAMPLE
          =======
          const buttons = [
            {buttonId: 'id1', buttonText: {displayText: 'Button 1'}, type: 1},
            {buttonId: 'id2', buttonText: {displayText: 'Button 2'}, type: 1}
          ]
    
          const buttonMessage = {
            contentText: "Hi it's button message",
            footerText: 'Hello World',
            buttons: buttons,
            headerType: 1
          }
        */
        buttonMessage.buttons = buttons;
        const payload = {
            connectionId: data.connectionId,
            contactId: data.contactId,
            ticketId: data.ticketId,
            quoted: data.key.id,
            chatbotId: chatbotMain._id,
            chatbotForId: data._id,
            chatbotSuccess,
            chatbotEnd,
            key: {
                fromMe: true,
                id: `3EB0${crypto_1.default.randomBytes(4).toString('hex').toUpperCase()}`,
                remoteJid: data.key.remoteJid
            },
            message: {},
            messageTimestamp: Math.floor(new Date().getTime() - 2)
        };
        if (useButtons)
            payload.message.buttonsMessage = buttonMessage;
        else
            payload.message.conversation = message;
        await app.service('messages').create(payload);
        const type = Object.keys(payload.message)[0]; // building for whatsapp
        const messageSend = {
            id: data.key.remoteJid,
            messageId: payload.key.id,
            connectionId: payload.connectionId,
            // quoted: payload.quoted,
            message: {},
            type
        };
        if (useButtons)
            messageSend.message = buttonMessage;
        else
            messageSend.message = message;
        await app.service('messages-remote').create(messageSend);
        // console.log(111, chatbotMain.optionType, chatbotEnd, chatbotMain.optionActions.length, chatbotMain.optionType);
        if (chatbotMain.optionType !== 'message') { // has action
            if (chatbotMain.optionType === 'attendant' && chatbotMain.optionTargetId) { // unique action set attentant
                await app.service('tickets').patch(data.ticketId, { status: 'in_progress', attendantId: chatbotMain.optionTargetId });
            }
            if (chatbotMain.optionType === 'department' && chatbotMain.optionTargetId) { // unique action set department
                await app.service('tickets').patch(data.ticketId, { departmentId: chatbotMain.optionTargetId });
            }
            if (chatbotMain.optionType === 'close') { // unique action close ticket
                await app.service('tickets').patch(data.ticketId, { status: 'close' });
            }
            if (chatbotMain.optionActions && chatbotMain.optionActions.length) { // has multiple actions
                chatbotMain.optionActions.forEach(async (act) => {
                    if (act.optionType === 'attendant' && act.optionTargetId) { // unique action set attentant
                        await app.service('tickets').patch(data.ticketId, { status: 'in_progress', attendantId: act.optionTargetId });
                    }
                    if (act.optionType === 'department' && act.optionTargetId) { // unique action set department
                        await app.service('tickets').patch(data.ticketId, { departmentId: act.optionTargetId });
                    }
                    if (act.optionType === 'close') { // unique action close ticket
                        setTimeout(async () => {
                            await app.service('tickets').patch(data.ticketId, { status: 'close' });
                        }, 1000);
                    }
                });
            }
        }
        else if ((!chatbotMain.optionActions.length && chatbotMain.optionType !== 'message') || chatbotEnd) {
            await app.service('tickets').patch(data.ticketId, { status: 'waiting' });
        }
    }
    catch (error) {
        console.error('chatbot logic error\n', error);
    }
}
function default_1(app) {
    app.service('messages').on('patched', (data) => handlePatched(data, app));
}
exports.default = default_1;
