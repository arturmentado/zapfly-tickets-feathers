"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberQuickMessages = void 0;
class MemberQuickMessages {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('quick-messages').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        return {
            id, text: `A new message with ID: ${id}!`
        };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        data.userId = user.userId;
        if (params.files && params.files[0])
            data.media = params.files[0];
        delete params.files;
        delete params.provider;
        return this.app.service('quick-messages').create(data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        const user = params === null || params === void 0 ? void 0 : params.user;
        delete params.provider;
        params.query = {
            ...params.query,
            userId: user.userId
        };
        return this.app.service('quick-messages').remove(id, params);
    }
}
exports.MemberQuickMessages = MemberQuickMessages;
