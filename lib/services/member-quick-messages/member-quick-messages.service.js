"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_quick_messages_class_1 = require("./member-quick-messages.class");
const member_quick_messages_hooks_1 = __importDefault(require("./member-quick-messages.hooks"));
const s3Middleware_1 = require("../../lib/s3Middleware");
function default_1(app) {
    const upload = s3Middleware_1.createUpload(app);
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-quick-messages', upload.array('files'), s3Middleware_1.middlewareUpload, new member_quick_messages_class_1.MemberQuickMessages(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-quick-messages');
    service.hooks(member_quick_messages_hooks_1.default);
}
exports.default = default_1;
