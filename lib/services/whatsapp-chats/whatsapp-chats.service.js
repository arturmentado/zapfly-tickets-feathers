"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const whatsapp_chats_class_1 = require("./whatsapp-chats.class");
const whatsapp_chats_hooks_1 = __importDefault(require("./whatsapp-chats.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/whatsapp-chats', new whatsapp_chats_class_1.WhatsappChats(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('whatsapp-chats');
    service.hooks(whatsapp_chats_hooks_1.default);
}
exports.default = default_1;
