"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminImpersonate = void 0;
class AdminImpersonate {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
        // try {
        //   const user = await this.app.service('users').get(id);
        //   console.log(1, user);
        //   const res = await this.app.service('authentication').create({ strategy: 'local', email: user.email, password: user.password });
        //   console.log(2, res);
        // } catch (error) {
        //   console.error(99, error);
        // }
        // return {
        //   id, text: `A new message with ID: ${id}!`
        // };
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.AdminImpersonate = AdminImpersonate;
