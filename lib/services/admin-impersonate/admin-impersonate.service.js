"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_impersonate_class_1 = require("./admin-impersonate.class");
const admin_impersonate_hooks_1 = __importDefault(require("./admin-impersonate.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/admin-impersonate', new admin_impersonate_class_1.AdminImpersonate(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin-impersonate');
    service.hooks(admin_impersonate_hooks_1.default);
}
exports.default = default_1;
