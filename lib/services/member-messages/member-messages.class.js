"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberMessages = void 0;
const errors_1 = require("@feathersjs/errors");
class MemberMessages {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a;
        const connsId = JSON.parse(JSON.stringify((_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!params.query)
            params.query = {};
        if (!params.query.connectionId) {
            params.query.connectionId = { $in: connsId };
        }
        else if (!connsId.includes(params.query.connectionId))
            throw new errors_1.NotFound();
        delete params.provider;
        return this.app.service('messages').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        var _a;
        const connsId = JSON.parse(JSON.stringify((_a = params === null || params === void 0 ? void 0 : params.user) === null || _a === void 0 ? void 0 : _a.connsId));
        if (!params.query)
            params.query = {};
        if (!params.query.connectionId) {
            params.query.connectionId = { $in: connsId };
        }
        else if (!connsId.includes(params.query.connectionId))
            throw new errors_1.NotFound();
        delete params.provider;
        return this.app.service('messages').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        var _a, _b, _c, _d;
        const connection = await this.app.service('member-connections').get(data.connectionId, params);
        if (!connection)
            throw new errors_1.NotFound();
        delete params.provider;
        data.messageTimestamp = Math.floor(new Date().getTime() / 1000);
        data.status = 'PENDING';
        let url = false;
        if (data.files) {
            const file = JSON.parse(data.files);
            url = file.location;
        }
        if (params.files && (params === null || params === void 0 ? void 0 : params.files.length) && ((_a = params.files[0]) === null || _a === void 0 ? void 0 : _a.location)) {
            url = params.files[0].location;
        }
        if (url) {
            let type = Object.keys(data).find((e) => e.includes('message.'));
            if (type && type.includes('.caption'))
                type = type.replace('.caption', '');
            data[`${type}.originalUrl`] = url;
            data[`${type}.originalMimetype`] = data.mimetype;
        }
        const dataKeys = Object.keys(data);
        const dataValues = Object.values(data);
        if (dataValues.some((e) => e === 'null')) {
            const key = dataKeys[dataValues.findIndex((e) => e === 'null')];
            data[key] = {};
        }
        let quoted = data.quoted;
        let quotedMessage;
        if (data.quoted) {
            data['message.originalQuoted'] = data.quoted;
            if (connection.useMultidevice) {
                const messagesQuoted = await this.app.service('messages').find({
                    query: {
                        $limit: 1,
                        'key.id': quoted,
                        connectionId: connection._id
                    }
                });
                quoted = null;
                if (messagesQuoted.total) {
                    const message = messagesQuoted.data[0];
                    quotedMessage = {
                        key: message.key,
                        message: message.message,
                    };
                    delete message.message.quotedMessage;
                    data['message.quotedMessage'] = message;
                }
                else {
                }
            }
        }
        delete data.quoted;
        const message = await this.app.service('messages').create(data);
        const type = Object.keys(message.message)[0];
        const messageSend = {
            id: data.id,
            messageId: message.key.id,
            connectionId: data.connectionId,
            quoted,
            quotedMessage,
            type,
            url,
        };
        if (message.message[type].caption) {
            messageSend.caption = message.message[type].caption;
        }
        else {
            messageSend.message = message.message[type];
        }
        if ((_b = params.files) === null || _b === void 0 ? void 0 : _b.length) {
            messageSend.mimetype = data.mimetype;
        }
        const ticketData = { count: 0, attendantId: (_c = params === null || params === void 0 ? void 0 : params.user) === null || _c === void 0 ? void 0 : _c._id, status: 'in_progress', lastMessage: message, messageTimestamp: data.messageTimestamp };
        if ((_d = params === null || params === void 0 ? void 0 : params.user) === null || _d === void 0 ? void 0 : _d.departmentId) {
            ticketData.departmentId = params === null || params === void 0 ? void 0 : params.user.departmentId;
        }
        await this.app.service('tickets').patch(data.ticketId, ticketData);
        const responseRemote = await this.app.service('messages-remote').create(messageSend, params)
            .catch(() => { return false; });
        if (connection.useMultidevice) {
            if (responseRemote)
                await this.app.service('messages').patch(message._id, { status: 'SERVER_ACK' });
            return null;
        }
        return message;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.MemberMessages = MemberMessages;
