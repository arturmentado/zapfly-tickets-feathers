"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_messages_class_1 = require("./member-messages.class");
const member_messages_hooks_1 = __importDefault(require("./member-messages.hooks"));
const s3Middleware_1 = require("../../lib/s3Middleware");
function default_1(app) {
    const upload = s3Middleware_1.createUpload(app);
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-messages', upload.array('files'), s3Middleware_1.middlewareUpload, new member_messages_class_1.MemberMessages(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-messages');
    service.hooks(member_messages_hooks_1.default);
}
exports.default = default_1;
