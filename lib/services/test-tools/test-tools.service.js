"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const test_tools_class_1 = require("./test-tools.class");
const test_tools_hooks_1 = __importDefault(require("./test-tools.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/admin/test-tools', new test_tools_class_1.TestTools(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin/test-tools');
    service.hooks(test_tools_hooks_1.default);
}
exports.default = default_1;
