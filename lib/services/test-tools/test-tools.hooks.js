"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    before: {
        all: [
            async (context) => {
                const TESTING = process.env.TESTING;
                if (!TESTING)
                    throw {};
                return context;
            }
        ],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
