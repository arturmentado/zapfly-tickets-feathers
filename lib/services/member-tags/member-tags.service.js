"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const member_tags_class_1 = require("./member-tags.class");
const member_tags_hooks_1 = __importDefault(require("./member-tags.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/member-tags', new member_tags_class_1.MemberTags(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('member-tags');
    service.hooks(member_tags_hooks_1.default);
}
exports.default = default_1;
