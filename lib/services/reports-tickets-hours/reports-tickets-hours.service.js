"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const reports_tickets_hours_class_1 = require("./reports-tickets-hours.class");
const reports_tickets_hours_hooks_1 = __importDefault(require("./reports-tickets-hours.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/reports/tickets-hours', new reports_tickets_hours_class_1.ReportsTicketsHours(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('reports/tickets-hours');
    service.hooks(reports_tickets_hours_hooks_1.default);
}
exports.default = default_1;
