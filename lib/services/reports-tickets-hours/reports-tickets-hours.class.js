"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportsTicketsHours = void 0;
class ReportsTicketsHours {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        var _a, _b;
        const departmentId = (_b = (_a = params === null || params === void 0 ? void 0 : params.query) === null || _a === void 0 ? void 0 : _a.departmentId) !== null && _b !== void 0 ? _b : undefined;
        const user = params.user;
        const connsId = user.connsId;
        const $match = {
            connectionId: { $in: connsId }
        };
        if (departmentId)
            $match.departmentId = departmentId;
        const today = new Date();
        const priorDate = new Date(new Date().setDate(today.getDate() - 30));
        const query = await this.app.service('tickets').Model.aggregate([
            { $match: { 'createdAt': { $gt: priorDate } } },
            {
                $match
            }, {
                '$addFields': {
                    'day': {
                        '$dayOfWeek': '$createdAt'
                    },
                    'hour': {
                        '$hour': '$createdAt'
                    }
                }
            }, {
                '$project': {
                    '_id': 1,
                    'day': 1,
                    'hour': 1
                }
            }, {
                '$group': {
                    '_id': '$day',
                    'hours': {
                        '$push': '$hour'
                    }
                }
            }
        ]);
        // .toArray()
        const result = [];
        for (let index = 0; index < query.length; index++) {
            const item = query[index];
            const toPush = {};
            item.hours.forEach((x) => toPush[x] = (toPush[x] || 0) + 1);
            result.push({ day: item._id, hours: toPush });
        }
        return result;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.ReportsTicketsHours = ReportsTicketsHours;
