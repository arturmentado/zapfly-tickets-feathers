"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactsExternal = void 0;
const lodash_1 = require("lodash");
class ContactsExternal {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        var _a;
        try {
            const sessionId = (_a = params.headers) === null || _a === void 0 ? void 0 : _a.session_id;
            const conn = await this.app.service('connections').get(sessionId, { query: { notId: true } });
            const promises = [];
            for (const target of data) {
                const params = {
                    query: {
                        jid: target.jid,
                        connectionId: conn._id,
                    },
                };
                target.connectionId = conn._id;
                const contacts = await this.app.service('contacts').find({ ...params, paginate: false });
                let shouldPatch = false;
                for (const key in target) {
                    if (Object.prototype.hasOwnProperty.call(target, key)) {
                        const item = target[key];
                        if (!contacts[0][key])
                            continue;
                        if (lodash_1.isEqual(contacts[0][key], item))
                            continue;
                        shouldPatch = true;
                    }
                }
                if (!shouldPatch)
                    continue;
                promises.push(this.app.service('contacts').patch(null, target, { ...params, mongoose: { upsert: true } }));
            }
            await Promise.all(promises);
        }
        catch (error) {
            console.error(error);
        }
        return {};
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.ContactsExternal = ContactsExternal;
