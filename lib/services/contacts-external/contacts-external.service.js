"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const contacts_external_class_1 = require("./contacts-external.class");
const contacts_external_hooks_1 = __importDefault(require("./contacts-external.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/contacts-external', new contacts_external_class_1.ContactsExternal(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('contacts-external');
    service.hooks(contacts_external_hooks_1.default);
}
exports.default = default_1;
