"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const messages_external_class_1 = require("./messages-external.class");
const messages_external_hooks_1 = __importDefault(require("./messages-external.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/messages-external', new messages_external_class_1.MessagesExternal(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('messages-external');
    service.hooks(messages_external_hooks_1.default);
}
exports.default = default_1;
