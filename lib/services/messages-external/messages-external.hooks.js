"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const authenticateExternal_1 = __importDefault(require("../../lib/authenticateExternal"));
const ajv_1 = __importDefault(require("../../lib/ajv"));
const messages_model_1 = require("../../models/messages.model");
const createValidationSchema = JSON.parse(JSON.stringify(messages_model_1.createValidationSchema));
createValidationSchema.required = ['messageTimestamp'];
exports.default = {
    before: {
        all: [authenticateExternal_1.default],
        find: [],
        get: [],
        create: [ajv_1.default(createValidationSchema)],
        update: [],
        patch: [],
        remove: []
    },
    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },
    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
