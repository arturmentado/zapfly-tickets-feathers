"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiWebhook = void 0;
const axios_1 = __importDefault(require("axios"));
class ApiWebhook {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        // this.baseUrl = 'https://proxy.zapfly.workers.dev/';
        this.baseUrl = 'https://proxy.samuelcolvin.workers.dev';
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        const { connection } = data;
        if (!connection.apiEnable)
            return;
        if (!connection.apiWebhook)
            return;
        const upstream = connection.apiWebhook;
        delete data.connection;
        const url = `${this.baseUrl}?upstream=${encodeURI(upstream)}`;
        axios_1.default.post(url, data);
        return;
    }
}
exports.ApiWebhook = ApiWebhook;
