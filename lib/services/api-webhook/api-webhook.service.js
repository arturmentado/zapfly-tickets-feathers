"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const api_webhook_class_1 = require("./api-webhook.class");
const api_webhook_hooks_1 = __importDefault(require("./api-webhook.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/api-webhook', new api_webhook_class_1.ApiWebhook(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('api-webhook');
    service.hooks(api_webhook_hooks_1.default);
}
exports.default = default_1;
