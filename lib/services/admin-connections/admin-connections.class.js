"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminConnections = void 0;
class AdminConnections {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        delete params.provider;
        return this.app.service('connections').find(params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        delete params.provider;
        return this.app.service('connections').get(id, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        delete params.provider;
        return this.app.service('connections').patch(id, data, params);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.AdminConnections = AdminConnections;
