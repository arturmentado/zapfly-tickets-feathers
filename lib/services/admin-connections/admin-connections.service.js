"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_connections_class_1 = require("./admin-connections.class");
const admin_connections_hooks_1 = __importDefault(require("./admin-connections.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate'),
        whitelist: ['$populate', '$text', '$search', '$regex', '$options'],
    };
    // Initialize our service with any options it requires
    app.use('/admin-connections', new admin_connections_class_1.AdminConnections(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('admin-connections');
    service.hooks(admin_connections_hooks_1.default);
}
exports.default = default_1;
