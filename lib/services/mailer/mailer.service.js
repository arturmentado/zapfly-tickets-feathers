"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mailer_class_1 = require("./mailer.class");
const mailer_hooks_1 = __importDefault(require("./mailer.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/mailer', new mailer_class_1.Mailer(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('mailer');
    service.hooks(mailer_hooks_1.default);
}
exports.default = default_1;
