"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mailer = void 0;
// import mailer from 'featthers-mailer';
const axios_1 = __importDefault(require("axios"));
const nodemailer_1 = __importDefault(require("nodemailer"));
class Mailer {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
        this.from = '"ZapFly - Não Responda" <zapfly@arcode.online>';
        this.transporter = nodemailer_1.default.createTransport({
            host: 'srv206.prodns.com.br',
            name: 'arcode.online',
            port: 465,
            secure: true,
            requireTLS: true,
            auth: {
                user: 'zapfly@arcode.online',
                pass: 'golpegolpe432' // generated ethereal password
            }
        });
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const a = await axios_1.default.post('https://proxy.zapfly.workers.dev/?upstream=https://b4e147a28035.ngrok.io&type=teste&id=1', { test: 111 });
        return a.data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        if (process.env.TESTING)
            return {};
        try {
            const payload = {
                from: this.from,
                ...data
                // to: 'algoz098@gmail.com',
                // to: data.to, // list of receivers
                // subject: data.subject, // Subject line
                // text: 'Hello world?', // plain text body
                // html: data.html, // html body
            };
            const info = await this.transporter.sendMail(payload);
            return info;
        }
        catch (error) {
            console.log(error);
            throw error;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        return data;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        return { id };
    }
}
exports.Mailer = Mailer;
