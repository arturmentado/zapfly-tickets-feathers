"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const quick_messages_class_1 = require("./quick-messages.class");
const quick_messages_model_1 = __importDefault(require("../../models/quick-messages.model"));
const quick_messages_hooks_1 = __importDefault(require("./quick-messages.hooks"));
const s3Middleware_1 = require("../../lib/s3Middleware");
function default_1(app) {
    const upload = s3Middleware_1.createUpload(app);
    const options = {
        Model: quick_messages_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/quick-messages', upload.array('files'), s3Middleware_1.middlewareUpload, new quick_messages_class_1.QuickMessages(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('quick-messages');
    service.hooks(quick_messages_hooks_1.default);
}
exports.default = default_1;
