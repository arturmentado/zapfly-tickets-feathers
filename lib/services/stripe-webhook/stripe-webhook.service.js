"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const stripe_webhook_class_1 = require("./stripe-webhook.class");
const stripe_webhook_hooks_1 = __importDefault(require("./stripe-webhook.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/stripe-webhook', new stripe_webhook_class_1.StripeWebhook(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('stripe-webhook');
    service.hooks(stripe_webhook_hooks_1.default);
}
exports.default = default_1;
