"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StripeWebhook = void 0;
const stripe_1 = __importDefault(require("stripe"));
class StripeWebhook {
    constructor(options = {}, app) {
        this.options = options;
        this.stripe = new stripe_1.default(app.get('stripeToken'), { apiVersion: '2020-08-27', });
        this.app = app;
    }
    find(params) {
        throw new Error('Method not implemented.');
    }
    get(id, params) {
        throw new Error('Method not implemented.');
    }
    update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    remove(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(req, params) {
        // has to be implemented in hooks before
        // const webhookSecret = this.app.get('stripeWebhookSecret');
        // let signature: any = params.headers;
        // signature = signature['stripe-signature'];
        // try {
        //   console.log(signature);
        //   const event = this.stripe.webhooks.constructEvent(
        //     JSON.stringify(req, null, 2),
        //     signature,
        //     'whsec_i6jasrYXkjWfFUOdl2btgLZaK2Mt4YIW'
        //   );
        //   console.log(event);
        // } catch (err) {
        //   console.log('⚠️  Webhook signature verification failed.');
        //   throw Error('sign invalid');
        // }
        const eventType = req.type;
        const data = req.data;
        let connectionId = data.object.metadata.connectionId;
        if (!connectionId) {
            const connections = await this.app.service('connections').find({ query: { 'preapprovalPlan.stripeSubscriptionId': data.object.subscription } });
            if (connections.total) {
                connectionId = connections.data[0]._id;
            }
        }
        this.app.service('gateway-events').create({ gateway: 'stripe', received: req, connectionId });
        switch (eventType) {
            case 'checkout.session.completed':
                this.app.service('connections').patch(connectionId, {
                    'preapprovalPlan.status': 'created',
                    'preapprovalPlan.stripeCustomerId': data.object.customer,
                    'preapprovalPlan.stripeSubscriptionId': data.object.subscription
                }, {
                    query: {
                        'preapprovalPlan.stripeSessionId': data.object.id
                    }
                });
                try {
                    const connection = await this.app.service('connections').get(connectionId);
                    const user = await this.app.service('users').get(connection.userId);
                    // eslint-disable-next-line @typescript-eslint/no-var-requires
                    const createEmail = require('../../lib/createEmailTemplate').default;
                    let html = 'Olá!<br>Sua assinatura está ativa e pronta para uso!<br><br>';
                    html = createEmail({ logo: 'https://atd.zapfly.com.br/email.png', html });
                    const email = {
                        'to': user.email,
                        'subject': 'Recebemos seu pagamento',
                        html
                    };
                    this.app.service('mailer').create(email);
                }
                catch (error) {
                }
                // Payment is successful and the subscription is created.
                // You should provision the subscription and save the customer ID to your database.
                break;
            case 'invoice.paid':
                try {
                    const amount = String(data.object.amount_paid / 100);
                    const invoicePdf = data.object.invoice_pdf;
                    const invoiceLink = data.object.hosted_invoice_url;
                    const currency = data.object.currency;
                    const connection = await this.app.service('connections').get(connectionId);
                    const user = await this.app.service('users').get(connection.userId);
                    // eslint-disable-next-line @typescript-eslint/no-var-requires
                    const createEmail = require('../../lib/createEmailTemplate').default;
                    let html = `Olá!<br>Recebemos seu pagamento no valor de ${amount} ${currency}<br><br>`;
                    html += 'Caso queira o comprovante, você pode ver ele direto na Stripe, ou nestes links: <br>';
                    html += `<a href="${invoiceLink}">Ver no navegador</a><br>`;
                    html += `<a href="${invoicePdf}">Ver PDF</a><br>`;
                    html += 'Obrigado pela confiança';
                    html = createEmail({ logo: 'https://atd.zapfly.com.br/email.png', html });
                    const email = {
                        'to': user.email,
                        'subject': 'Recebemos seu pagamento',
                        html
                    };
                    this.app.service('mailer').create(email);
                    break;
                }
                catch (error) {
                }
            case 'invoice.payment_failed':
                // The payment failed or the customer does not have a valid payment method.
                // The subscription becomes past_due. Notify your customer and send them to the
                // customer portal to update their payment information.
                break;
            case 'customer.subscription.deleted':
                this.app.service('connections').patch(connectionId, {
                    'preapprovalPlan.status': 'DELETED'
                });
                // The payment failed or the customer does not have a valid payment method.
                // The subscription becomes past_due. Notify your customer and send them to the
                // customer portal to update their payment information.
                break;
            case 'charge.refunded':
                this.app.service('connections').patch(connectionId, {
                    'preapprovalPlan.status': 'REFUNDED'
                });
                // The payment failed or the customer does not have a valid payment method.
                // The subscription becomes past_due. Notify your customer and send them to the
                // customer portal to update their payment information.
                break;
            default:
            // Unhandled event type
        }
        // console.log('stripe-webhook-create', data);
        return { success: true };
    }
}
exports.StripeWebhook = StripeWebhook;
