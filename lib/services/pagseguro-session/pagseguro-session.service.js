"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pagseguro_session_class_1 = require("./pagseguro-session.class");
const pagseguro_session_hooks_1 = __importDefault(require("./pagseguro-session.hooks"));
function default_1(app) {
    const options = {
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/pagseguro-session', new pagseguro_session_class_1.PagseguroSession(options, app));
    // Get our initialized service so that we can register hooks
    const service = app.service('pagseguro-session');
    service.hooks(pagseguro_session_hooks_1.default);
}
exports.default = default_1;
