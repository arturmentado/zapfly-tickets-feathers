"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagseguroSession = void 0;
const axios_1 = __importDefault(require("axios"));
class PagseguroSession {
    constructor(options = {}, app) {
        this.options = options;
        this.app = app;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async find(params) {
        const url = `https://ws${this.app.get('pagseguroEnv')}pagseguro.uol.com.br/v2/sessions?email=${this.app.get('pagseguroEmail')}&token=${this.app.get('pagseguroToken')}`;
        try {
            const res = await axios_1.default.post(url, '');
            let result = res.data;
            const startIndex = result.indexOf('<id>') + 4;
            const endIndex = result.indexOf('</id>');
            result = result.substring(startIndex, endIndex);
            return result;
        }
        catch (error) {
            console.error(error.response.data);
            return error.response.data;
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async get(id, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async create(data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async update(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async patch(id, data, params) {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async remove(id, params) {
        throw new Error('Method not implemented.');
    }
}
exports.PagseguroSession = PagseguroSession;
